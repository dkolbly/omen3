package ptr

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/binary"
	"fmt"
	"math"
	"math/big"
)

type LiteralKind int

const obfuscateLiterals = true

var literalCipher cipher.Block

func init() {
	literalKey := [24]byte{
		0x49, 0xc7, 0x3f, 0x94, 0xe1, 0x23, 0x29, 0x5f,
		0xec, 0xa5, 0xfb, 0xad, 0xd5, 0x94, 0xe7, 0xd6,
	}
	c, err := aes.NewCipher(literalKey[:])
	if err != nil {
		panic(err)
	}
	literalCipher = c
}

const (
	KindString = LiteralKind(iota)
	KindInt
	KindFloat
	KindBytes
	KindUUID
)

const (
	majorGeneral = iota
	majorLongString
	majorLongBytes
)

const (
	minorFixedWidth = iota
	minorString
	minorBytes
	minorNonNegativeInt
	minorNegativeInt
)

const (
	fwUuid = iota
	fwFloat64
)

func (p Ptr) IntValue() *big.Int {
	if !p.IsInt() {
		panic("not an int literal")
	}
	unobscure(&p)
	i := new(big.Int)
	i.SetBytes(p.Bits[:p.glen()])
	if p.Bits[30]>>5 == minorNegativeInt {
		i.Neg(i)
	}
	return i
}

// return the length parameter of a majorGeneral literal
func (p Ptr) glen() int {
	return int(p.Bits[30] & 0x1f)
}

func (p Ptr) IsString() bool {
	if p.Bits[31] == uint8(Literal)+(majorLongString<<4) {
		return true
	}
	if p.Bits[31] != uint8(Literal)+(majorGeneral<<4) {
		return false
	}
	unobscure(&p)
	minor := p.Bits[30] >> 5
	return minor == minorString
}

func (p Ptr) IsBytes() bool {
	if p.Bits[31] == uint8(Literal)+(majorLongBytes<<4) {
		return true
	}
	if p.Bits[31] != uint8(Literal)+(majorGeneral<<4) {
		return false
	}
	unobscure(&p)
	minor := p.Bits[30] >> 5
	return minor == minorBytes
}

func (p Ptr) IsInt() bool {
	if p.Bits[31] != uint8(Literal)+(majorGeneral<<4) {
		return false
	}
	unobscure(&p)
	minor := p.Bits[30] >> 5
	return minor == minorNonNegativeInt || minor == minorNegativeInt
}

func (p Ptr) Kind() LiteralKind {
	switch p.Bits[31] {
	case uint8(Literal) + (majorLongString << 4):
		return KindString
	case uint8(Literal) + (majorLongBytes << 4):
		return KindBytes
	case uint8(Literal) + (majorGeneral << 4):
		unobscure(&p)
		switch p.Bits[30] >> 5 {
		case minorString:
			return KindString
		case minorBytes:
			return KindBytes
		case minorNegativeInt, minorNonNegativeInt:
			return KindInt
		case minorFixedWidth:
			switch p.Bits[30] & 0x1f {
			case fwUuid:
				return KindUUID
			case fwFloat64:
				return KindFloat
			default:
				panic(fmt.Sprintf("invalid fixed-width kind <%02x %02x>", p.Bits[30], p.Bits[31]))
			}
		default:
			panic(fmt.Sprintf("invalid minor kind <%02x %02x>", p.Bits[30], p.Bits[31]))
		}
	default:
		panic(fmt.Sprintf("invalid major kind <%02x %02x>", p.Bits[30], p.Bits[31]))
	}
}

func unobscure(p *Ptr) {
	if obfuscateLiterals {
		right := p.Bits[31-16 : 31]
		left := p.Bits[0:16]
		literalCipher.Decrypt(right, right)
		literalCipher.Decrypt(left, left)
	}
}

func fixliteral(major, minor uint8, len int, p *Ptr) {
	if major == majorGeneral {
		p.Bits[30] = (minor << 5) + uint8(len)
	}

	p.Bits[31] = (major << 4) + uint8(Literal)

	if obfuscateLiterals {
		right := p.Bits[31-16 : 31]
		left := p.Bits[0:16]
		literalCipher.Encrypt(left, left)
		literalCipher.Encrypt(right, right)
	}
}

func (p Ptr) StringValue() string {
	if p.Bits[31] == uint8(Literal)+(majorLongString<<4) {
		unobscure(&p)
		return string(p.Bits[:31])
	}
	if p.Bits[31] != uint8(Literal)+(majorGeneral<<4) {
		panic("not a string literal")
	}
	unobscure(&p)
	minor := p.Bits[30] >> 5
	if minor != minorString {
		panic("not a string literal")
	}
	return string(p.Bits[:p.glen()])
}

func (p Ptr) BytesValue() []byte {
	if p.Bits[31] == uint8(Literal)+(majorLongBytes<<4) {
		unobscure(&p)
		return p.Bits[:31]
	}
	if p.Bits[31] != uint8(Literal)+(majorGeneral<<4) {
		panic("not a bytes literal")
	}
	unobscure(&p)
	minor := p.Bits[30] >> 5
	if minor != minorBytes {
		panic("not a bytes literal")
	}
	return p.Bits[:p.glen()]
}

func (p Ptr) IsFloat() bool {
	if p.Bits[31] != uint8(Literal)+(majorGeneral<<4) {
		return false
	}
	unobscure(&p)
	const floatMinor = (minorFixedWidth << 5) + fwFloat64
	return p.Bits[30] == floatMinor
}

func (p Ptr) IsUUID() bool {
	if p.Bits[31] != uint8(Literal)+(majorGeneral<<4) {
		return false
	}
	unobscure(&p)
	const floatMinor = (minorFixedWidth << 5) + fwUuid
	return p.Bits[30] == floatMinor
}

func (p Ptr) UUIDValue() [16]byte {
	if p.Bits[31] != uint8(Literal)+(majorGeneral<<4) {
		panic("not a uuid literal")
	}
	unobscure(&p)
	const uuidMinor = (minorFixedWidth << 5) + fwUuid
	if p.Bits[30] != uuidMinor {
		panic("not a uuid literal")
	}
	var out [16]byte
	copy(out[:], p.Bits[:16])
	return out
}

func (p Ptr) Float64Value() float64 {
	if p.Bits[31] != uint8(Literal)+(majorGeneral<<4) {
		panic("not a float literal")
	}
	unobscure(&p)
	const floatMinor = (minorFixedWidth << 5) + fwFloat64
	if p.Bits[30] != floatMinor {
		panic("not a float literal")
	}
	data := binary.BigEndian.Uint64(p.Bits[0:8])
	return math.Float64frombits(data)
}

func Float64(f float64) (p Ptr) {
	data := math.Float64bits(f)
	binary.BigEndian.PutUint64(p.Bits[0:8], data)
	fixliteral(majorGeneral, minorFixedWidth, fwFloat64, &p)
	return
}

func UUID(uuid [16]byte) (p Ptr) {
	copy(p.Bits[:16], uuid[:])
	fixliteral(majorGeneral, minorFixedWidth, fwUuid, &p)
	return
}

func Bytes(b []byte) (p Ptr) {
	if len(b) > 31 {
		panic("too many bytes")
	}
	if len(b) == 31 {
		copy(p.Bits[0:31], b)
		fixliteral(majorLongBytes, 0, 0, &p)
		return
	}
	copy(p.Bits[:len(b)], b)
	fixliteral(majorGeneral, minorBytes, len(b), &p)
	return
}

func String(s string) (p Ptr) {
	if len(s) > 31 {
		panic("string too long")
	}

	if len(s) == 31 {
		copy(p.Bits[0:31], s)
		fixliteral(majorLongString, 0, 0, &p)
		return
	}
	copy(p.Bits[:len(s)], s)
	fixliteral(majorGeneral, minorString, len(s), &p)
	return
}

func Int(x *big.Int) (p Ptr) {
	src := x.Bytes()
	copy(p.Bits[:], src)
	if len(src) > 30 {
		panic("int is too big, max is 30 bytes")
	}

	if x.Sign() == -1 {
		fixliteral(majorGeneral, minorNegativeInt, len(src), &p)
	} else {
		fixliteral(majorGeneral, minorNonNegativeInt, len(src), &p)
	}
	return
}

func Int64(x int64) (p Ptr) {
	return Int(big.NewInt(x))
}

/*
var bits uint64
var float float64

float := math.Float64frombits(bits)

bits := math.Float64bits(float)
*/
