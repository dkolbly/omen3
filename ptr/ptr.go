package ptr

const PtrSize = 32

type Ptr struct {
	Bits [PtrSize]byte
}

var Null = Ptr{}

func (p Ptr) IsNull() bool {
	return p == Null
}

// note that Null does not count as valid
func (p Ptr) IsValid() bool {
	switch p.Metatype() {
	case Untyped, HasTypePointer, Observable, Literal, Hash:
		return true
	}
	return false
}

func (p Ptr) IsLoadable() bool {
	switch MetatypeCode(p.Bits[31] & 0xf) {
	case Untyped, HasTypePointer:
		return true
	}
	return false
}

func (p Ptr) IsUntyped() bool {
	return MetatypeCode(p.Bits[31]&0xf) == Untyped
}

func (p Ptr) IsTyped() bool {
	return MetatypeCode(p.Bits[31]&0xf) == HasTypePointer
}

func (p Ptr) IsObservable() bool {
	return MetatypeCode(p.Bits[31]&0xf) == Observable
}

func (p Ptr) IsLiteral() bool {
	return MetatypeCode(p.Bits[31]&0xf) == Literal
}

func (p Ptr) IsHash() bool {
	return MetatypeCode(p.Bits[31]&0xf) == Hash
}

type ErrNotPresent Ptr

func (err ErrNotPresent) Error() string {
	return "object does not exist"
}

func IsNotPresent(err error) bool {
	_, ok := err.(ErrNotPresent)
	return ok
}
