package ptr

import (
	"crypto/sha256"
)

func ContentAddress(mt MetatypeCode, data []byte) (p Ptr) {
	h := sha256.New()
	h.Write(data)
	copy(p.Bits[:], h.Sum(nil))
	p.Bits[31] = (p.Bits[31] & 0xf0) + uint8(mt)
	return
}

// TypedContentAddress computes the Ptr value for a body
// with the given type header.  The resulting Ptr has
// metatype HasTypePointer
func TypedContentAddress(t Ptr, body []byte) (p Ptr) {
	h := sha256.New()
	h.Write(t.Bits[:])
	h.Write(body)
	copy(p.Bits[:], h.Sum(nil))
	p.Bits[31] = (p.Bits[31] & 0xf0) + uint8(HasTypePointer)
	return
}
