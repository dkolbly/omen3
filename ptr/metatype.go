package ptr

import (
	"errors"
)

var ErrInvalidMetatype = errors.New("invalid ptr type")

type MetatypeCode int

const (
	Zero = MetatypeCode(iota)
	Untyped
	HasTypePointer
	Observable
	Literal
	Hash
)

func (p Ptr) Metatype() MetatypeCode {
	return MetatypeCode(p.Bits[31] & 0xf)
}

func mtToChar(m MetatypeCode) byte {
	switch m {
	case Untyped:
		return 'u'
	case HasTypePointer:
		return 't'
	case Observable:
		return 'o'
	case Literal:
		return 'l'
	case Hash:
		return 'h'
	default:
		return '?'
	}
}

func charToMt(ch byte) (MetatypeCode, bool) {
	switch ch {
	case 'u':
		return Untyped, true
	case 't':
		return HasTypePointer, true
	case 'o':
		return Observable, true
	case 'l':
		return Literal, true
	case 'h':
		return Hash, true
	default:
		return 0, false
	}
}
