package ptr

import (
	"errors"
)

// Decode a string-encoded Ptr
func DecodeStringPtr(s string) (p Ptr, ok bool) {
	return decodeBytesPtr([]byte(s))
}

func decodeBytesPtr(s []byte) (p Ptr, ok bool) {

	if len(s) != 43 {
		return
	}
	mt, ok := charToMt(s[42])
	if !ok {
		return
	}

	j := 0
	// 10 regular groups
	for i := 0; i < 10; i++ {
		a0 := decoding[s[i*4]]
		a1 := decoding[s[i*4+1]]
		a2 := decoding[s[i*4+2]]
		a3 := decoding[s[i*4+3]]
		if a0 == 0 || a1 == 0 || a2 == 0 || a3 == 0 {
			ok = false
			return
		}

		var group uint32
		group = uint32(a0-1) << 18
		group += uint32(a1-1) << 12
		group += uint32(a2-1) << 6
		group += uint32(a3 - 1)
		p.Bits[j] = uint8(group >> 16)
		p.Bits[j+1] = uint8(group >> 8)
		p.Bits[j+2] = uint8(group)
		j += 3
	}

	// stub group
	a0 := decoding[s[40]]
	a1 := decoding[s[41]]
	if a0 == 0 || a1 == 0 {
		ok = false
		return
	}

	var group uint32
	group = uint32(a0-1) << 10
	group += uint32(a1-1) << 4
	group += uint32(mt)
	p.Bits[j] = uint8(group >> 8)
	p.Bits[j+1] = uint8(group)
	ok = true
	return
}

// Stub Group
// ==========
//
//
//            byte                    byte
//            [30]                    [31]
// |                       |                          |
// |--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
// |	             : 	   |           :              |
// :                 :                 :<------------>|
// :<------ [40] --->:<------[41]----->:   metatype
//          char             char
//
//

// String returns the string representation of a ptr, which is a
// base64 encoding of 252 bits, followed by a letter based on the
// metatype.  The metatype code is 0-15, only some of which are used.
// Notably 1 is for untyped data, represented by a 'u' suffix
func (p Ptr) String() string {
	var out [42 + 1]byte

	outp := p.encodeASCII(out[:])
	return string(out[:outp])
}

func (p Ptr) encodeASCII(out []byte) int {

	// once we nick out the low 4 bits, there are 256-4 = 252 bits
	// left, which corresponds to 252/6 = 42 output chars.  We can
	// process it in 10 groups of 3 input bytes followed by a stub
	// group of 12 bits, which, combined with the metatype code
	// of four bits, is 256 bits (10*24 + 12 + 4 = 256)
	outp := 0

	// first 10 full groups

	for i := 0; i < 10; i++ {
		var group uint32
		group = uint32(p.Bits[i*3]) << 16
		group += uint32(p.Bits[i*3+1]) << 8
		group += uint32(p.Bits[i*3+2])

		out[outp] = encoding[63&(group>>18)]
		out[outp+1] = encoding[63&(group>>12)]
		out[outp+2] = encoding[63&(group>>6)]
		out[outp+3] = encoding[63&group]
		outp += 4
	}

	// stub group
	{
		var group uint32
		group = 0xff0 & (uint32(p.Bits[30]) << 4)
		group += 0xf & (uint32(p.Bits[31]) >> 4)
		out[outp] = encoding[63&(group>>6)]
		out[outp+1] = encoding[63&group]
		outp += 2
	}

	out[outp] = mtToChar(MetatypeCode(p.Bits[31] & 0xf))
	outp++
	return outp
}

const encoding = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"

var decoding = [256]uint8{
	'A': 1,
	'B': 2,
	'C': 3,
	'D': 4,
	'E': 5,
	'F': 6,
	'G': 7,
	'H': 8,
	'I': 9,
	'J': 10,
	'K': 11,
	'L': 12,
	'M': 13,
	'N': 14,
	'O': 15,
	'P': 16,
	'Q': 17,
	'R': 18,
	'S': 19,
	'T': 20,
	'U': 21,
	'V': 22,
	'W': 23,
	'X': 24,
	'Y': 25,
	'Z': 26,
	'a': 27,
	'b': 28,
	'c': 29,
	'd': 30,
	'e': 31,
	'f': 32,
	'g': 33,
	'h': 34,
	'i': 35,
	'j': 36,
	'k': 37,
	'l': 38,
	'm': 39,
	'n': 40,
	'o': 41,
	'p': 42,
	'q': 43,
	'r': 44,
	's': 45,
	't': 46,
	'u': 47,
	'v': 48,
	'w': 49,
	'x': 50,
	'y': 51,
	'z': 52,
	'0': 53,
	'1': 54,
	'2': 55,
	'3': 56,
	'4': 57,
	'5': 58,
	'6': 59,
	'7': 60,
	'8': 61,
	'9': 62,
	'-': 63,
	'_': 64,
}

func (p Ptr) Encode(dest []byte) (int, error) {
	copy(dest, p.Bits[:])
	return PtrSize, nil
}

func (p *Ptr) Decode(buf []byte) (int, error) {
	mtc := mtToChar(MetatypeCode(buf[31] & 0xf))
	if mtc == '?' {
		return 0, ErrInvalidMetatype
	}

	copy(p.Bits[:], buf)
	return PtrSize, nil
}

var ErrInvalidPtr = errors.New("invalid ptr")

func (p *Ptr) UnmarshalJSON(src []byte) error {
	// unmarshal an empty string as the null ptr
	if len(src) == 2 && src[0] == '"' && src[1] == '"' {
		*p = Null
		return nil
	}

	if len(src) != 45 || src[0] != '"' || src[44] != '"' {
		return ErrInvalidPtr
	}
	d, ok := DecodeStringPtr(string(src[1:44]))
	if !ok {
		return ErrInvalidPtr
	}
	*p = d
	return nil
}

func (p Ptr) MarshalJSON() ([]byte, error) {

	// marshal the null ptr as the empty string
	if p.IsNull() {
		return []byte{'"', '"'}, nil
	}

	ch := mtToChar(MetatypeCode(p.Bits[31] & 0xf))
	if ch == '?' {
		return nil, ErrInvalidPtr
	}
	buf := make([]byte, 45)
	buf[0] = '"'
	buf[44] = '"'
	p.encodeASCII(buf[1:44])
	return buf, nil
}
