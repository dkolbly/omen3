// Memoizing functional computation
package cache

import (
	"context"
	"fmt"
	"io"
	"sync"
	"time"

	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
)

type Cache struct {
	lock sync.Mutex
	// turn off state caching for now, until we figure out a
	// good approach to pushing state from backend to client
	//state  map[string]*db.State
	cam    map[ptr.Ptr]*db.Atom
	loader GetLoader
	notify chan []*db.State
}

type Option func(context.Context, *Cache) error

func New(ctx context.Context, opts ...Option) (*Cache, error) {
	c := &Cache{
		//state:  make(map[string]*db.State, 100),
		cam:    make(map[ptr.Ptr]*db.Atom, 1000),
		notify: make(chan []*db.State, 2),
	}
	for _, opt := range opts {
		err := opt(ctx, c)
		if err != nil {
			return nil, err
		}
	}

	if ns, ok := c.loader.(NeedStuffinger); ok {
		go c.monitorstuff(ns)
	}

	return c, nil
}

type NeedStuffinger interface {
	NeedStuffing() <-chan *db.Atom
}

func (c *Cache) monitorstuff(ns NeedStuffinger) {
	for a := range ns.NeedStuffing() {
		c.lock.Lock()
		c.cam[a.Addr] = a
		c.lock.Unlock()
	}
}

type GetLoader interface {
	Obs(ctx context.Context, name string) (*db.State, error)
	Load(ctx context.Context, addr ptr.Ptr) (*db.Atom, error)
	Alloc(ctx context.Context) uint64
	//Updates() <-chan []*db.State
}

func (c *Cache) Updates() <-chan []*db.State {
	return c.notify
}

func (c *Cache) load(ctx context.Context, addr ptr.Ptr) (*db.Atom, error) {
	c.lock.Lock()
	atom, ok := c.cam[addr]
	c.lock.Unlock()
	if ok {
		return atom, nil
	}

	if c.loader == nil {
		panic("no backing loader")
	}
	atom, err := c.loader.Load(ctx, addr)
	if err != nil {
		return nil, err
	}
	c.lock.Lock()
	c.cam[addr] = atom
	c.lock.Unlock()
	return atom, nil
}

/*func (c *Cache) UpdatedN(item ...*db.State) {
	c.Updated(item)
}

func (c *Cache) Updated(lst []*db.State) {
	c.lock.Lock()
	for _, s := range lst {
		c.state[s.Name] = s
	}
	c.lock.Unlock()
	c.notify <- lst
}
*/
func (c *Cache) obs(ctx context.Context, name string) (*db.State, error) {
	return c.loader.Obs(ctx, name)
	/*
		c.lock.Lock()
		s := c.state[name]
		c.lock.Unlock()

		if s != nil {
			return s, nil
		}

		if c.loader == nil {
			panic("no backing loader")
		}
		s, err := c.loader.Obs(ctx, name)
		if err != nil {
			return nil, err
		}

		if err != nil {
			panic(err) // RATS, could not load it
		}

		// put it in the cache
		c.lock.Lock()
		c.state[name] = s
		c.lock.Unlock()
		return s, nil
	*/
}

func LoadFrom(l GetLoader) Option {
	return func(ctx context.Context, c *Cache) error {
		c.loader = l
		return nil
	}
}

type Tx struct {
	base  *Cache
	outer *Tx
	visit map[string]*db.State
}

func (tx *Tx) Using() map[string]*db.State {
	return tx.visit
}

/*
type Checker struct {
	base  *Cache
	index map[string]*db.State
}

func (ch *Checker) Match() bool {
	c := ch.base
	for k, v := range ch.index {
		cur := c.get(k)
		if cur.Value != v.Value {
			return false
		}
	}
	return true
}

func (tx *Tx) Checker() *Checker {
	return &Checker{
		base:  tx.base,
		index: tx.visit,
	}
}

func (tx *Tx) Implicit(ch *Checker) {
	for k, v := range ch.index {
		tx.visit[k] = v
	}
	if tx.outer != nil {
		tx.outer.Implicit(ch)
	}
}

*/

func (tx *Tx) Load(ctx context.Context, addr ptr.Ptr) (*db.Atom, error) {
	return tx.base.load(ctx, addr)
}

func (tx *Tx) Obs(ctx context.Context, name string) (*db.State, error) {
	s := tx.visit[name]
	if s != nil {
		// it's already been visited at this layer; stop now
		return s, nil
	}

	// pull it from the outer layer, if any
	var err error

	if tx.outer != nil {
		s, err = tx.outer.Obs(ctx, name)
	} else {
		s, err = tx.base.obs(ctx, name)
	}
	// make sure it's visited at this layer
	tx.visit[name] = s
	return s, err
}

func (ic *Cache) Begin() *Tx {
	return &Tx{
		base:  ic,
		visit: make(map[string]*db.State),
	}
}

func (tx *Tx) BeginTop() *Tx {
	return &Tx{
		base:  tx.base,
		visit: make(map[string]*db.State),
	}
}

func (tx *Tx) Begin() *Tx {
	return &Tx{
		outer: tx,
		base:  tx.base,
		visit: make(map[string]*db.State),
	}
}

func (tx *Tx) Dump(dst io.Writer) {
	fmt.Fprintf(dst, "%d visited:\n", len(tx.visit))
	for k, v := range tx.visit {
		if v.Value.IsNull() {
			fmt.Fprintf(dst, "   <%s> missing\n", k)
		} else {
			fmt.Fprintf(dst, "   <%s> #%d value: %s\n",
				k, v.Sequence, v.Value)
		}
	}
}

// hack for testing; stuff untyped data block into cam
func (c *Cache) StuffUntyped(blob string) ptr.Ptr {
	buf := []byte(blob)
	addr := ptr.ContentAddress(ptr.Untyped, buf)
	c.cam[addr] = &db.Atom{
		Addr:    addr,
		Payload: buf,
	}
	return addr
}

func (c *Cache) Alloc(ctx context.Context) uint64 {
	// TODO actually cache allocation chunks
	return c.loader.(db.Store).Alloc(ctx)
}

func (c *Cache) Apply(ctx context.Context, txr *db.TxnRecord) (uint64, time.Time, []*db.State, error) {
	return c.loader.(db.Store).Apply(ctx, txr)
}

func (c *Cache) Load(ctx context.Context, addr ptr.Ptr) (*db.Atom, error) {
	return c.load(ctx, addr)
}

func (c *Cache) Obs(ctx context.Context, name string) (*db.State, error) {
	return c.obs(ctx, name)
}

func (c *Cache) Store(ctx context.Context, dt db.DataType, buf []byte) (*db.Atom, error) {
	addr := db.PtrForData(dt, buf)
	c.lock.Lock()
	existing, ok := c.cam[addr]
	c.lock.Unlock()
	if ok {
		// hit in the cache... no need to send to the underlying store
		return existing, nil
	}
	// this will panic if our loader is not also a storer
	a, err := c.loader.(db.Storer).Store(ctx, dt, buf)
	if err == nil {
		if a.Addr != addr {
			panic("addr mismatch")
		}
		c.lock.Lock()
		if existing, ok := c.cam[addr]; ok {
			a = existing
		} else {
			c.cam[addr] = a
		}
		c.lock.Unlock()
	}
	return a, err
}

type Walker interface {
	Walk(ctx context.Context, roots []string) ([]*db.State, error)
}

func (c *Cache) Walk(ctx context.Context, roots []string) ([]*db.State, error) {
	if w, ok := c.loader.(Walker); ok {
		return w.Walk(ctx, roots)
	}
	panic("TODO... implement locally at this layer, or return error?")
}
