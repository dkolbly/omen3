package main

import (
	"context"
	"os"
	"strings"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/lib/rest"
	cache "bitbucket.org/dkolbly/omen3/lib/statecache"
)

var log = logging.New("sw")

func main() {
	logging.SetHumanOutput(false, false, "DEBUG")
	logging.PrettyShowExtraFields()

	r := rest.Dial("http://localhost:8444")

	sc := cache.New(r)

	ctx := context.Background()

	poll := false
	args := os.Args[1:]

	if args[0] == "-poll" {
		poll = true
		args = args[1:]
	}

	if poll {
		scan := func() {
			for _, arg := range args {
				state, err := sc.Get(ctx, arg)
				if err != nil {
					log.Warningf(ctx, "%s : %s", arg, err)
				} else {
					log.Infof(ctx, "%s --> #%d %s", arg, state.Sequence, state.Value)
				}
			}
		}

		scan()
		for range time.NewTicker(time.Second).C {
			scan()
		}
	} else {
		for i, arg := range args {
			go watch1(sc, i, arg)
		}
		<-time.After(100 * time.Hour)
	}
}

func watch1(sc *cache.StateCache, i int, arg string) {
	ctx := logging.Set(context.Background(), "arg", i)

	var lst []*db.State
	index := make(map[string]int)

	for i, name := range strings.Split(arg, ",") {
		lst = append(lst, &db.State{Name: name})
		index[name] = i
	}
	for {
		t := time.Now()
		ret, err := sc.WaitForChange(ctx, lst)
		if err != nil {
			panic(err)
		}
		log.Debugf(ctx, "wait ended after %s", time.Since(t))
		for i, s := range ret {
			log.Infof(ctx, "(%d) %s --> #%d %s", i, s.Name, s.Sequence, s.Value)
			if k, ok := index[s.Name]; ok {
				lst[k].Sequence = s.Sequence
			} else {
				panic("oops")
			}
		}

	}
}
