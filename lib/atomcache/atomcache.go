package atomcache

import (
	"container/list"
	"context"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
)

var log = logging.New("atomcache")

type entry struct {
	first  time.Time
	latest time.Time
	elem   *list.Element
	atom   *db.Atom
}

type atomCache struct {
	lock   sync.Mutex
	cache  map[ptr.Ptr]*entry
	aging  *list.List
	stored int
	next   db.AtomSupplier
	limit  int
}

func New(next db.AtomSupplier, limit int) db.AtomSupplier {
	return &atomCache{
		cache: make(map[ptr.Ptr]*entry),
		aging: list.New(),
		next:  next,
		limit: limit,
	}
}

func (a *atomCache) Store(ctx context.Context, dt db.DataType, data []byte) (*db.Atom, error) {
	t := time.Now()
	addr := db.PtrForData(dt, data)

	e := a.bump(t, addr)
	if e != nil {
		// it's already been stored, skip it (no need to hit the network)
		return e, nil
	}

	u, err := a.next.Store(ctx, dt, data)
	if err != nil {
		return nil, err
	}

	if len(data) >= a.limit/2 {
		// don't cache things at all that are more than half the limit size
		return u, nil
	}
	return a.encache(ctx, t, u), nil
}

func (a *atomCache) encache(ctx context.Context, t time.Time, u *db.Atom) *db.Atom {

	a.lock.Lock()
	defer a.lock.Unlock()

	e, ok := a.cache[u.Addr]
	if ok {
		// it has been cached by someone else *while* we were thinking
		// about it
		return e.atom
	}

	// create an entry for it
	e = &entry{
		first:  t,
		latest: t,
		atom:   u,
	}
	e.elem = a.aging.PushFront(e)
	a.cache[u.Addr] = e

	a.stored += len(u.Payload)

	for a.stored > a.limit {
		last := a.aging.Remove(a.aging.Back()).(*entry)
		log.Debugf(ctx, "stored %d > limit %d ; removing %s (%d bytes)",
			a.stored, a.limit, last.atom.Addr, len(last.atom.Payload))
		delete(a.cache, last.atom.Addr)
		a.stored -= len(last.atom.Payload)
	}

	return u
}

func (a *atomCache) bump(t time.Time, p ptr.Ptr) *db.Atom {
	a.lock.Lock()
	e, ok := a.cache[p]
	if ok {
		// move it to the front
		a.aging.MoveToFront(e.elem)
		e.latest = t
	}
	a.lock.Unlock()
	if ok {
		// we found it in cache... return it
		return e.atom
	}
	return nil
}

func (a *atomCache) Load(ctx context.Context, p ptr.Ptr) (*db.Atom, error) {
	t := time.Now()
	u := a.bump(t, p)
	if u != nil {
		return u, nil
	}

	// we did not find it... pull it from the next supplier
	u, err := a.next.Load(ctx, p)
	if err != nil {
		return nil, err
	}
	return a.encache(ctx, t, u), nil
}
