package reachable

import (
	"context"
	"io/ioutil"
	"testing"

	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/lib/omentest"
	"bitbucket.org/dkolbly/omen3/lib/protodecode"
	pdt "bitbucket.org/dkolbly/omen3/lib/protodecode/test"
	"bitbucket.org/dkolbly/omen3/ptr"
	"github.com/golang/protobuf/proto"
)

var fset *protodecode.FileSet

func init() {
	buf, err := ioutil.ReadFile("../protodecode/test/pbtypes.pb")
	if err != nil {
		panic(err)
	}
	fset = protodecode.DeclareProtobufFileSet(buf)
}

func save(ctx context.Context, sto db.Storer, debug string, item proto.Message, crypt *protodecode.Crypter) ptr.Ptr {
	buf, _ := proto.Marshal(item)
	dt := protodecode.DataTypeForMessage(item)

	for _, extra := range dt.Atoms() {
		var t db.DataType
		if !extra.Type.IsNull() {
			t = db.ScanlessType(extra.Type)
		}
		a2, err := sto.Store(ctx, t, extra.Payload)
		if err != nil {
			panic(err)
		}
		if a2.Addr != extra.Addr {
			panic("addr mismatch")
		}
	}
	if crypt != nil {
		sto = crypt.Storer(sto)
	}

	a, _ := sto.Store(ctx, dt, buf)
	return a.Addr
}

func hydratePtr(p ptr.Ptr) *pdt.Ptr {
	return &pdt.Ptr{
		Addr: p.Bits[:],
	}
}

var childname = []string{"first", "second", "third", "fourth", "fifth"}

func testGraph(ctx context.Context, tx *omentest.TestTx, c *protodecode.Crypter, name string, children ...ptr.Ptr) ptr.Ptr {

	me := &pdt.Node{
		Name: name,
	}
	for i, ch := range children {
		sub := &pdt.Node{
			Name:      childname[i],
			Reference: hydratePtr(ch),
		}
		me.Children = append(me.Children, sub)
	}
	p := save(ctx, tx, name, me, c)
	//fmt.Printf("Node(%s) is %s\n", name, p)
	return p
}

func setup(dag bool, c *protodecode.Crypter) (*omentest.Omen, ptr.Ptr) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)

	left := testGraph(
		ctx,
		tx,
		c,
		"left-interior",
		testGraph(ctx, tx, c, "l-bottom-left"),
		testGraph(ctx, tx, c, "l-bottom-middle"),
		testGraph(ctx, tx, c, "l-bottom-right"),
	)
	var right ptr.Ptr
	if dag {
		right = left
	} else {
		right = testGraph(
			ctx,
			tx,
			c,
			"right-interior",
			testGraph(ctx, tx, c, "r-bottom-left"),
			testGraph(ctx, tx, c, "r-bottom-middle"),
			testGraph(ctx, tx, c, "r-bottom-right"),
		)
	}

	top := testGraph(ctx, tx, c, "top", left, right)
	tx.Commit(ctx)
	return o, top
}

func TestRecursiveTreeWalk(t *testing.T) {
	o, top := setup(false, nil)
	scanner := New([]ptr.Ptr{top}, -1)

	ctx := context.Background()
	tx := o.Begin(ctx)

	count := 0
	for range scanner.Run(ctx, tx) {
		count++
	}
	expect := 11
	if count != expect {
		t.Fatalf("expected %d pointers, scanned %d", expect, count)
	}
}

func TestRecursiveDAGWalk(t *testing.T) {
	o, top := setup(true, nil)
	scanner := New([]ptr.Ptr{top}, -1)

	ctx := context.Background()
	tx := o.Begin(ctx)

	count := 0
	for range scanner.Run(ctx, tx) {
		count++
	}
	expect := 7
	if count != expect {
		t.Fatalf("expected %d pointers, scanned %d", expect, count)
	}
}

func TestNoReturningLiterals(t *testing.T) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)
	top := testGraph(
		ctx,
		tx,
		nil,
		"top",
		ptr.String("left"),
		ptr.String("right"),
	)
	tx.Commit(ctx)

	scanner := New([]ptr.Ptr{top}, -1)

	tx = o.Begin(ctx)

	count := 0
	for range scanner.Run(ctx, tx) {
		count++
	}
	expect := 3 // root, plus the two objects in support of the type system
	if count != expect {
		t.Fatalf("expected %d pointers, scanned %d", expect, count)
	}
}

func TestNoRecurseOfUntypedPtrs(t *testing.T) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)

	plainType := db.ScanlessType(ptr.String("mime:t/plain"))
	untyped1, _ := tx.Store(ctx, nil, []byte("This is untyped data"))
	littyped, _ := tx.Store(ctx, plainType, []byte("This is slightly typed data"))

	top := testGraph(
		ctx,
		tx,
		nil,
		"top",
		untyped1.Addr,
		littyped.Addr,
	)
	tx.Commit(ctx)

	scanner := New([]ptr.Ptr{top}, -1)

	tx = o.Begin(ctx)

	count := 0
	for range scanner.Run(ctx, tx) {
		count++
	}
	expect := 1 + 2 + 2 // top + two unexpanded objects + two type objects
	if count != expect {
		t.Fatalf("expected %d pointers, scanned %d", expect, count)
	}
}

func TestBoundedRecursionTreeWalk(t *testing.T) {
	brtreewalk(t, false, []int{1, 4, 11, 11})
}

func TestBoundedRecursionDAGWalk(t *testing.T) {
	brtreewalk(t, true, []int{1, 3, 7, 7})
}

func brtreewalk(t *testing.T, dag bool, expects []int) {
	o, top := setup(dag, nil)

	for depth, expect := range expects {

		scanner := New([]ptr.Ptr{top}, depth)

		ctx := context.Background()
		tx := o.Begin(ctx)

		count := 0
		for range scanner.Run(ctx, tx) {
			count++
		}
		//fmt.Printf("at depth %d scanned %d\n", depth, count)
		if count != expect {
			t.Fatalf("expected %d pointers, scanned %d", expect, count)
		}
	}
}

func crypter() *protodecode.Crypter {
	d := fset.NewProtoDecoder(".protodecodetest.Node")
	if d == nil {
		panic("no message type Node")
	}

	scan := d.CompileScanner()
	return scan.NewCrypterFromPassword("hello")
}

func TestEncryptedDAGWalk(t *testing.T) {
	c := crypter()
	o, top := setup(true, c)
	ctx := context.Background()

	tx := o.Begin(ctx)
	scanner := New([]ptr.Ptr{top}, -1)
	count := 0

	for range scanner.Run(ctx, tx) {
		count++
	}
	expect := 7
	if count != expect {
		t.Fatalf("expected %d pointers, scanned %d", expect, count)
	}

}
