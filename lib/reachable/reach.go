package reachable

import (
	"context"
	"fmt"

	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
)

// A Scan keeps track of a reachability analysis.  It has an index
// of already visited objects so that the same Ptr is not returned
// multiple times.  Only loadable pointers are returned.
type Scan struct {
	queue   [][]ptr.Ptr
	visited map[ptr.Ptr]struct{}
	errors  int
	depth   int
}

func New(roots []ptr.Ptr, depth int) *Scan {
	// make a copy because we're going to use it as our queue
	q := make([]ptr.Ptr, 0, len(roots)+10)
	visited := make(map[ptr.Ptr]struct{})

	for _, root := range roots {
		if root.IsLoadable() {
			if _, ok := visited[root]; !ok {
				q = append(q, root)
				visited[root] = struct{}{}
			}
		}
	}
	return &Scan{
		queue:   [][]ptr.Ptr{q},
		visited: visited,
		depth:   depth,
	}
}

// Run starts scanning the graph.  Only one Run can be active at once;
// Scan itself is not concurrency aware
func (s *Scan) Run(ctx context.Context, store db.Loader) <-chan ptr.Ptr {
	ch := make(chan ptr.Ptr)
	go s.pump(ctx, ch, store)
	return ch
}

func (s *Scan) pump(ctx context.Context, dst chan<- ptr.Ptr, src db.Loader) {
	defer close(dst)
	s.pumpatdepth(ctx, dst, src, 0)
}

func (s *Scan) pumpatdepth(ctx context.Context, dst chan<- ptr.Ptr, src db.Loader, d int) {
	if d >= len(s.queue) {
		return
	}

	// flush this depth
	var next []ptr.Ptr
	if d+1 < len(s.queue) {
		next = s.queue[d+1]
	}

	current := s.queue[d]
	s.queue[d] = nil

	recur := s.depth < 0 || d < s.depth

	for i, p := range current {
		select {

		case <-ctx.Done():
			s.queue[d] = current[i:]
			return

		case dst <- p:
			if recur && p.Metatype() == ptr.HasTypePointer {
				next = s.recurse(ctx, p, next, src)
			}
		}
	}

	if len(next) > 0 {
		if d+1 < len(s.queue) {
			s.queue[d+1] = next
		} else {
			s.queue = append(s.queue, next)
		}
	}

	// now go on to the next depth
	s.pumpatdepth(ctx, dst, src, d+1)
	return
}

func (s *Scan) recurse(ctx context.Context, p ptr.Ptr, dst []ptr.Ptr, src db.Loader) []ptr.Ptr {

	// we need to load it, if nothing else to get its type pointer
	atom, err := src.Load(ctx, p)
	if err != nil {
		// TODO warning
		s.errors++
		return dst
	}
	fmt.Printf("Recurse from <%s> (loaded %d bytes with type %s)\n", p, len(atom.Payload), atom.Type)

	dt, err := db.HydrateDataType(ctx, atom.Type, src)
	fmt.Printf("type for %s is %T\n", atom.Type, dt)

	/*
		// see if the type pointer *itself* is loadable
		var typeatom *db.Atom
		if atom.Type.IsLoadable() {
			typeatom, err = src.Load(ctx, atom.Type)
			if err != nil {
				fmt.Printf("Load of type data <%s> failed\n", atom.Type)
				s.errors++
				return dst
			}
		}

		dt, ok, err := protodecode.DataTypeForType(ctx, atom.Type, typeatom, src)
		if !ok || err != nil {
			s.errors++
			return dst
		}
	*/

	lst, err := db.Scan(dt, atom.Payload)
	if err != nil {
		panic(err)
		s.errors++
		return dst
	}

	for _, next := range lst {
		if !next.IsLoadable() {
			// Scan() should not have returned a non-loadable ptr :/
			// but if it screws up, we don't want to pass along that
			// screwup
			fmt.Printf("OOPS: %s is not loadable\n", next)
			panic("scan should not return non-loadable ptrs")
			continue
		}
		if _, ok := s.visited[next]; !ok {
			dst = append(dst, next)
			s.visited[next] = struct{}{}
		}
	}
	return dst
}
