package incore

type Empty struct{}

/*
// An Assignment keeps track of where a given observable
// is currently resident

type Assignment struct {
	addr omen.Addr
	requests chan <- req
}

type req struct {
	release bool
	reply chan Empty
	requestor *Link
}

func (a *Assignment) run(ch <- chan req) {
	var borrowed *Link

	for r := range ch {
		if r.release {
			borrowed = nil
			continue
		}
		if borrowed != nil {
			borrowed.retrieve(a.addr)
		}
		r.reply <- Empty{}
		borrowed = r.requestor
	}
}

// An Actor manages a single observable that this
// process has temporary ownership of
type Actor struct {
	name omen.Addr
	requests chan <- req
}

// a req is a request to use the observable.
type req struct {
	reply chan ObservableState
	requestor *Node
}

func (a *Actor) run(ch <- chan req) {
	state := load(a.name)
	var lessor *Node

	for r := range ch {
		if lessor != nil {
			state <- lessor.retrieve(a.name)
		}
		lessor = r.requestor
		r.reply <- state
	}
}
*/
