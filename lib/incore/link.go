package incore

import (
	"context"
	"encoding/binary"
	"fmt"
	"net"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
)

var log = logging.New("incore")

const idletime = 5 * time.Second

// A Link is an external connection along which assignment
// requests can arrive
type Link struct {
	conn     *net.TCPConn
	tx       chan<- packet
	dispatch DispatchTable
	rxseq    int64
	lock     sync.Mutex
	cancel   func()
}

type DispatchTable map[byte]func(context.Context, []byte)

type packet struct {
	header uint32
	wire   []byte // *includes* header
}

func LinkServer(addr string, disp DispatchTable) error {
	local, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		panic(err)
	}

	lis, err := net.ListenTCP("tcp", local)
	if err != nil {
		panic(err)
	}
	for {
		con, err := lis.AcceptTCP()
		if err != nil {
			panic(err)
		}
		linkup(con, disp)
	}

}

func LinkClient(addr string, disp DispatchTable) *Link {
	ctx := context.Background()
	log.Debugf(ctx, "link client for: %s", addr)

	remote, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		panic(err)
	}

	conn, err := net.DialTCP("tcp", nil, remote)
	if err != nil {
		panic(err)
	}
	log.Debugf(ctx, "up and running")
	return linkup(conn, disp)
}

func linkup(conn *net.TCPConn, disp DispatchTable) *Link {
	ctx, cancel := context.WithCancel(context.Background())
	ctx = logging.Set(ctx, "peer_addr", conn.RemoteAddr().String())

	log.Debugf(ctx, "bringing up link to %s", conn.RemoteAddr())

	ch := make(chan packet, 50)
	link := &Link{
		conn:     conn,
		tx:       ch,
		dispatch: disp,
		cancel:   cancel,
	}

	go link.rxloop(ctx)
	go link.txloop(ctx, ch)
	go link.idle(ctx)
	return link
}

const (
	OpIdle = 'i'
)

func mkcheck(a, b, c byte) uint8 {
	return 0xe5 ^ (a + b + c)
}

func (link *Link) Send(op byte, data []byte) {
	wire := make([]byte, len(data)+4)
	wire[0] = op
	if len(data) > 1000 {
		panic("too much data")
	}

	binary.BigEndian.PutUint16(wire[2:4], uint16(len(data)))
	wire[1] = mkcheck(wire[0], wire[2], wire[3])

	copy(wire[4:], data)

	link.tx <- packet{
		header: binary.BigEndian.Uint32(wire[0:4]),
		wire:   wire,
	}
}

func (link *Link) shutdown() {
	link.lock.Lock()
	defer link.lock.Unlock()

	// close the network layer
	link.conn.Close()

	// mark the txer as closed
	link.tx = nil

	// shut down the idler and rxloop (the idler will close the txloop)
	link.cancel()
}

func (link *Link) idle(ctx context.Context) {
	ticker := time.NewTicker(time.Second)
	tx := link.tx

	defer close(tx)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			log.Debugf(ctx, "stopping idle loop")
			return

		case t := <-ticker.C:
			buf := make([]byte, 12)
			buf[0] = OpIdle
			buf[3] = 8
			buf[1] = mkcheck(buf[0], buf[2], buf[3])
			binary.BigEndian.PutUint64(buf[4:], uint64(t.UnixNano()))
			tx <- packet{
				header: binary.BigEndian.Uint32(buf[0:4]),
				wire:   buf[:],
			}
		}
	}
}

func (link *Link) txloop(ctx context.Context, ch <-chan packet) {
	for p := range ch {
		link.conn.Write(p.wire)
	}
	log.Debugf(ctx, "txloop is done")
}

func (link *Link) rxloop(ctx context.Context) {
	conn := link.conn
	var tmp [4096]byte

	//fmt.Printf("run %s -> %s\n", conn.LocalAddr(), conn.RemoteAddr())

	fill := 0

	for {
		conn.SetReadDeadline(time.Now().Add(idletime))

		//fmt.Printf("fill is %d\n", fill)
		n, err := conn.Read(tmp[fill:])
		if err != nil {
			link.shutdown()
			return
		}
		fill += n
		//fmt.Printf("but got %d bytes, total is now %d: %q\n", n, fill, tmp[:fill])
		remain := link.consumeAll(ctx, tmp[:fill])
		//fmt.Printf("after consuming, %d remain\n", len(remain))
		copy(tmp[:], remain)
		fill = len(remain)
	}
}

func (link *Link) consumeAll(ctx context.Context, buf []byte) []byte {
	for len(buf) >= 4 {
		header := binary.BigEndian.Uint32(buf[:4])
		recordLen := int(header & 0xfff)
		if len(buf) < 4+recordLen {
			// an entire record is not here yet
			return buf
		}
		link.rxseq++
		link.consumeRecord(logging.Set(ctx, "rx_seq", link.rxseq), header, buf[:4+recordLen])
		buf = buf[4+recordLen:]
	}
	return buf
}

func (link *Link) consumeRecord(ctx context.Context, header uint32, rec []byte) {
	recordType := uint8(header >> 24)
	check := uint8(header >> 16)
	if fn, ok := link.dispatch[recordType]; ok {
		fmt.Printf("got record type=%#x chk=%#x data=%x\n", recordType, check, rec)
		fn(ctx, rec[4:])
	} else if recordType == OpIdle {
		fmt.Printf("got record type=%#x chk=%#x data=%x IDLE\n", recordType, check, rec)
	} else {
		fmt.Printf("got record type=%#x chk=%#x data=%x UNKNOWN\n", recordType, check, rec)
	}
}
