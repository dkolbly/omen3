package filestore

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"strings"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/lib/filestore/serz"
	"bitbucket.org/dkolbly/omen3/ptr"
	"github.com/golang/protobuf/proto"
)

var log = logging.New("filestore")

const firstSequence = 100 // start sequence at 100 to provide some specials
// (in particular, sequence 1 denotes an initial missing value that
// has already been observed)

var ErrStateCorrupt = errors.New("state is corrupted")

type Dir struct {
	base     string
	creates  int
	updates  int
	stores   int
	lock     sync.Mutex
	nexttxid uint64
}

func New(dir string) *Dir {
	return &Dir{
		base:     dir,
		nexttxid: loadid(dir),
	}
}

func (d *Dir) allocid() uint64 {
	id := d.nexttxid
	d.nexttxid++
	line := fmt.Sprintf("%d\n", d.nexttxid)
	ioutil.WriteFile(path.Join(d.base, "next.dat"), []byte(line), 0666)
	return id
}

func loadid(d string) uint64 {
	buf, err := ioutil.ReadFile(path.Join(d, "next.dat"))
	if err != nil {
		return 1000000
	}
	var k uint64
	n, err := fmt.Sscanf(string(buf), "%d", &k)
	if err != nil || n != 1 {
		panic("oops")
	}
	return k
}

func (d *Dir) StoreCount() int {
	return d.stores
}

type ErrNotLoadable ptr.Ptr

func (err ErrNotLoadable) Error() string {
	return fmt.Sprintf("address {%s} is not loadable", ptr.Ptr(err))
}

func (d *Dir) drop(dest ptr.Ptr) error {
	s := dest.String()
	fdir := path.Join(d.base, fmt.Sprintf("%c%c.%c", s[0], s[1], s[42]))
	filename := path.Join(fdir, s)
	return os.Remove(filename)
}

func (d *Dir) read(dest ptr.Ptr) ([]byte, error) {
	s := dest.String()
	fdir := path.Join(d.base, fmt.Sprintf("%c%c.%c", s[0], s[1], s[42]))
	filename := path.Join(fdir, s)

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, ptr.ErrNotPresent(dest)
		}
	}
	return data, err
}

func (d *Dir) Load(ctx context.Context, addr ptr.Ptr) (*db.Atom, error) {

	if !addr.IsLoadable() {
		return nil, ErrNotLoadable(addr)
	}

	data, err := d.read(addr)
	if err != nil {
		return nil, err
	}

	var typePtr ptr.Ptr
	if addr.IsTyped() {
		copy(typePtr.Bits[:], data[:ptr.PtrSize])
		data = data[ptr.PtrSize:]
	}
	return &db.Atom{
		Addr:    addr,
		Type:    typePtr,
		Payload: data,
	}, nil
}

func (d *Dir) write(dest ptr.Ptr, prefix, data []byte, replace bool) (bool, error) {
	s := dest.String()
	fdir := path.Join(d.base, fmt.Sprintf("%c%c.%c", s[0], s[1], s[42]))
	filename := path.Join(fdir, s)

	isnew := true

	_, err := os.Stat(filename)
	if err == nil {
		if replace {
			isnew = false
		} else {
			return false, nil
		}
	}

	os.Mkdir(fdir, 0777)

	fd, err := os.Create(filename + "~")
	if err != nil {
		return true, err
	}
	if prefix != nil {
		fd.Write(prefix)
	}
	fd.Write(data)
	fd.Close() // we have to close before the rename... thanks, Windows

	return isnew, os.Rename(filename+"~", filename)
}

func (d *Dir) Store(ctx context.Context, t db.DataType, buf []byte) (*db.Atom, error) {

	var a ptr.Ptr
	var typePtr ptr.Ptr

	created := false

	var err error

	if t == nil {
		a = ptr.ContentAddress(ptr.Untyped, buf)
		created, err = d.write(a, nil, buf, false)
	} else {
		typePtr = t.Addr()
		a = ptr.TypedContentAddress(typePtr, buf)
		created, err = d.write(a, typePtr.Bits[:], buf, false)
	}
	if err != nil {
		return nil, err
	}

	if created {
		d.stores++
	}

	return &db.Atom{
		Addr:    a,
		Type:    typePtr,
		Payload: buf,
	}, nil
}

// the db.Store name for Get()
func (d *Dir) Obs(ctx context.Context, name string) (*db.State, error) {
	return d.Get(ctx, name)
}

func (d *Dir) Get(ctx context.Context, name string) (*db.State, error) {
	return d.GetAddr(ptr.Obs(name))
}

func (d *Dir) GetAddr(addr ptr.Ptr) (*db.State, error) {
	buf, err := d.read(addr)

	if err != nil {
		return nil, err
	}

	var ob serz.ObsState
	err = proto.Unmarshal(buf, &ob)
	if err != nil {
		return nil, err
	}
	if ob.Name == "" || ob.Seq < 1 {
		return nil, ErrStateCorrupt
	}
	// this is a version migration trick... if we read an "old"
	// obs off disk and it has too early of a sequence number,
	// bump it up
	if ob.Seq < firstSequence {
		ob.Seq = firstSequence
	}

	s := &db.State{
		Name:     ob.Name,
		Audit:    ob.Audit,
		Sequence: ob.Seq,
		Modified: time.Unix(0, int64(ob.ModTime)),
	}
	copy(s.Value.Bits[:], ob.Value)
	return s, nil
}

func (d *Dir) Alloc(ctx context.Context) uint64 {
	d.lock.Lock()
	defer d.lock.Unlock()
	return d.allocid()
}

func (d *Dir) Apply(ctx context.Context, t0 *db.TxnRecord) (uint64, time.Time, []*db.State, error) {
	txn := *t0
	d.lock.Lock()
	defer d.lock.Unlock()

	// check the prereqs
	for _, pre := range txn.Prereqs {
		state, err := d.Get(ctx, pre.Name)
		if err != nil {
			if ptr.IsNotPresent(err) {
				if !pre.Value.IsNull() {
					log.Debugf(ctx, "{%s} is not set, but prereq value is %s",
						pre.Name,
						pre.Value)
					return 0, time.Time{}, nil, db.ErrConcurrencyFailure
				}
			} else {
				// some other error, which is bad
				return 0, time.Time{}, nil, err
			}
		} else {
			if state.Value != pre.Value {
				if pre.Value.IsNull() {
					log.Debugf(ctx, "{%s} is set to %s, but prereq is null",
						pre.Name,
						state.Value)
				} else {
					log.Debugf(ctx, "{%s} is set to %s, but prereq is %s",
						pre.Name,
						state.Value,
						pre.Value)
				}
				return 0, time.Time{}, nil, db.ErrConcurrencyFailure
			}
		}
	}

	txn.ID = d.allocid()
	txn.Timestamp = time.Now()
	if txn.Audit == 0 {
		txn.Audit = txn.ID
	}

	log.Debugf(ctx, "Allocated txn id {%d} at %s ; audit %d",
		txn.ID, txn.Timestamp.Format(time.RFC3339Nano), txn.Audit)

	// apply the edits
	result := make([]*db.State, len(txn.Edits))

	for i, edit := range txn.Edits {
		/*if edit.Value.IsNull() {
			err := d.drop(ptr.Obs(edit.Name))
			if err != nil {
				if !os.IsNotExist(err) {
					return 0, time.Time{}, nil, err
				}
			}
			result[i] = &db.State{
				Name:     edit.Name,
				Modified: txn.Timestamp,
				Audit:    txn.Audit,
			}
		} else {*/
		result[i] = &db.State{
			Name:     edit.Name,
			Modified: txn.Timestamp,
			Value:    edit.Value,
			Audit:    txn.Audit,
		}
		err := d.Set(ctx, result[i])
		if err != nil && !ptr.IsNotPresent(err) {
			return 0, time.Time{}, nil, err
		}
		//}
	}
	return txn.ID, txn.Timestamp, result, nil
}

// note that this MUTATES the `s` argument to set the sequence number
func (d *Dir) Set(ctx context.Context, s *db.State) error {
	at := ptr.Obs(s.Name)

	cap := serz.ObsState{
		Name:    s.Name,
		ModTime: uint64(s.Modified.UnixNano()),
		Value:   s.Value.Bits[:],
		Audit:   s.Audit,
	}

	buf, err := d.read(at)

	if err == nil {
		var old serz.ObsState
		err = proto.Unmarshal(buf, &old)
		if err != nil {
			return err
		}
		cap.Seq = old.Seq + 1
		log.Debugf(ctx, "seq %d -> %d", old.Seq, cap.Seq)
	} else if ptr.IsNotPresent(err) {
		log.Debugf(ctx, "read absent")
		err = nil
		cap.Seq = firstSequence
	} else {
		log.Debugf(ctx, "read failure: %s", err)
		return err
	}

	s.Sequence = cap.Seq

	newbuf, err := proto.Marshal(&cap)
	if err != nil {
		panic(err)
	}
	created, err := d.write(at, nil, newbuf, true)
	if err != nil {
		panic(err)
		return err
	}
	if created {
		d.creates++
		log.Debugf(ctx, "created")
	} else {
		d.updates++
		log.Debugf(ctx, "updated")
	}
	return err
}

var noUpdatesEver = make(chan []*db.State)

func (d *Dir) Updates() <-chan []*db.State {
	return noUpdatesEver
}

func compileObsGlob(glob string) *regexp.Regexp {
	buf := &bytes.Buffer{}
	buf.WriteByte('^') // anchor to start of input
	prev := false
	for _, ch := range glob {
		if ch == '*' {
			if prev { // handle "**" in glob
				buf.WriteString(".*")
			} else {
				buf.WriteString("[^/:]*")
			}
			prev = true
		} else if ch == '?' {
			buf.WriteString("[^/:]")
		} else {
			buf.WriteRune(ch)
			prev = false
		}
	}
	buf.WriteByte('$') // anchor to end of input

	return regexp.MustCompile(buf.String())
}

func (d *Dir) Glob(ctx context.Context, glob string) ([]string, error) {
	lst, err := ioutil.ReadDir(d.base)
	if err != nil {
		return nil, err
	}
	re := compileObsGlob(glob)
	answer := []string{}
	for _, group := range lst {
		if strings.HasSuffix(group.Name(), ".o") {
			sub, err := ioutil.ReadDir(path.Join(d.base, group.Name()))
			if err != nil {
				return nil, err
			}
			for _, s := range sub {
				if oa, ok := ptr.DecodeStringPtr(s.Name()); ok {
					state, err := d.GetAddr(oa)
					if err == nil && re.MatchString(state.Name) {
						answer = append(answer, state.Name)
					}
				}
			}
		}
	}
	return answer, nil
}
