#! /bin/bash

set -x
set -e

if test -z "$GOPATH"
then GOPATH=$(readlink -f ../../../../..)
fi

protoc -I $GOPATH/src --go_out=plugins=grpc:$GOPATH/src \
       bitbucket.org/dkolbly/omen3/lib/filestore/serz/serz.proto
