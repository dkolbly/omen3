package romen

import (
	"context"
	"time"

	"bitbucket.org/dkolbly/omen3/api"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/rpc"
	"github.com/golang/protobuf/proto"
)

type Remote interface {
	WaitForReady(ctx context.Context)
	Send(ctx context.Context, endpoint string, tx proto.Message) error
	RPC(ctx context.Context, endpoint string, tx, rx proto.Message) error
}

type States struct {
	Connection Remote
}

func StatesOver(r Remote) States {
	return States{
		Connection: r,
	}
}

var backoff = []time.Duration{
	20 * time.Millisecond,
	100 * time.Millisecond,
	1000 * time.Millisecond,
}

func tryharder(ctx context.Context, conn Remote, ep string, tx, rx proto.Message) (err error) {
	for i := 0; i <= len(backoff); i++ {

		err = conn.RPC(ctx, ep, tx, rx)
		if err == nil || !rpc.IsRetryWorthy(err) {
			return
		}
		if i < len(backoff) {
			time.Sleep(backoff[i])
			conn.WaitForReady(ctx)
		}
	}
	return
}

func (s States) WaitForChange(ctx context.Context, current []*db.State) ([]*db.State, error) {
	q := api.WaitForChange{
		Items: make([]*api.State, len(current)),
	}
	for i, s := range current {
		q.Items[i] = &api.State{
			Name: s.Name,
			Seq:  s.Sequence,
		}
	}
	var r api.WaitForChange
	err := tryharder(ctx, s.Connection, "omen.state.WaitForChange", &q, &r)
	if err != nil {
		return nil, err
	}

	ret := make([]*db.State, len(r.Items))
	for i, f := range r.Items {
		ret[i] = stateFromApi(f)
	}
	return ret, nil
}

func (s States) Apply(ctx context.Context, tx *db.TxnRecord) (uint64, time.Time, []*db.State, error) {

	q := api.ApplyRequest{
		Txn: tx.ToAPI(),
	}

	var r api.ApplyResponse
	err := tryharder(ctx, s.Connection, "omen.state.Apply", &q, &r)

	if err != nil {
		return 0, time.Time{}, nil, err
	}

	lst := make([]*db.State, len(r.States))

	for i, s := range r.States {
		lst[i] = stateFromApi(s)
	}

	return r.Id, time.Unix(0, r.Ts), lst, nil

}

func stateFromApi(s *api.State) *db.State {
	return db.NewStateFromAPI(s)
}
