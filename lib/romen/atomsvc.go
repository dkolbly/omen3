package romen

import (
	"context"

	"bitbucket.org/dkolbly/omen3/api"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
)

type Atoms struct {
	Connection Remote
}

func AtomsOver(r Remote) Atoms {
	return Atoms{
		Connection: r,
	}
}

func (a Atoms) Load(ctx context.Context, p ptr.Ptr) (*db.Atom, error) {
	if !p.IsLoadable() {
		panic("can only load stored CAM data")
	}

	q := api.LoadAtomRequest{
		Subjects: []*api.Ptr{api.FromPtr(p)},
	}
	var r api.LoadAtomResponse

	err := tryharder(ctx, a.Connection, "omen.atom.Load", &q, &r)
	if err != nil {
		return nil, err
	}
	x := r.Atoms[0]
	ans := &db.Atom{
		Addr:    p,
		Payload: x.Payload,
	}
	if x.Type != nil {
		ans.Type = db.PtrFromAPI(x.Type)
	}
	return ans, nil
}

func (a Atoms) Store(context.Context, db.DataType, []byte) (*db.Atom, error) {
	panic("hi")
}
