package protodecode

import (
	"crypto/aes"
	"crypto/sha256"

	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
)

var aesEncryptedType = ptr.String("omen:crypt:aes-256-cbc")

func (s *Scanner) NewCrypterFromPassword(password string) *Crypter {
	h := sha256.New()
	h.Write([]byte(password))
	key := h.Sum(nil)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	h = sha256.New()
	h.Write(key)
	iv := h.Sum(nil)[:aes.BlockSize]

	return s.NewCrypter(aesEncryptedType, block, iv)
}

func init() {
	db.RegisterTypeHydrater(EncryptedMetatype{aesEncryptedType, aes.BlockSize})
}
