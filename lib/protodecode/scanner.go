package protodecode

import (
	"encoding/binary"
	"fmt"

	"bitbucket.org/dkolbly/omen3/api"
	"bitbucket.org/dkolbly/omen3/ptr"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/protoc-gen-go/descriptor"
)

type Scanner struct {
	loc locator
}

func (s *Scanner) NoOp() bool {
	return s.loc == nil
}

func fallback(name string) (*precomp, bool) {
	for _, fs := range pbdecls {
		if entry, ok := fs.byProtoTypename[name]; ok {
			return &precomp{
				name:  name,
				desc:  entry,
				isptr: isMessagePtr(entry),
			}, true
		}
	}
	return nil, false
}

func (d *Decoder) CompileScanner() *Scanner {
	pbc := &pbCompile{
		decoder: d,
		xref:    make(map[string]*precomp),
	}
	// this might be overkill... but scan all the known message types
	var entry string
	for k, v := range d.fs.byProtoTypename {
		pbc.xref[k] = &precomp{
			name:  k,
			desc:  v,
			isptr: isMessagePtr(v),
		}
		if v == d.msg {
			entry = k
		}
	}
	if entry == "" {
		panic("entry point is not found in fset")
	}

	for _, p := range pbc.xref {
		if p.isptr {
			continue
		}
		p.fields = make(map[int32]*precomp)
		for _, field := range p.desc.Field {
			if *field.Type != descriptor.FieldDescriptorProto_TYPE_MESSAGE {
				continue
			}

			typeName := *field.TypeName
			if f, ok := pbc.xref[typeName]; ok {
				p.fields[*field.Number] = f
			} else {
				// pretty sure this is the wrong thing to do...
				// the persistent representation does not necessarily
				// include this :/
				if f, ok := fallback(typeName); ok {
					p.fields[*field.Number] = f
				} else {
					panic("missing: " + typeName)
				}
			}
		}
	}

	// build the reverse index; i.e., if A includes a B,
	// then we want B.referents to list A
	for _, p := range pbc.xref {
		for _, r := range p.fields {
			if r == nil {
				panic("oops")
			}
			r.referents = append(r.referents, p)
		}
	}

	// now, find everything that references, directly or indirectly,
	// a PTR
	for _, p := range pbc.xref {
		if p.isptr {
			p.pushuplinks(make(map[*precomp]bool))
		}
	}

	// build out the msgLocators -- pass 1 allocate them
	mlocs := make(map[*precomp]*msgLocator)
	for k, p := range pbc.xref {
		if p.isptr {
			continue
		}
		if p.flag == ptrs {
			mlocs[p] = &msgLocator{
				name: k,
			}
		}
	}
	// pass 2 fill out the fields
	for k, loc := range mlocs {
		loc.fillin(k, mlocs)
	}

	// finally, get the one we care about
	if loc, ok := mlocs[pbc.xref[entry]]; ok {
		//prwalk(0, loc, make(map[*msgLocator]bool))
		return &Scanner{
			loc: loc,
		}
	} else {
		return &Scanner{}
	}
}

func (s *Scanner) Scan(buf []byte) []ptr.Ptr {
	posns := s.Locate(buf)

	ptrs := make([]ptr.Ptr, len(posns))
	for i, posn := range posns {
		ptrs[i], _ = PtrAtPosn(buf, posn)
	}
	return ptrs
}

type pbCompile struct {
	decoder *Decoder
	xref    map[string]*precomp
}

type precomp struct {
	name      string
	fields    map[int32]*precomp
	referents []*precomp
	isptr     bool
	flag      travFlag
	desc      *descriptor.DescriptorProto
	loc       locator
}

type scanctx struct {
	locations []uint
}

type locator interface {
	scan(*scanctx, uint, []byte)
}

type ptrLocator struct {
}

const protoPtrTag = (1 << 3) + (2)
const protoPtrLen = 32

func PtrAtPosn(buf []byte, posn uint) (ptr.Ptr, int) {
	if buf[posn] != protoPtrTag || buf[posn+1] != protoPtrLen {
		panic("not a ptr posn")
	}
	var value ptr.Ptr
	copy(value.Bits[:], buf[posn+2:])
	return value, 34
}

// length should be exactly 34
//   1 for the field indicator (field=1, wire type "repeated")
//   1 for the length (=32)
//   32 for the data
func (p ptrLocator) scan(ctx *scanctx, offset uint, buf []byte) {
	remain := len(buf)
	if remain != 2+protoPtrLen {
		panic("malformed data")
	}
	if buf[0] != protoPtrTag {
		panic("expected field id 1, wire type length-delimited")
	}
	if buf[1] != protoPtrLen {
		panic("expected length 32")
	}
	var value ptr.Ptr
	copy(value.Bits[:], buf[2:])
	if value.IsLoadable() {
		ctx.locations = append(ctx.locations, offset)
	}
}

func (l *msgLocator) scan(ctx *scanctx, offset uint, buf []byte) {

	for len(buf) > 0 {
		tag, n := binary.Uvarint(buf)
		if n == 0 {
			panic("message truncated")
		}
		buf = buf[n:]
		offset += uint(n)

		wireType := int(tag & 7)
		fieldNumber := int32(tag >> 3)

		if sub, ok := l.fields[fieldNumber]; ok {
			if wireType != 2 {
				panic("expected message to have wire type 2")
			}
			msglen, n := binary.Uvarint(buf)
			offset += uint(n)

			end := int(msglen) + n
			sub.scan(ctx, offset, buf[n:end])
			buf = buf[end:]
			offset += uint(msglen)
		} else {
			n := skipWire(wireType, buf)
			buf = buf[n:]
			offset += uint(n)
		}
	}
}

type msgLocator struct {
	fields map[int32]locator
	flag   travFlag
	name   string
}

type travFlag uint

const (
	unvisited = travFlag(iota)
	noptrs
	ptrs
)

/*func (p *precomp) locator(index map[*precomp]*msgLocator) locator {
	mloc, ok := index[p]
	if !ok {
		return nil
	}
	if len(mloc.fields) == 0 {
		return nil
	}
	return mloc
}*/

// fill in the fields array for message locators; at this point,
// we only have message locators for messages that might have ptrs
func (l *msgLocator) fillin(p *precomp, index map[*precomp]*msgLocator) {
	fields := make(map[int32]locator)
	for k, field := range p.fields {
		if field.isptr {
			fields[k] = ptrLocator{}
		} else if mloc, ok := index[field]; ok {
			fields[k] = mloc
		}
	}
	l.fields = fields
}

func (p *precomp) pushuplinks(visit map[*precomp]bool) {
	p.flag = ptrs
	for _, pred := range p.referents {
		if !visit[pred] {
			visit[pred] = true
			pred.pushuplinks(visit)
		}
	}
}

func prwalk(indent int, loc locator, visited map[*msgLocator]bool) {
	switch loc := loc.(type) {
	case *msgLocator:
		fmt.Printf("%*s (%s) %p\n", 2*indent, "", loc.name, loc)
		if !visited[loc] {
			visited[loc] = true
			for k, v := range loc.fields {
				fmt.Printf("%*s   [%d]:\n", 2*indent, "", k)
				prwalk(indent+2, v, visited)
			}
		}
	case ptrLocator:
		fmt.Printf("%*s PTR\n", 2*indent, "")
	default:
		fmt.Printf("%*s %T\n", 2*indent, "", loc)
	}

}

func (pbc *pbCompile) compileMessage(p *precomp) locator {
	if p.isptr {
		return ptrLocator{}
	}

	fieldLocators := make(map[int32]locator)

	for k, v := range p.fields {
		if v.flag == ptrs {
			loc := pbc.compileMessage(v)
			if loc != nil {
				fieldLocators[k] = loc
			}
		}
	}
	if len(fieldLocators) == 0 {
		return nil
	}

	return &msgLocator{
		name:   p.name,
		fields: fieldLocators,
	}
}

// must have exactly one field of type bytes with number 1
func isValidPtrDescriptor(msg *descriptor.DescriptorProto) bool {
	if len(msg.Field) != 1 {
		return false
	}
	field := msg.Field[0]
	if *field.Number != 1 {
		return false
	}
	if *field.Type != descriptor.FieldDescriptorProto_TYPE_BYTES {
		return false
	}
	return true
}

func isMessagePtr(msg *descriptor.DescriptorProto) bool {
	ex, err := proto.GetExtension(msg.Options, api.E_OmenKind)
	if err != nil {
		return false
	}

	kind := *(ex.(*string))
	switch kind {
	case "ptr":
		if !isValidPtrDescriptor(msg) {
			// TODO this could be a user error
			panic("invalid ptr descriptor")
		}
		return true
	default:
		panic(fmt.Sprintf("unsupported omen_kind %q", kind))
	}
}

// locate all the loadable pointers
func (c *Scanner) Locate(buf []byte) []uint {
	if c.loc == nil {
		return nil
	}
	ctx := &scanctx{}
	c.loc.scan(ctx, 0, buf)
	return ctx.locations
}
