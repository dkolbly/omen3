package protodecode

import (
	"context"
	"fmt"
	"reflect"
	"sync"

	"bitbucket.org/dkolbly/omen3/api"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
	"github.com/golang/protobuf/descriptor"
	"github.com/golang/protobuf/proto"
	desc "github.com/golang/protobuf/protoc-gen-go/descriptor"
)

type Storer interface {
	Store(context.Context, ptr.Ptr, []byte) (*db.Atom, error)
}

var pbdecls []*FileSet

func DeclareProtobufFileSet(data []byte) *FileSet {
	fs, err := NewFileSetFromBytes(data)
	if err != nil {
		panic(err)
	}
	/*fmt.Printf("*** DECLARING FS ***\n")
	for k := range fs.byProtoTypename {
		fmt.Printf("  includes %q\n", k)
	}*/
	pbdecls = append(pbdecls, fs)
	return fs
}

type MessageDataType struct {
	decode *Decoder
	scan   *Scanner
	atom   *db.Atom
}

func getDecoder(file *desc.FileDescriptorProto, md *desc.DescriptorProto) *Decoder {
	fqname := fmt.Sprintf(".%s.%s", *file.Package, *md.Name)
	for _, fs := range pbdecls {
		if _, ok := fs.byProtoTypename[fqname]; ok {
			return fs.NewProtoDecoder(fqname)
		}
	}
	panic(fmt.Sprintf("fully qualified protobuf type name %q not found",
		fqname))
}

func (d *Decoder) asDataType() *MessageDataType {

	tmp := &api.NamedType{
		Fileset: &api.Ptr{
			Addr: d.fs.atom.Addr.Bits[:],
		},
		TypeName: d.fqname,
	}
	buf, err := proto.Marshal(tmp)
	if err != nil {
		panic(err)
	}

	t := &MessageDataType{
		atom: &db.Atom{
			Type:    api.MetaTypeNamed,
			Addr:    ptr.TypedContentAddress(api.MetaTypeNamed, buf),
			Payload: buf,
		},
		decode: d,
		scan:   d.CompileScanner(),
	}
	//fmt.Printf("fileset supporting %s is: <%s>\n", d.fqname, d.fs.atom.Addr)
	//fmt.Printf("data type for %s is: <%s>\n", d.fqname, t.atom.Addr)
	return t
}

var mdtLock sync.Mutex
var mdtCache = make(map[reflect.Type]*MessageDataType)

func DataTypeForMessage(msg proto.Message) *MessageDataType {
	key := reflect.TypeOf(msg)

	mdtLock.Lock()
	defer mdtLock.Unlock()

	if t, ok := mdtCache[key]; ok {
		return t
	}

	dm := msg.(descriptor.Message)
	fd, md := descriptor.ForMessage(dm)

	t := getDecoder(fd, md).asDataType()
	mdtCache[key] = t
	return t
}

func (mdt *MessageDataType) Scan(buf []byte) ([]ptr.Ptr, error) {
	return mdt.scan.Scan(buf), nil
}

func (mdt *MessageDataType) Addr() ptr.Ptr {
	return mdt.atom.Addr
}

func (mdt *MessageDataType) String() string {
	return fmt.Sprintf("<MessageDataType %s>", mdt.decode.fqname)
}

// returns a list of atoms required to embody this data type
func (mdt *MessageDataType) Atoms() []*db.Atom {
	return []*db.Atom{mdt.decode.fs.atom, mdt.atom}
}
