package protodecodetest

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"testing"

	"bitbucket.org/dkolbly/omen3/lib/protodecode"
	"bitbucket.org/dkolbly/omen3/ptr"
	"github.com/golang/protobuf/proto"
)

func testEmpty() (*protodecode.Scanner, []byte) {
	d := fset.NewProtoDecoder(".protodecodetest.Node")
	if d == nil {
		panic("no message type Node")
	}

	datum := &Node{}

	nbuf, err := proto.Marshal(datum)
	if err != nil {
		panic(err)
	}
	return d.CompileScanner(), nbuf
}

func testBufThreePtrs() (*protodecode.Scanner, []byte) {
	foop1 := ptr.ContentAddress(ptr.Untyped, []byte("foo1"))
	foop2 := ptr.ContentAddress(ptr.Untyped, []byte("foo2"))
	foop3 := ptr.ContentAddress(ptr.Untyped, []byte("foo3"))

	datum := &Node{
		Name:      "a few things",
		Reference: hydratePtr(foop3),
	}
	datum.Children = append(datum.Children, &Node{
		Name:      "thing the first",
		Reference: hydratePtr(foop1),
	})
	datum.Children = append(datum.Children, &Node{
		Name:      "thing the second",
		Reference: hydratePtr(foop2),
	})

	nbuf, err := proto.Marshal(datum)
	if err != nil {
		panic(err)
	}
	d := fset.NewProtoDecoder(".protodecodetest.Node")
	if d == nil {
		panic("no message type Node")
	}
	return d.CompileScanner(), nbuf
}

func TestEncrypt(t *testing.T) {
	scan, nbuf := testBufThreePtrs()
	crypt := scan.NewCrypterFromPassword("hello")

	_, ebuf := crypt.Encrypt(&foop, nbuf)

	_, ebuf2 := crypt.Encrypt(&foop, nbuf)
	if !bytes.Equal(ebuf, ebuf2) {
		t.Fatal("expected encryption to be purely functional")
	}

	fmt.Printf("%s\n", base64.StdEncoding.EncodeToString(ebuf))

	rt, rbuf := crypt.Decrypt(ebuf)
	if rt != foop {
		t.Fatal("expected decrypted type to be foop")
	}
	if !bytes.Equal(rbuf, nbuf) {
		t.Fatal("expected decrypt to produce original data")
	}
}

func TestBackwardCompatDecrypt(t *testing.T) {
	encrypted := `XvOsLHyF37wBFKaiMQfVjUcELCa0a2j/xo/5m0U8HTBBNBNCLXBkg7+g+YpeiGJm56FJY71xOn6xvORYhosMhHK9yLxZKaeJKpLdJDRK6pIJMWWLMX+kSvjC1J9pf26XctmfYulQzmlADzGoypQ2nBnRu07KM09hrztntdUokH0wKFFRYQIAU5MC9MjKvmYiW1FX6UpS7x2J9kcoxW1nFapvQ+n1nlVD+QQSqKGiACULmgJXG9akw8tEyJuMhsbEcP2xor8g7HVZ+CeFJ6xCISX+zYi3zyrSmBEkVy1UVRDLAA==`

	d := fset.NewProtoDecoder(".protodecodetest.Node")
	if d == nil {
		t.Fatalf("no message type Node")
	}

	scan, orig := testBufThreePtrs()
	crypt := scan.NewCrypterFromPassword("hello")

	buf, err := base64.StdEncoding.DecodeString(encrypted)
	if err != nil {
		t.Fatal(err)
	}
	rt, rbuf := crypt.Decrypt(buf)
	if rt != foop {
		t.Fatal("expected decrypted type to be foop")
	}

	if !bytes.Equal(rbuf, orig) {
		t.Fatal("expected decrypt to produce original 3-ptrs data")
	}
}

func TestEncryptVaryByPassword(t *testing.T) {
	scan, nbuf := testEmpty()
	crypt1 := scan.NewCrypterFromPassword("hello")
	crypt2 := scan.NewCrypterFromPassword("hell0")

	_, ebuf1 := crypt1.Encrypt(nil, nbuf)
	_, ebuf2 := crypt2.Encrypt(nil, nbuf)

	protodecode.Hexdump(ebuf1)
	protodecode.Hexdump(ebuf2)

	if bytes.Equal(ebuf1, ebuf2) {
		t.Fatal("expected different passwords to produce different encryptions")
	}
}

func TestEncryptAllTypes(t *testing.T) {

	scan, emptybuf := testEmpty()
	_, busybuf := testBufThreePtrs()
	crypt := scan.NewCrypterFromPassword("hello")

	for _, nbuf := range [][]byte{emptybuf, busybuf} {

		loadablet := foop
		litt := ptr.String("here-is-a-type")

		for _, useType := range []*ptr.Ptr{nil, &loadablet, &litt} {
			_, ebuf := crypt.Encrypt(useType, nbuf)

			rt, rbuf := crypt.Decrypt(ebuf)
			if useType == nil {
				if !rt.IsNull() {
					t.Fatalf("expected decrypted type to be null, got %s", rt)
				}
			} else {
				if rt != *useType {
					t.Fatalf("expected decrypted type to be %s, got %s", *useType, rt)
				}
			}
			if !bytes.Equal(rbuf, nbuf) {
				t.Fatal("expected decrypt to produce original data")
			}
		}
	}
}
