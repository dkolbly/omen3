package protodecodetest

import (
	"context"
	"io/ioutil"
	"testing"

	"bitbucket.org/dkolbly/omen3/lib/protodecode"
	"bitbucket.org/dkolbly/omen3/ptr"
	"github.com/golang/protobuf/proto"
)

func buildTestNode() *Node {
	// formulate a message
	n1 := &Node{
		Name:      "literal-valued-thing",
		Reference: hydratePtr(ptr.String("Literal")),
	}
	n2 := &Node{
		Name:      "reference-valued-thing",
		Reference: hydratePtr(foop),
	}
	n3 := &Node{
		Name: "parent",
	}
	n3.Children = append(n3.Children, n1)
	n3.Children = append(n3.Children, n2)
	return n3
}

func hydratePtr(p ptr.Ptr) *Ptr {
	return &Ptr{
		Addr: p.Bits[:],
	}
}

var foop = ptr.ContentAddress(ptr.Untyped, []byte("foo"))

var fset *protodecode.FileSet

func init() {
	buf, err := ioutil.ReadFile("pbtypes.pb")
	if err != nil {
		panic(err)
	}
	fs, err := protodecode.NewFileSetFromBytes(buf)
	if err != nil {
		panic(err)
	}
	fset = fs
}

func TestLocateNoPointers(t *testing.T) {
	nodeDecoder := fset.NewProtoDecoder(".protodecodetest.Node")
	if nodeDecoder == nil {
		t.Fatalf("no message type Node")
	}

	scan := nodeDecoder.CompileScanner()

	triv := &Node{
		Name:      "nothing to see here",
		Reference: hydratePtr(ptr.String("literal")),
	}

	nbuf, err := proto.Marshal(triv)
	if err != nil {
		t.Fatal(err)
	}

	posns := scan.Locate(nbuf)
	if len(posns) != 0 {
		t.Fatalf("rats, expected no refs, got %d", len(posns))
	}
}

func TestLocateOnePointer(t *testing.T) {
	nodeDecoder := fset.NewProtoDecoder(".protodecodetest.Node")
	if nodeDecoder == nil {
		t.Fatalf("no message type Node")
	}

	scan := nodeDecoder.CompileScanner()

	one := &Node{
		Reference: hydratePtr(foop),
	}

	nbuf, err := proto.Marshal(one)
	if err != nil {
		t.Fatal(err)
	}

	posns := scan.Locate(nbuf)
	if len(posns) != 1 {
		t.Fatalf("rats, expected one posn, got %d", len(posns))
	}

	ptrs := scan.Scan(nbuf)
	if ptrs[0] != foop {
		t.Fatalf("rats, expected to find foop, got %s", ptrs[0])
	}
}

func TestScanStaticNoPointers(t *testing.T) {
	d := fset.NewProtoDecoder(".protodecodetest.RecursiveTypeWithNoPtrs")
	if d == nil {
		t.Fatalf("no message type")
	}

	scan := d.CompileScanner()

	if !scan.NoOp() {
		t.Fatal("expected to compile to nothing")
	}
}

func TestDecodeNegativeValue(t *testing.T) {
	n := &Node{
		Ivalue: -37,
		Uvalue: 3701,
		Svalue: -41,
	}
	nbuf, err := proto.Marshal(n)
	if err != nil {
		t.Fatal(err)
	}
	nodeDecoder := fset.NewProtoDecoder(".protodecodetest.Node")
	if nodeDecoder == nil {
		t.Fatalf("no message type Node")
	}
	ctx := context.Background()
	q, lst := nodeDecoder.Decode(ctx, nbuf)
	if len(lst) != 0 {
		t.Fatalf("expected no ptrs, got %d", len(lst))
	}

	ivalue := q.(*protodecode.Object).Fields["ivalue"]
	i, ok := ivalue.(int64)
	if !ok {
		t.Fatalf("got %T instead of int64", ivalue)
	}
	if i != n.Ivalue {
		t.Fatalf("expected %d but got %d", n.Ivalue, i)
	}

	svalue := q.(*protodecode.Object).Fields["svalue"]
	i2, ok := svalue.(int64)
	if !ok {
		t.Fatalf("got %T instead of int64", svalue)
	}
	if i2 != n.Svalue {
		t.Fatalf("expected %d but got %d", n.Svalue, i2)
	}

	uvalue := q.(*protodecode.Object).Fields["uvalue"]
	u, ok := uvalue.(uint64)
	if !ok {
		t.Fatalf("got %T instead of uint64", uvalue)
	}
	if u != 3701 {
		t.Fatalf("expected %d but got %d", n.Uvalue, u)
	}
}
