// +build false

package protodecodetest

import (
	"bytes"
	"testing"

	"bitbucket.org/dkolbly/omen3/lib/secdomain"
)

func TestSecurityDomain(t *testing.T) {
	sd, err := secdomain.NewSecurityDomainAES("hello")
	if err != nil {
		t.Fatal(err)
	}

	if !sd.ValidPassword("hello") {
		t.Fatal("expected valid password")
	}
	if sd.ValidPassword("hel1o") {
		t.Fatal("expected not valid password")
	}
}

func TestSecurityDomainEncryptionExternalSalt(t *testing.T) {
	sd1, err := secdomain.NewSecurityDomainAES("hello")
	if err != nil {
		t.Fatal(err)
	}
	sd2, err := secdomain.NewSecurityDomainAES("hello")
	if err != nil {
		t.Fatal(err)
	}

	_, iv1 := sd1.Unlock("hello")
	_, iv2 := sd2.Unlock("hello")
	if bytes.Equal(iv1, iv2) {
		t.Fatal("expected different security domains to have different IVs")
	}
}

func TestSecurityDomainEncryption(t *testing.T) {
	sd, err := secdomain.NewSecurityDomainAES("hello")
	if err != nil {
		t.Fatal(err)
	}

	scan, nbuf := testBufThreePtrs()
	block, iv := sd.Unlock("hello")
	crypt := scan.NewCrypter(sd.Type(), block, iv)
	if err != nil {
		t.Fatal("expected success")
	}

	_, ebuf := crypt.Encrypt(&foop, nbuf)

	rt, rbuf := crypt.Decrypt(ebuf)
	if rt != foop {
		t.Fatal("expected decrypted type to match")
	}
	if !bytes.Equal(rbuf, nbuf) {
		t.Fatal("expected decrypted payload to match")
	}
}
