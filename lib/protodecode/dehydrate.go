package protodecode

import (
	"context"
	"errors"
	"sync"

	"bitbucket.org/dkolbly/omen3/api"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
	"github.com/golang/protobuf/proto"
	desc "github.com/golang/protobuf/protoc-gen-go/descriptor"
)

var fsCacheLock sync.Mutex
var fsCache = make(map[ptr.Ptr]*FileSet)
var dtCacheLock sync.Mutex
var dtCache = make(map[ptr.Ptr]db.DataType)

// DataTypeForType interprets a *type* reference (which, if it is a loadable
// type, the caller supplies the Atom as well).  If we know how to interpret
// it, then we return the DataType and true.  If not, we return nil and false.
func DataTypeForType(ctx context.Context, p ptr.Ptr, a *db.Atom, ld db.Loader) (db.DataType, bool, error) {
	if a == nil {
		// it's not even loadable... forget it, but no error
		return nil, false, nil
	}
	if a.Type != api.MetaTypeNamed {
		// the metatype is not what we know how to interpret
		return nil, false, nil
	}

	dtCacheLock.Lock()
	dt, ok := dtCache[p]
	dtCacheLock.Unlock()
	if ok {
		return dt, true, nil
	}

	nt, err := api.UnmarshalNamedType(a.Payload)
	if err != nil {
		// we thought we could handle it, but there was a problem
		return nil, false, err
	}

	var fsp ptr.Ptr
	copy(fsp.Bits[:], nt.Fileset.Addr)
	fs, err := getFileSetFromPtr(ctx, fsp, ld)
	if err != nil {
		return nil, false, err
	}

	d := fs.NewProtoDecoder(nt.TypeName)

	dt = &MessageDataType{
		atom:   a,
		decode: d,
		scan:   d.CompileScanner(),
	}
	dtCacheLock.Lock()
	if d2, ok := dtCache[p]; ok {
		dt = d2
	} else {
		dtCache[p] = dt
	}
	dtCacheLock.Unlock()
	return dt, true, nil
}

var ErrCorruptNamedTyped = errors.New("named type object is corrupt (does not refer to a fileset)")

func getFileSetFromPtr(ctx context.Context, p ptr.Ptr, ld db.Loader) (*FileSet, error) {
	fsCacheLock.Lock()
	fs, ok := fsCache[p]
	fsCacheLock.Unlock()

	if ok {
		return fs, nil
	}

	a, err := ld.Load(ctx, p)
	if err != nil {
		return nil, err
	}

	if a.Type != api.MetaTypeFileSet {
		return nil, ErrCorruptNamedTyped
	}

	fs, err = newFileSetFromAtom(a)
	if err != nil {
		return nil, err
	}

	fsCacheLock.Lock()
	// check again... someone else may have loaded it in the
	// meantime, and we want to return the same one they had
	// returned (as if we were at the top of this function instead
	// of down here)
	if refs, ok := fsCache[p]; ok {
		fs = refs
	} else {
		fsCache[p] = fs
	}
	fsCacheLock.Unlock()
	return fs, nil
}

func newFileSetFromAtom(a *db.Atom) (*FileSet, error) {
	set := new(desc.FileDescriptorSet)

	err := proto.Unmarshal(a.Payload, set)
	if err != nil {
		return nil, err
	}

	fs := NewFileSet(set)
	fs.atom = a

	return fs, nil
}

type ProtoBufMetatype struct {
}

func (mt ProtoBufMetatype) DataTypeHydrate(ctx context.Context, p ptr.Ptr, a *db.Atom, ld db.Loader) (db.DataType, bool, error) {
	return DataTypeForType(ctx, p, a, ld)
}

// the type denoted by api.MetaTypeNamed : ptr.String("type^:omen:pb/named_type")
// an instance of which contains a single pointer

type pbNamedType struct {
}

func (nt pbNamedType) DataTypeHydrate(ctx context.Context, p ptr.Ptr, a *db.Atom, ld db.Loader) (db.DataType, bool, error) {
	if p != api.MetaTypeNamed {
		return nil, false, nil
	}
	return nt, true, nil
}

func (nt pbNamedType) Addr() ptr.Ptr {
	return api.MetaTypeNamed
}

func (nt pbNamedType) Scan(buf []byte) ([]ptr.Ptr, error) {
	unpacked, err := api.UnmarshalNamedType(buf)
	if err != nil {
		return nil, err
	}
	ret := make([]ptr.Ptr, 1)
	copy(ret[0].Bits[:], unpacked.Fileset.Addr)
	return ret, nil
}

// the type denoted by api.MetaTypeFileSet : ptr.String("type^:omen:pb/file_set")
// which contains no pointers
type pbFilesetType struct {
}

func (nt pbFilesetType) DataTypeHydrate(ctx context.Context, p ptr.Ptr, a *db.Atom, ld db.Loader) (db.DataType, bool, error) {
	if p != api.MetaTypeFileSet {
		return nil, false, nil
	}
	return nt, true, nil
}

func (nt pbFilesetType) Addr() ptr.Ptr {
	return api.MetaTypeFileSet
}

func (nt pbFilesetType) Scan(buf []byte) ([]ptr.Ptr, error) {
	return nil, nil
}

func init() {
	db.RegisterTypeHydrater(ProtoBufMetatype{})
	db.RegisterTypeHydrater(pbNamedType{})
	db.RegisterTypeHydrater(pbFilesetType{})
}
