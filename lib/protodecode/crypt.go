package protodecode

// TODO: hide topology data where possible.  the reflist should be
//   (1) sorted, to hide information about what order the ptrs occur in the document
//   (2) dup-eliminated, to hide information about cardinality
// both goals are accomplishable by inserting a reverse marker at the locations
//
//  replace:
//      Ptr { bytes addr = 1; }
//  with something like:
//      PtrPlace { uint32 index = 2; uint32 next = 3 }
//
//  then, instead of storing the locations of all the ptrs in the
//  (encrypted) header, we just store the first one

import (
	"bytes"
	"context"
	"crypto/cipher"
	"encoding/binary"
	"fmt"
	"sort"

	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
)

const debug = false

func debugf(msg string, args ...interface{}) {
	if debug {
		fmt.Printf(msg, args...)
	}
}

type Crypter struct {
	scanner *Scanner
	block   cipher.Block
	iv      []byte
	encType ptr.Ptr
}

func (s *Scanner) NewCrypter(mt ptr.Ptr, block cipher.Block, iv []byte) *Crypter {
	c := &Crypter{
		scanner: s,
		block:   block,
		iv:      iv,
		encType: mt,
	}
	return c
}

type storeEncrypting struct {
	using *Crypter
	next  db.Storer
}

func (c *Crypter) Storer(next db.Storer) db.Storer {
	return &storeEncrypting{
		using: c,
		next:  next,
	}
}

func (e *storeEncrypting) Store(ctx context.Context, t db.DataType, buf []byte) (*db.Atom, error) {
	var tp *ptr.Ptr
	if t != nil {
		ta := t.Addr()
		tp = &ta
	}
	nxt, encdata := e.using.Encrypt(tp, buf)
	return e.next.Store(ctx, db.ScanlessType(nxt), encdata)
}

const ptrPlaceEndTag = 0xF0
const ptrPlaceWithNextTag = 0xF1

func writePtrPlace(dst *bytes.Buffer, index, next uint) {
	var tmp [32]byte
	// note that the next field can't be 0; the *first* might be 0,
	// but not the next
	if next == 0 {
		tmp[0] = ptrPlaceEndTag
	} else {
		tmp[0] = ptrPlaceWithNextTag
	}
	i := 1
	i += binary.PutUvarint(tmp[i:], uint64(index))
	if next != 0 {
		i += binary.PutUvarint(tmp[i:], uint64(next))
	}

	dst.Write(tmp[:i])
}

func (c *Crypter) Encrypt(t *ptr.Ptr, buf []byte) (ptr.Ptr, []byte) {
	var original ptr.Ptr
	debugf("encrypting %d bytes\n", len(buf))

	// figure out what the ptr to the original (unwrapped) object is
	if t == nil {
		original = ptr.ContentAddress(ptr.Untyped, buf)
	} else {
		original = ptr.TypedContentAddress(*t, buf)
	}

	// rehash the original ptr with the key to compute the salt;
	// this is to prevent a sort of traffic analysis (i.e., the
	// ability to tell whether a certain plaintext is present in a
	// data-at-rest dump without having to know the password)
	// while preserving the pure-functional characteristic (i.e.,
	// that the encryption of the same data with the same password
	// produces the same result)
	salt := c.salt(original)

	header := &bytes.Buffer{}
	header.Write(salt)

	// scan the object, locating all the pointers and building a
	// sorted, de-duped index of ptrs
	locs := c.scanner.Locate(buf)
	tocList, tocMap := buildToc(locs, buf, t)

	var nbuf [16]byte

	plaintext := &bytes.Buffer{}

	// write the value of the original size, useful so during decrypt
	// we can preallocate the output payload
	n := binary.PutUvarint(nbuf[:], uint64(len(buf)))
	plaintext.Write(nbuf[:n])
	debugf("original size %d (took %d bytes to say that)\n", len(buf), n)

	// write the value of loc[0] ; remaining locs are linked via
	// the PtrPlace objects

	// the +1 is to encode the possibility that locs[0] is 0
	if len(locs) == 0 {
		plaintext.WriteByte(0) // the encoding of 0 is []byte{0}
		debugf("first location is NONE (took 1 byte to say that)\n")
	} else {
		n = binary.PutUvarint(nbuf[:], uint64(locs[0]+1))
		plaintext.Write(nbuf[:n])
		debugf("first location is +1+%d (took %d bytes to say that)\n", locs[0], n)
	}

	if t == nil {
		plaintext.WriteByte(0)
		debugf("type index ZERO (1 bytes)\n")
	} else if t.IsLoadable() {
		// the +1 is to encode the two other possibilities for the type ptr
		//  0 = no type ptr
		//  1 = non-loadable type ptr (in which case it occurs in the plaintext
		//  immediately after this value)
		typeIndex := tocMap[*t]
		n := binary.PutUvarint(nbuf[:], uint64(typeIndex+2))
		plaintext.Write(nbuf[:n])
		debugf("type index %d+2 (%d bytes)\n", typeIndex, n)
	} else {
		plaintext.WriteByte(1)
		plaintext.Write(t.Bits[:])
		debugf("type index ONE (1+32 bytes)\n")
	}

	var k uint

	for j, l := range locs {
		if l > k {
			// write the content BEFORE this Ptr
			plaintext.Write(buf[k:l])
		}
		// pull out the Ptr, and write the PtrPlace instead
		p, n := PtrAtPosn(buf, l)
		var next uint
		if j+1 < len(locs) {
			next = locs[j+1]
		}
		debugf("LOC[%d] at +%d --> installing (%d,%d) for %s\n",
			j, l,
			tocMap[p], next,
			p)
		writePtrPlace(plaintext, tocMap[p], next)

		// update to AFTER this PTR
		k = l + uint(n)
	}

	if k < uint(len(buf)) {
		plaintext.Write(buf[k:])
	}

	// write the length of the plaintext
	unencLen := plaintext.Len()
	header.Write(nbuf[:binary.PutUvarint(nbuf[:], uint64(unencLen))])

	// write the reference block (table of contents)
	header.Write(nbuf[:binary.PutUvarint(nbuf[:], uint64(len(tocList)))])
	for i, p := range tocList {
		debugf("TOC[%d] %x %s\n", i, p.Bits[:], p)
		header.Write(p.Bits[:])
	}

	// pad it out to a block size
	blockSize := len(c.iv)
	padlen := (blockSize - unencLen%blockSize) % blockSize

	if debug {
		debugf("preamble:\n")
		Hexdump(header.Bytes())
		debugf("plaintext:\n")
		Hexdump(plaintext.Bytes())
		debugf("padding %d with +%d\n", plaintext.Len(), padlen)
	}

	plaintext.Write(salt[:padlen])

	cbc := cipher.NewCBCEncrypter(c.block, salt)

	hlen := header.Len()
	dst := make([]byte, hlen+unencLen+padlen)
	copy(dst, header.Bytes())
	cbc.CryptBlocks(dst[hlen:], plaintext.Bytes())

	return c.encType, dst
}

func (c *Crypter) salt(original ptr.Ptr) []byte {
	salt := make([]byte, len(c.iv))
	copy(salt, original.Bits[:])
	c.block.Encrypt(salt, salt)
	return salt
}

func (c *Crypter) Decrypt(buf []byte) (ptr.Ptr, []byte) {
	debugf("======================= DECRYPT ===================\n")
	blockSize := len(c.iv)
	salt := buf[:blockSize]

	index := blockSize
	unencLen, n := binary.Uvarint(buf[index:])
	index += n

	numRefs, n := binary.Uvarint(buf[index:])
	index += n

	debugf("decrypt unenc len %d numrefs %d\n", unencLen, numRefs)

	refBase := index
	toc := func(i int) (p ptr.Ptr) {
		copy(p.Bits[:], buf[refBase+i*32:])
		return
	}

	index += int(numRefs) * ptr.PtrSize

	cbc := cipher.NewCBCDecrypter(c.block, salt)

	padlen := (blockSize - int(unencLen)%blockSize) % blockSize
	plain := make([]byte, int(unencLen)+padlen)

	cbc.CryptBlocks(plain, buf[index:])

	if debug {
		debugf("decryption preamble:\n")
		Hexdump(buf[:index])
		debugf("decrypted plaintext:\n")
		Hexdump(plain)
	}

	plain = plain[:unencLen]

	var typePtr ptr.Ptr

	payloadLen, n := binary.Uvarint(plain)
	plain = plain[n:]
	debugf("payload len is %d\n", payloadLen)

	firstLoc, n := binary.Uvarint(plain)
	plain = plain[n:]
	debugf("first location is %d\n", firstLoc)

	typeIndex, n := binary.Uvarint(plain)
	plain = plain[n:]
	debugf("type index is %d\n", typeIndex)

	if typeIndex == 1 {
		// read the actual type
		copy(typePtr.Bits[:], plain[:32])
		plain = plain[32:]
		debugf("type ptr from PLAINTEXT is %s\n", typePtr)
	} else if typeIndex >= 2 {
		typePtr = toc(int(typeIndex - 2))
		debugf("type ptr from TOC is %s\n", typePtr)
	}

	// interpolate the PtrPlaces in the plaintext into the payload
	payload := make([]byte, payloadLen)

	var dst int // dest offset

	if firstLoc > 0 {
		refat := int(firstLoc - 1)
		for {
			debugf("len(plain) = %d\n", len(plain))
			debugf("at dst[%d:] <- src[:%d] : this ref at +%d\n",
				dst, refat-dst, refat)
			copy(payload[dst:], plain[:refat-dst])
			plain = plain[refat-dst:]
			debugf("*** here we see this: %x\n", plain[:5])

			tag := plain[0]
			index, n := binary.Uvarint(plain[1:])
			p := toc(int(index))
			debugf("i.e. index TOC[%d] = %s\n", index, p)
			plain = plain[n+1:]

			// populate the Ptr we stripped out before
			l := refat
			payload[l] = protoPtrTag
			payload[l+1] = protoPtrLen
			copy(payload[l+2:], p.Bits[:])
			dst = l + 34

			if tag == ptrPlaceWithNextTag {
				next, n := binary.Uvarint(plain)
				plain = plain[n:]
				debugf("and next is %d\n", next)
				refat = int(next)
			} else if tag == ptrPlaceEndTag {
				debugf("that's all there is\n")
				break
			}
		}
	} else {
		debugf("no refs in payload\n")
	}

	// copy over the trailer
	debugf("trailer dst[%d:] <- src[:%d]\n", dst, len(plain))
	copy(payload[dst:], plain)

	return typePtr, payload
}

func Hexdump(data []byte) {

	line := func(base int, line []byte) {
		if len(line) > 16 {
			line = line[:16]
		}
		fmt.Printf("%04x", base)
		for i := 0; i < 16; i++ {
			if i == 8 {
				fmt.Printf(" ")
			}
			if i < len(line) {
				fmt.Printf(" %02x", line[i])
			} else {
				fmt.Printf("   ")
			}
		}
		fmt.Printf(" | ")
		for i, ch := range line {
			if i == 8 {
				fmt.Printf(" ")
			}
			if ch >= ' ' && ch <= '~' {
				fmt.Printf("%c", ch)
			} else {
				fmt.Printf(".")
			}
		}
		fmt.Printf("\n")
	}

	for i := 0; i < len(data); i += 16 {
		line(i, data[i:])
	}
}

type EncryptedMetatype struct {
	typePtr   ptr.Ptr
	blockSize int
}

func (mt EncryptedMetatype) DataTypeHydrate(ctx context.Context, p ptr.Ptr, a *db.Atom, ld db.Loader) (db.DataType, bool, error) {
	if p != mt.typePtr {
		return nil, false, nil
	}
	return mt, true, nil
}

func (mt EncryptedMetatype) Addr() ptr.Ptr {
	return mt.typePtr
}

func (mt EncryptedMetatype) Scan(buf []byte) ([]ptr.Ptr, error) {
	// note that, by design, we can scan an encrypted block
	// without decrypting it.  That is because we need to be able
	// to compute reachability (for garbage collection purposes)
	// on the server side.  As a consequence, the topology of a
	// graph is not encrypted
	index := mt.blockSize
	_, n := binary.Uvarint(buf[index:])
	index += n

	numRefs, n := binary.Uvarint(buf[index:])
	index += n

	refs := make([]ptr.Ptr, numRefs)
	for i := range refs {
		copy(refs[i].Bits[:], buf[index:])
		index += ptr.PtrSize
	}
	return refs, nil
}

func buildToc(locs []uint, buf []byte, t *ptr.Ptr) ([]ptr.Ptr, map[ptr.Ptr]uint) {
	index := make(map[ptr.Ptr]uint)
	lst := make([]ptr.Ptr, 0, len(locs)+1)

	if t != nil {
		lst = append(lst, *t)
	}
	for _, l := range locs {
		p, _ := PtrAtPosn(buf, l)
		if _, ok := index[p]; !ok {
			index[p] = 0
			lst = append(lst, p)
		}
	}
	sort.Sort(ptrList(lst))
	for i, p := range lst {
		index[p] = uint(i)
	}

	return lst, index
}

type ptrList []ptr.Ptr

func (l ptrList) Len() int {
	return len(l)
}

func (l ptrList) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

func (l ptrList) Less(i, j int) bool {
	a := binary.BigEndian.Uint32(l[i].Bits[:])
	b := binary.BigEndian.Uint32(l[j].Bits[:])
	if a < b {
		return true
	} else if a > b {
		return false
	}
	for k := 1; k < ptr.PtrSize/4; k++ {
		a := binary.BigEndian.Uint32(l[i].Bits[k*4:])
		b := binary.BigEndian.Uint32(l[j].Bits[k*4:])
		if a < b {
			return true
		} else if a > b {
			return false
		}
	}
	return false
}
