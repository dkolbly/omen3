package protodecode

import (
	"context"
	"encoding/binary"
	"fmt"
	"math"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/omen3/api"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
	"github.com/golang/protobuf/proto"
	desc "github.com/golang/protobuf/protoc-gen-go/descriptor"
)

var log = logging.New("pbd")

const trace = false

type FileSet struct {
	atom            *db.Atom
	set             *desc.FileDescriptorSet
	index           map[string]*desc.DescriptorProto
	kind            map[*desc.DescriptorProto]string
	byProtoTypename map[string]*desc.DescriptorProto
	byOmenTypename  map[string]*desc.DescriptorProto
}

type Decoder struct {
	fs     *FileSet
	fqname string
	msg    *desc.DescriptorProto
}

func NewFileSetFromBytes(buf []byte) (*FileSet, error) {
	a := &db.Atom{
		Type:    api.MetaTypeFileSet,
		Addr:    ptr.TypedContentAddress(api.MetaTypeFileSet, buf),
		Payload: buf,
	}
	fs, err := newFileSetFromAtom(a)
	if err != nil {
		return nil, err
	}

	fsCacheLock.Lock()
	fsCache[a.Addr] = fs
	fsCacheLock.Unlock()

	return fs, nil
}

func NewFileSet(set *desc.FileDescriptorSet) *FileSet {
	index := make(map[string]*desc.DescriptorProto)
	kind := make(map[*desc.DescriptorProto]string)
	otn := make(map[string]*desc.DescriptorProto)

	var loadtype func(prefix string, mt *desc.DescriptorProto)

	loadtype = func(prefix string, mt *desc.DescriptorProto) {

		name := fmt.Sprintf("%s.%s", prefix, *mt.Name)
		if trace {
			fmt.Printf("  > %s  %p\n", name, mt)
			if mt.Options != nil && mt.Options.MapEntry != nil {
				fmt.Printf("  * map entry %t\n",
					*mt.Options.MapEntry)
			}
		}
		tn, err := proto.GetExtension(mt.Options, api.E_OmenTypename)
		if err == nil {
			tn := tn.(*string)
			if trace {
				fmt.Printf("found omen type %q\n", *tn)
			}
			otn[*tn] = mt
		}
		ex, err := proto.GetExtension(mt.Options, api.E_OmenKind)
		if err == nil {
			ex := ex.(*string)
			if trace {
				fmt.Printf("    omen kind %q\n", *ex)
			}
			kind[mt] = *ex
		}

		for _, nt := range mt.NestedType {
			if trace {
				fmt.Printf("    BEGIN nested type [%s.%s]\n",
					name, *nt.Name)
			}
			loadtype(name, nt)
			if trace {
				fmt.Printf("    END nested type [%s.%s]\n",
					name, *nt.Name)
			}
		}
		index[name] = mt
	}

	for _, fd := range set.File {
		if trace {
			fmt.Printf("File: %s (%s)\n", *fd.Name, *fd.Package)
		}
		pkgPrefix := fmt.Sprintf(".%s", *fd.Package)
		for _, mt := range fd.MessageType {
			loadtype(pkgPrefix, mt)
		}
	}

	return &FileSet{
		set:             set,
		byProtoTypename: index,
		kind:            kind,
		byOmenTypename:  otn,
	}
}

func (fs *FileSet) NewProtoDecoder(protoName string) *Decoder {
	msg := fs.byProtoTypename[protoName]
	if msg == nil {
		return nil
	}
	return &Decoder{
		fs:     fs,
		msg:    msg,
		fqname: protoName,
	}
}

func (fs *FileSet) NewDecoder(otn string) *Decoder {
	msg := fs.byOmenTypename[otn]
	if msg == nil {
		return nil
	}
	return &Decoder{
		fs:  fs,
		msg: msg,
	}
}

/*func NewDecoder(set *desc.FileDescriptorSet, msg *desc.DescriptorProto) *Decoder {
	index := make(map[string]*desc.DescriptorProto)
	kind := make(map[*desc.DescriptorProto]string)

	for _, fd := range set.File {
		//fmt.Printf("File: %s (%s)\n", *fd.Name, *fd.Package)
		for _, mt := range fd.MessageType {
			name := fmt.Sprintf(".%s.%s", *fd.Package, *mt.Name)
			//fmt.Printf("  > %s\n", name)
			tn, err := proto.GetExtension(mt.Options, api.E_OmenTypename)
			if err == nil {
				tn := tn.(*string)
				fmt.Printf("found type %q\n", *tn)
			}
			ex, err := proto.GetExtension(mt.Options, api.E_OmenKind)
			if err == nil {
				ex := ex.(*string)
				//fmt.Printf("    omen kind %q\n", *ex)
				kind[mt] = *ex
			}

			index[name] = mt
		}
	}

	return &Decoder{
		name:  *msg.Name,
		set:   set,
		msg:   msg,
		index: index,
		kind:  kind,
	}
}
*/

type Object struct {
	Type    string
	Fields  map[string]interface{}
	Unknown map[int]interface{}
}

type ObsReference struct {
	Value  interface{}
	Format string
}

func (d *Decoder) Decode(ctx context.Context, buf []byte) (interface{}, []ptr.Ptr) {
	var lst []ptr.Ptr
	accum := make(chan ptr.Ptr)
	dc := &decodeCtx{
		Decoder: d,
		ctx:     ctx,
		form:    d.msg,
		dst:     accum,
	}
	var top interface{}
	go func() {
		defer close(accum)
		top = dc.decodeObj(buf)
	}()
	for p := range accum {
		lst = append(lst, p)
	}
	return top, lst
}

type decodeCtx struct {
	*Decoder
	dst  chan<- ptr.Ptr
	ctx  context.Context
	form *desc.DescriptorProto
}

// create a new (sub) decoding context for the given data type
func (dc *decodeCtx) fork(t *desc.DescriptorProto) *decodeCtx {
	return &decodeCtx{
		Decoder: dc.Decoder,
		dst:     dc.dst,
		ctx:     dc.ctx,
		form:    t,
	}
}

func (dc *decodeCtx) find(fieldNum int) *desc.FieldDescriptorProto {
	if dc.form == nil {
		return nil
	}
	for _, field := range dc.form.Field {
		if *field.Number == int32(fieldNum) {
			return field
		}
	}
	return nil
}

func (dc *decodeCtx) decodeObj(buf []byte) interface{} {

	into := &Object{
		Type:   *dc.form.Name,
		Fields: make(map[string]interface{}),
	}
	var fname string
	listform := make(map[string]bool)

	for len(buf) > 0 {
		tag, n := binary.Uvarint(buf)
		if n == 0 {
			panic("message truncated")
		}
		buf = buf[n:]

		wireType := int(tag & 7)
		fieldNumber := int(tag >> 3)

		if false {
			log.Debugf(dc.ctx, "read %#x : wire type %d , field number %d",
				tag,
				wireType,
				fieldNumber)
		}

		fd := dc.find(int(fieldNumber))
		if fd == nil {
			value, n := decodeWire(wireType, buf)
			if into.Unknown == nil {
				into.Unknown = make(map[int]interface{})
			}
			into.Unknown[int(fieldNumber)] = value
			buf = buf[n:]
		} else {
			fname = *fd.Name
			value, n, mapentry := dc.decodeField(wireType, buf, fd)
			if mapentry {
				v0 := into.Fields[fname]
				var m map[interface{}]interface{}
				if v0 == nil {
					m = make(map[interface{}]interface{})
					into.Fields[fname] = m
				} else {
					m = v0.(map[interface{}]interface{})
				}
				k := value.(*Object).Fields["key"]
				v := value.(*Object).Fields["value"]
				m[k] = v
				if trace {
					log.Debugf(dc.ctx, "decoded <%s> entry [%#v] = %#v",
						*fd.Name,
						k,
						v)
				}
			} else {
				if trace {
					log.Debugf(dc.ctx, "decoded <%s> = %#v (%d bytes)", *fd.Name, value, n)
				}
				if listform[fname] {
					lst := into.Fields[fname].([]interface{})
					lst = append(lst, value)
					into.Fields[fname] = lst
				} else {
					if first, ok := into.Fields[fname]; ok {
						lst := []interface{}{first, value}
						into.Fields[fname] = lst
						listform[fname] = true
					} else {
						into.Fields[fname] = value
					}
				}
			}
			buf = buf[n:]
		}
	}

	kind := dc.Decoder.fs.kind[dc.form]
	if kind != "" {
		if len(into.Fields) == 0 {
			return ptr.Null
		}
		if len(into.Fields) > 1 {
			panic(fmt.Sprintf("object of kind %q has %d fields",
				kind,
				len(into.Fields)))
		}
		entry := into.Fields[fname]
		array, ok := entry.([]byte)
		if !ok {
			panic(fmt.Sprintf("object with kind %q has wrong data",
				kind))
		}
		var p ptr.Ptr
		copy(p.Bits[:], array)
		dc.dst <- p
		return p
	}

	return into
}

func decodeWire(wireType int, buf []byte) (interface{}, int) {
	// not sure what field this is... use the wire
	// type to skip it
	switch wireType {
	case 0:
		value, n := binary.Uvarint(buf)
		return value, n
	case 1:
		value := binary.BigEndian.Uint64(buf)
		return value, 8
	case 2:
		sub, n := binary.Uvarint(buf)
		return buf[n : int(sub)+n], int(sub) + n
	case 5:
		value := binary.LittleEndian.Uint32(buf)
		return value, 4
	default:
		panic("unknown wire type")
	}
}

func skipWire(wireType int, buf []byte) int {
	// not sure what field this is... use the wire
	// type to skip it
	switch wireType {
	case 0:
		_, n := binary.Uvarint(buf)
		return n
	case 1:
		return 8
	case 2:
		sub, n := binary.Uvarint(buf)
		return int(sub) + n
	case 5:
		return 4
	default:
		panic(fmt.Sprintf("unknown wire type %d", wireType))
	}
}

func (dc *decodeCtx) resolveTypeName(name string) *desc.DescriptorProto {
	if t, ok := dc.Decoder.fs.byProtoTypename[name]; ok {
		return t
	}
	panic(name)
}

func (dc *decodeCtx) decodeField(wireType int, buf []byte, fd *desc.FieldDescriptorProto) (interface{}, int, bool) {

	of, err := proto.GetExtension(fd.Options, api.E_OmenObsFormat)
	if err == nil {
		of := of.(*string)

		fmt.Printf("format %q for field %q wire type %d (has extn)\n",
			*of,
			*fd.Name,
			wireType)

		value, n, ismap := dc.decodeFieldRaw(wireType, buf, fd)
		if ismap {
			// assume the option is for the value :/
			oref := &ObsReference{
				Value:  value.(*Object).Fields["value"],
				Format: *of,
			}
			fmt.Printf("format %q for field %q value %v\n",
				oref.Format,
				*fd.Name,
				oref.Value)
			value = &Object{
				Type:    value.(*Object).Type,
				Unknown: value.(*Object).Unknown,
				Fields: map[string]interface{}{
					"key":   value.(*Object).Fields["key"],
					"value": oref,
				},
			}
			return value, n, ismap
		} else {
			oref := &ObsReference{
				Value:  value,
				Format: *of,
			}
			fmt.Printf("format %q for field %q value %v\n",
				oref.Format,
				*fd.Name,
				oref.Value)
			return oref, n, ismap
		}
	}
	fmt.Printf("field %q wire type %d (no extn)\n",
		*fd.Name,
		wireType)
	return dc.decodeFieldRaw(wireType, buf, fd)
}

func (dc *decodeCtx) decodeFieldRaw(wireType int, buf []byte, fd *desc.FieldDescriptorProto) (interface{}, int, bool) {

	switch *fd.Type {

	case desc.FieldDescriptorProto_TYPE_STRING:
		if wireType != 2 {
			panic("expected string to have wire type 2")
		}
		sub, n := binary.Uvarint(buf)
		return string(buf[n : int(sub)+n]), int(sub) + n, false

	case desc.FieldDescriptorProto_TYPE_BYTES:
		if wireType != 2 {
			panic("expected bytes to have wire type 2")
		}
		sub, n := binary.Uvarint(buf)
		return buf[n : int(sub)+n], int(sub) + n, false

	case desc.FieldDescriptorProto_TYPE_MESSAGE:
		if wireType != 2 {
			panic("expected message to have wire type 2")
		}
		sub, n := binary.Uvarint(buf)

		dc2 := dc.fork(dc.resolveTypeName(*fd.TypeName))
		mapent := false
		opt := dc2.form.Options
		if opt != nil && opt.MapEntry != nil {
			mapent = *opt.MapEntry
		}
		return dc2.decodeObj(buf[n : int(sub)+n]), int(sub) + n, mapent

	case desc.FieldDescriptorProto_TYPE_FLOAT:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_DOUBLE:
		if wireType != 1 {
			panic("expected double to have wire type 1")
		}
		value := binary.LittleEndian.Uint64(buf)
		return math.Float64frombits(value), 8, false

	case desc.FieldDescriptorProto_TYPE_UINT64:
		if wireType == 2 {
			value, n := dc.decodePackedField(buf, fd)
			return value, n, false
		}
		if wireType != 0 {
			panic(fmt.Sprintf("expected uint64 to have wire type 0, field %d has wire type %d instead", *fd.Number, wireType))
		}
		value, n := binary.Uvarint(buf)
		return value, n, false

	case desc.FieldDescriptorProto_TYPE_INT32:
		if wireType == 2 {
			value, n := dc.decodePackedField(buf, fd)
			return value, n, false
		}
		if wireType != 0 {
			panic("expected int32 to have wire type 0")
		}
		i, n := binary.Uvarint(buf)
		return int32(i), n, false

	case desc.FieldDescriptorProto_TYPE_INT64:
		value, n := binary.Uvarint(buf)
		return int64(value), n, false

	case desc.FieldDescriptorProto_TYPE_FIXED64:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_FIXED32:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_BOOL:
		if wireType != 0 {
			panic("expected bool to have wire type 0")
		}
		i, n := binary.Uvarint(buf)
		return i != 0, n, false

	case desc.FieldDescriptorProto_TYPE_UINT32:
		value, n := binary.Uvarint(buf)
		return uint32(value), n, false

	case desc.FieldDescriptorProto_TYPE_ENUM:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_SFIXED32:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_SFIXED64:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_SINT32:
		value, n := binary.Varint(buf)
		return int32(value), n, false

	case desc.FieldDescriptorProto_TYPE_SINT64:
		value, n := binary.Varint(buf)
		return value, n, false

	default:
		panic(fmt.Sprintf("unknown data type %#v", *fd.Type))
	}
}

func (dc *decodeCtx) decodePackedField(buf []byte, fd *desc.FieldDescriptorProto) (interface{}, int) {
	sub, n := binary.Uvarint(buf)

	buf = buf[n : int(sub)+n]

	var value interface{}

	switch *fd.Type {

	case desc.FieldDescriptorProto_TYPE_UINT64:
		var lst []uint64
		for len(buf) > 0 {
			i, n := binary.Uvarint(buf)
			lst = append(lst, i)
			buf = buf[n:]
		}
		value = lst

	case desc.FieldDescriptorProto_TYPE_INT32:
		var lst []int32
		for len(buf) > 0 {
			i, n := binary.Uvarint(buf)
			lst = append(lst, int32(i))
			buf = buf[n:]
		}
		value = lst

	case desc.FieldDescriptorProto_TYPE_DOUBLE:
		var lst []float64
		for len(buf) > 0 {
			value := binary.LittleEndian.Uint64(buf)
			lst = append(lst, math.Float64frombits(value))
			buf = buf[8:]
		}
		value = lst

	case desc.FieldDescriptorProto_TYPE_FLOAT:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_INT64:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_FIXED64:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_FIXED32:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_BOOL:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_BYTES:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_UINT32:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_ENUM:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_SFIXED32:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_SFIXED64:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_SINT32:
		panic("unsupported")

	case desc.FieldDescriptorProto_TYPE_SINT64:
		panic("unsupported")

	default:
		panic(fmt.Sprintf("unknown data type %#v", *fd.Type))
	}
	return value, int(sub) + n
}
