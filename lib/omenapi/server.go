package omenapi

import (
	"context"

	"bitbucket.org/dkolbly/omen3/api"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/rpc"
)

func NewAtomService(backend db.AtomSupplier) *AtomService {
	return &AtomService{
		backend: backend,
	}
}

func NewStateService(backend db.StateSupplier) *StateService {
	return &StateService{
		backend: backend,
	}
}

type AtomService struct {
	backend db.AtomSupplier
}

type StateService struct {
	backend db.StateSupplier
}

// Expose implements rpc.Service.  It returns a list of methods
// on that are entry points for the omenapi service.
func (s *AtomService) Expose() []string {
	return []string{
		"Load",
		"Store",
	}
}

// Accept implements rpc.Service
func (s *AtomService) Accept(ctx context.Context, conn rpc.Connection) rpc.Handler {
	return s
}

func (s *AtomService) DidClose(_ context.Context) {
}

func (s *StateService) Expose() []string {
	return []string{
		"WaitForChange",
		"Apply",
	}
}

func (s *StateService) Accept(ctx context.Context, conn rpc.Connection) rpc.Handler {
	return s
}

func (s *StateService) DidClose(_ context.Context) {
}

func (s *StateService) WaitForChange(ctx context.Context, req *api.WaitForChange) (*api.WaitForChange, error) {
	local := make([]*db.State, len(req.Items))
	for i, s := range req.Items {
		local[i] = &db.State{
			Name:     s.Name,
			Sequence: s.Seq,
		}
	}
	ret, err := s.backend.WaitForChange(ctx, local)
	if err != nil {
		return nil, err
	}
	answer := &api.WaitForChange{
		Items: make([]*api.State, len(ret)),
	}
	for i, s := range ret {
		answer.Items[i] = s.ToAPI()
	}
	return answer, nil
}

func (s *AtomService) Load(ctx context.Context, req *api.LoadAtomRequest) (*api.LoadAtomResponse, error) {
	p := db.PtrFromAPI(req.Subjects[0])

	a, err := s.backend.Load(ctx, p)
	if err != nil {
		return nil, err
	}
	ret := &api.Atom{
		Subject: req.Subjects[0],
		Payload: a.Payload,
	}
	if !a.Type.IsNull() {
		ret.Type = db.APIFromPtr(a.Type)
	}
	return &api.LoadAtomResponse{
		Atoms: []*api.Atom{ret},
	}, nil
}

func (s *StateService) Apply(ctx context.Context, req *api.ApplyRequest) (*api.ApplyResponse, error) {
	txr := db.NewTxnFromAPI(req.Txn)

	id, ts, lst, err := s.backend.Apply(ctx, txr)
	if err != nil {
		return nil, err
	}

	slist := make([]*api.State, len(lst))
	for i, s := range lst {
		slist[i] = s.ToAPI()
	}
	ret := &api.ApplyResponse{
		Id:     id,
		Ts:     ts.UnixNano(),
		States: slist,
	}
	return ret, nil
}
