// Package omentest provides facilities for writing tests of
// libraries and facilities that use the omen API
package omentest

import (
	"context"
	"errors"
	"fmt"
	"time"

	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
)

const Trace = false

type Omen struct {
	Atoms   map[ptr.Ptr]*db.Atom
	Current map[ptr.Ptr]ptr.Ptr
	id      uint64
}

func New() *Omen {
	return &Omen{
		Atoms:   make(map[ptr.Ptr]*db.Atom),
		Current: make(map[ptr.Ptr]ptr.Ptr),
		id:      9,
	}
}

func (omen *Omen) Begin(ctx context.Context) *TestTx {
	return &TestTx{
		owner:  omen,
		ctx:    context.Background(),
		states: make(map[string]*db.State),
		edited: make(map[string]bool),
	}
}

func (omen *Omen) WaitForChange(ctx context.Context, given []*db.State) ([]*db.State, error) {
	panic("TODO")
}

func (omen *Omen) Apply(ctx context.Context, tx *db.TxnRecord) (uint64, time.Time, []*db.State, error) {

	for _, pre := range tx.Prereqs {
		key := ptr.Obs(pre.Name)
		if omen.Current[key] != pre.Value {
			return 0, time.Time{}, nil, ErrConflict
		}
	}

	t := time.Now()
	omen.id++
	var lst []*db.State
	for _, ed := range tx.Edits {
		key := ptr.Obs(ed.Name)
		omen.Current[key] = ed.Value
		lst = append(lst, &db.State{
			Sequence: omen.id,
			Value:    ed.Value,
			Name:     ed.Name,
			Modified: t,
		})
	}

	return omen.id, t, lst, nil
}

type TestTx struct {
	ctx     context.Context
	owner   *Omen
	states  map[string]*db.State
	basedon map[string]ptr.Ptr
	edited  map[string]bool
}

func (tx *TestTx) Obs(ctx context.Context, name string) (*db.State, error) {
	if s, ok := tx.states[name]; ok {
		return s, nil
	}
	return nil, ptr.ErrNotPresent(ptr.Obs(name))
}

func (tx *TestTx) Set(ctx context.Context, name string, value ptr.Ptr) {
	tx.edited[name] = true
	tx.states[name] = &db.State{
		Name:  name,
		Value: value,
	}
}

func (tx *TestTx) Load(ctx context.Context, addr ptr.Ptr) (*db.Atom, error) {
	if Trace {
		fmt.Printf("Load(%s) of %d\n", addr, len(tx.owner.Atoms))
	}
	if data, ok := tx.owner.Atoms[addr]; ok {
		return data, nil
	}
	return nil, ptr.ErrNotPresent(addr)
}

func (tx *TestTx) Store(ctx context.Context, t db.DataType, data []byte) (*db.Atom, error) {
	clone := make([]byte, len(data))
	copy(clone, data)

	atom := &db.Atom{
		Payload: clone,
	}

	if t == nil {
		atom.Addr = ptr.ContentAddress(ptr.Untyped, clone)
	} else {
		atom.Type = t.Addr()
		atom.Addr = ptr.TypedContentAddress(atom.Type, clone)
	}

	if a, ok := tx.owner.Atoms[atom.Addr]; ok {
		return a, nil // already exists
	}
	if Trace {
		if atom.Addr.Metatype() == ptr.HasTypePointer {
			fmt.Printf("AtomStore(%s : %x) => %s\n", atom.Type, clone, atom.Addr)
		} else {
			fmt.Printf("AtomStore(%x) => %s\n", clone, atom.Addr)
		}
	}
	tx.owner.Atoms[atom.Addr] = atom
	return atom, nil
}

var ErrConflict = errors.New("concurrent conflicting change")

func (tx *TestTx) Commit(ctx context.Context) error {
	o := tx.owner

	for name := range tx.edited {
		key := ptr.Obs(name)
		old := o.Current[key]
		if tx.basedon[name] != old {
			return ErrConflict
		}
	}

	for name := range tx.edited {
		key := ptr.Obs(name)
		o.Current[key] = tx.states[name].Value
		fmt.Printf("Update(%s) => %s\n",
			name,
			tx.states[name].Value,
		)
	}

	return nil
}

func (tx *TestTx) Context() context.Context {
	return tx.ctx
}

func (tx *TestTx) Alloc(ctx context.Context) uint64 {
	tx.owner.id++
	return tx.owner.id
}

func __ensureCompatibleWithInterface(_ db.Txn) {
}

func init() {
	__ensureCompatibleWithInterface(&TestTx{})
}

func (tx *TestTx) AlreadyObserved(name string) *db.State {
	if here, ok := tx.states[name]; ok {
		return here
	}
	if p, ok := tx.owner.Current[ptr.Obs(name)]; ok {
		return &db.State{
			Name:     name,
			Value:    p,
			Sequence: 3,
		}
	}
	return nil
}

func (tx *TestTx) FlushObservations([]*db.State) {
}

func (tx *TestTx) StateSupplierReveal() db.StateSupplier {
	return tx.owner
}
