package db

import (
	"context"
	"sync"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/omen3/lib/mime"
	"bitbucket.org/dkolbly/omen3/ptr"
)

var log = logging.New("omen3.db")

type DataType interface {
	// Scan looks at the payload of an object of the given type
	// and finds all the pointers in it
	Scan([]byte) ([]ptr.Ptr, error)
	// Addr returns the address of the type itself, which will
	// be installed in the header of the stored object
	Addr() ptr.Ptr
}

func PtrForData(dt DataType, buf []byte) ptr.Ptr {
	if dt == nil {
		return ptr.ContentAddress(ptr.Untyped, buf)
	}
	return ptr.TypedContentAddress(dt.Addr(), buf)
}

// a more powerful decoding interface, useful for introspection
type Decoder interface {
	Decode(context.Context, []byte) (interface{}, []ptr.Ptr)
}

// a trivial implementation of a DataType that doesn't
// know how to scan (or has nothing *to* scan) but has a pointer

type ScanlessType ptr.Ptr

func (s ScanlessType) Scan([]byte) ([]ptr.Ptr, error) {
	return nil, nil
}

func (s ScanlessType) Addr() ptr.Ptr {
	return ptr.Ptr(s)
}

// an optional interface for a DataType that allows it to specify
// the set of atoms needed to elaborate the datatype from a persistent
// representation
type Atomser interface {
	Atoms() []*Atom
}

type DataTypeHydrater interface {
	DataTypeHydrate(context.Context, ptr.Ptr, *Atom, Loader) (DataType, bool, error)
}

var hydraters []DataTypeHydrater
var metatypeCache = make(map[ptr.Ptr]DataTypeHydrater)
var hydrationCache = make(map[ptr.Ptr]DataType)
var hydrationCacheLock sync.Mutex

func RegisterTypeHydrater(h DataTypeHydrater) {
	hydraters = append(hydraters, h)
}

func HydrateDataType(ctx context.Context, p ptr.Ptr, ld Loader) (DataType, error) {
	hydrationCacheLock.Lock()
	dt, ok := hydrationCache[p]
	hydrationCacheLock.Unlock()

	if ok {
		return dt, nil
	}

	dt, err := findHydration(ctx, p, ld)
	if err != nil {
		return nil, err
	}
	hydrationCacheLock.Lock()
	hydrationCache[p] = dt
	hydrationCacheLock.Unlock()
	return dt, nil
}

func findHydration(ctx context.Context, p ptr.Ptr, ld Loader) (DataType, error) {

	// see if it's in the mime type map
	if _, ok := mime.TypeUnmap[p]; ok {
		log.Debugf(ctx, "%q is a mime type", p.String())
		return ScanlessType(p), nil
	}

	// see if the type pointer *itself* is loadable
	var typeatom *Atom
	if p.IsLoadable() {
		var err error
		typeatom, err = ld.Load(ctx, p)
		if err != nil {
			log.Errorf(ctx, "failed to load type atom <%s> : %s", p, err)
			return nil, err
		}
	}

	for _, h := range hydraters {
		dt, ok, err := h.DataTypeHydrate(ctx, p, typeatom, ld)
		if err != nil {
			log.Errorf(ctx, "failed to resolved type <%s> : %s", p, err)
			return nil, err
		}
		if ok {
			log.Debugf(ctx, "resolved type <%s> as %T", p, dt)
			return dt, nil
		}
	}

	// should we return an unscannable type, or record an error?
	if p.IsString() {
		log.Warningf(ctx, "could not resolve type %q", p.StringValue())
	} else {
		log.Warningf(ctx, "could not resolve type <%s>", p)
	}
	return ScanlessType(p), nil
}

func Scan(as DataType, buf []byte) ([]ptr.Ptr, error) {
	if as == nil {
		return nil, nil
	}
	lst, err := as.Scan(buf)
	if err != nil {
		return nil, err
	}
	p := as.Addr()
	if p.IsLoadable() {
		lst = append(lst, p)
	}
	return lst, nil
}
