package db

type WaitFor struct {
	Name  string
	After uint64
}
