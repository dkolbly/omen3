package db

import (
	"time"

	"bitbucket.org/dkolbly/omen3/api"
	"bitbucket.org/dkolbly/omen3/ptr"
)

type State struct {
	Name     string
	Value    ptr.Ptr
	Modified time.Time
	Sequence uint64
	Audit    uint64
}

func (s *State) ToAPI() *api.State {
	a := ptr.Obs(s.Name)

	return &api.State{
		Addr:  &api.Ptr{a.Bits[:]},
		Name:  s.Name,
		Value: &api.Ptr{s.Value.Bits[:]},
		Mtime: uint64(s.Modified.UnixNano()),
		Seq:   s.Sequence,
		Audit: s.Audit,
	}
}

func NewStateFromAPI(s *api.State) *State {
	if s.Value == nil {
		// this is the case when the obs is undefined
		return &State{
			Name: s.Name,
		}
	}

	var p ptr.Ptr
	copy(p.Bits[:], s.Value.Addr)

	return &State{
		Name:     s.Name,
		Value:    p,
		Modified: time.Unix(0, int64(s.Mtime)),
		Sequence: s.Seq,
		Audit:    s.Audit,
	}
}
