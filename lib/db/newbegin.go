package db

import (
	"context"
	"sync"
	"time"

	"bitbucket.org/dkolbly/omen3/ptr"
)

type simpleBeginner struct {
	next    Store
	lock    sync.Mutex
	flushed map[DataType]struct{}
}

// NewBeginner provides a simple transaction interface on top of a
// basic store
func NewBeginner(s Store) StoreBeginner {
	return &simpleBeginner{
		next:    s,
		flushed: make(map[DataType]struct{}),
	}
}

type simpleTxn struct {
	backing       *simpleBeginner
	loaded        int
	stored        int
	alloced       int
	prereqs       map[string]*State
	edits         map[string]int
	record        TxnRecord
	commitResults []*State
	start         time.Time
}

func (s *simpleTxn) Final() *Disposition {
	if s.record.ID == 0 {
		return nil
	}
	states := make(map[string]*State, len(s.commitResults))
	for _, state := range s.commitResults {
		states[state.Name] = state
	}
	return &Disposition{
		Record:       s.record,
		AtomsRead:    s.loaded,
		AtomsWritten: s.stored,
		IDsAllocated: s.alloced,
		Committed:    states,
	}
}

func (b *simpleBeginner) Begin(ctx context.Context, audit uint64) Txn {
	return &simpleTxn{
		backing: b,
		record: TxnRecord{
			Audit: audit,
		},
		prereqs: make(map[string]*State),
		edits:   make(map[string]int),
		start:   time.Now(),
	}
}

func (t *simpleTxn) Commit(ctx context.Context) error {
	txid, tm, states, err := t.backing.Apply(ctx, &t.record)
	if err != nil {
		return err
	}
	t.record.ID = txid
	t.record.Timestamp = tm
	t.commitResults = states
	return nil
}

func (t *simpleTxn) Set(ctx context.Context, name string, value ptr.Ptr) {
	if i, ok := t.edits[name]; ok {
		t.record.Edits[i].Value = value
	} else {
		t.edits[name] = len(t.edits)
		t.record.Edits = append(t.record.Edits, Edit{
			Name:  name,
			Value: value,
		})
	}
}

func (t *simpleTxn) applyEdit(name string, old *State) *State {
	if i, ok := t.edits[name]; ok {
		var seq uint64
		if old != nil {
			seq = old.Sequence
		}
		return &State{
			Name:     name,
			Value:    t.record.Edits[i].Value,
			Modified: t.start,
			Sequence: seq + 1,
			Audit:    t.record.Audit,
		}
	}
	return old
}

func (t *simpleTxn) Obs(ctx context.Context, name string) (*State, error) {
	if s, ok := t.prereqs[name]; ok {
		// TODO what about the READ-MISSING DELETE READ case?
		s = t.applyEdit(name, s)
		if s == nil {
			return nil, ptr.ErrNotPresent(ptr.Obs(name))
		}
		return s, nil
	}
	log.Debugf(ctx, "backing is  %T", t.backing)
	s, err := t.backing.Obs(ctx, name)
	if err != nil {
		if ptr.IsNotPresent(err) {
			// remember this error for later
			t.prereqs[name] = nil
		}
		return nil, err
	}
	t.prereqs[name] = s
	// note that it could have been modified already
	// TODO! think about the prereq semantics of a
	//    WRITE
	//    READ
	// sequence
	return t.applyEdit(name, s), nil
}

func (t *simpleTxn) Load(ctx context.Context, p ptr.Ptr) (*Atom, error) {
	t.loaded++
	return t.backing.Load(ctx, p)
}

func (t *simpleTxn) Store(ctx context.Context, dt DataType, payload []byte) (*Atom, error) {
	t.stored++
	return t.backing.Store(ctx, dt, payload)
}

func (t *simpleTxn) Alloc(ctx context.Context) uint64 {
	t.alloced++
	return t.backing.Alloc(ctx)
}

func (s *simpleBeginner) Load(ctx context.Context, addr ptr.Ptr) (*Atom, error) {
	return s.next.Load(ctx, addr)
}

func (s *simpleBeginner) Obs(ctx context.Context, name string) (*State, error) {
	log.Debugf(ctx, "next beginner is %T", s.next)
	return s.next.Obs(ctx, name)
}

func (s *simpleBeginner) Store(ctx context.Context, dt DataType, buf []byte) (*Atom, error) {
	s.flushTypes(ctx, dt)
	return s.next.Store(ctx, dt, buf)
}

func (s *simpleBeginner) Apply(ctx context.Context, txr *TxnRecord) (uint64, time.Time, []*State, error) {
	return s.next.Apply(ctx, txr)
}

func (s *simpleBeginner) Alloc(ctx context.Context) uint64 {
	return s.next.Alloc(ctx)
}

func (s *simpleBeginner) flushTypes(ctx context.Context, dt DataType) {
	s.lock.Lock()
	defer s.lock.Unlock()

	if _, ok := s.flushed[dt]; ok {
		return
	}
	s.flushed[dt] = struct{}{}

	atomser, ok := dt.(Atomser)
	if !ok {
		return
	}

	for _, a := range atomser.Atoms() {
		var t DataType
		if !a.Type.IsNull() {
			t = ScanlessType(a.Type)
		}
		a2, err := s.next.Store(ctx, t, a.Payload)
		if err == nil {
			if a2.Addr != a.Addr {
				panic("addr mismatch")
			}
		}
	}
}
