package db

import (
	"errors"
	"time"

	"bitbucket.org/dkolbly/omen3/api"
	"bitbucket.org/dkolbly/omen3/ptr"
)

var ErrConcurrencyFailure = errors.New("concurrency failure")

// This is the data representation of a transaction.  A transaction
// includes a set of prerequistes, which are used for optimistic
// concurrency control, and a set of state changes.  The whole thing
// gets a non-zero transaction ID assigned by the underlying storage
// system, and a non-zero audit ID optionally assigned by the
// application (by convention, if the audit ID is not set by the
// application, it is set to the transaction id)
type TxnRecord struct {
	ID        uint64
	Audit     uint64
	Timestamp time.Time
	Prereqs   []Prereq
	Edits     []Edit
}

type Prereq struct {
	Name  string  `json:"name"`
	Value ptr.Ptr `json:"value"`
}

type Edit struct {
	Name  string  `json:"name"`
	Value ptr.Ptr `json:"value"`
}

func NewTxnFromAPI(t *api.Txn) *TxnRecord {
	var ts time.Time
	if t.Ts != 0 {
		ts = time.Unix(0, int64(t.Ts))
	}
	var plst []Prereq
	if len(t.Prereqs) > 0 {
		plst = make([]Prereq, len(t.Prereqs))
		for i, t := range t.Prereqs {
			plst[i].Name = t.Name
			if t.Value != nil {
				plst[i].Value = PtrFromAPI(t.Value)
			}
		}
	}

	var elst []Edit
	if len(t.Edits) > 0 {
		elst = make([]Edit, len(t.Edits))
		for i, t := range t.Edits {
			elst[i].Name = t.Name
			if t.Value != nil {
				elst[i].Value = PtrFromAPI(t.Value)
			}
		}
	}
	return &TxnRecord{
		ID:        t.Id,
		Audit:     t.Audit,
		Timestamp: ts,
		Prereqs:   plst,
		Edits:     elst,
	}
}

func (tx *TxnRecord) ToAPI() *api.Txn {
	tuple := func(name string, value ptr.Ptr) *api.Tuple {
		if value.IsNull() {
			return &api.Tuple{
				Name: name,
			}
		}
		return &api.Tuple{
			Name:  name,
			Value: APIFromPtr(value),
		}
	}

	plst := make([]*api.Tuple, len(tx.Prereqs))
	for i, t := range tx.Prereqs {
		plst[i] = tuple(t.Name, t.Value)
	}

	elst := make([]*api.Tuple, len(tx.Edits))
	for i, t := range tx.Edits {
		elst[i] = tuple(t.Name, t.Value)
	}

	return &api.Txn{
		Id:      tx.ID,
		Audit:   tx.Audit,
		Ts:      tx.Timestamp.UnixNano(),
		Prereqs: plst,
		Edits:   elst,
	}
}
