package db

import (
	"context"
	"time"

	"bitbucket.org/dkolbly/omen3/ptr"
)

type Loader interface {
	Load(context.Context, ptr.Ptr) (*Atom, error)
}

type Obser interface {
	Obs(context.Context, string) (*State, error)
}

type Storer interface {
	Store(context.Context, DataType, []byte) (*Atom, error)
}

type Store interface {
	Loader
	Obser
	Storer
	Apply(context.Context, *TxnRecord) (uint64, time.Time, []*State, error)
	Alloc(context.Context) uint64
}

type Beginner interface {
	Begin(context.Context, uint64) Txn
}

type StoreBeginner interface {
	Store
	Beginner
}

type Txn interface {
	Loader
	Obser
	Storer
	Alloc(context.Context) uint64
	Set(context.Context, string, ptr.Ptr)
	Commit(context.Context) error
}

type Disposition struct {
	Record       TxnRecord
	Committed    map[string]*State
	AtomsRead    int
	AtomsWritten int
	IDsAllocated int
}

type Finaler interface {
	Final() *Disposition
}

type StateSupplier interface {
	WaitForChange(context.Context, []*State) ([]*State, error)
	Apply(context.Context, *TxnRecord) (uint64, time.Time, []*State, error)
}

type AtomSupplier interface {
	Load(context.Context, ptr.Ptr) (*Atom, error)
	Store(context.Context, DataType, []byte) (*Atom, error)
}

func Get(ctx context.Context, s StateSupplier, name string) (*State, error) {
	initial := &State{Name: name}
	lst, err := s.WaitForChange(ctx, []*State{initial})
	if err != nil {
		if ptr.IsNotPresent(err) {
			return initial, nil
		}
		return nil, err
	}
	return lst[0], nil
}
