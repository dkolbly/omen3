package db

import (
	"fmt"

	"bitbucket.org/dkolbly/omen3/api"
	"bitbucket.org/dkolbly/omen3/ptr"
)

type Atom struct {
	Addr    ptr.Ptr
	Type    ptr.Ptr // ptr.Null if untyped data
	Payload []byte  // does not include the type header, if HasTypePointer
}

func PtrFromAPI(a *api.Ptr) (p ptr.Ptr) {
	copy(p.Bits[:], a.Addr)
	return
}

func APIFromPtr(p ptr.Ptr) *api.Ptr {
	// as a general principle, we never encode invalid values, *including*
	// null values
	if !p.IsValid() {
		panic(fmt.Sprintf("refusing to encode an invalid value %s", p.String()))
	}

	return &api.Ptr{
		Addr: p.Bits[:],
	}
}
