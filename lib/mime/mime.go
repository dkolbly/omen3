package mime

import (
	"bitbucket.org/dkolbly/omen3/ptr"
)

// TypeMap maps from MIME types to the corresponding omen3 type Ptr,
// which are (in most cases, but try not to rely on that) represented
// as string literals
var TypeMap = map[string]ptr.Ptr{
	"application/json":       ptr.String("mime:a/json"),
	"application/javascript": ptr.String("mime:a/javascript"),
	"application/gzip":       ptr.String("mime:a/gzip"),
	"application/pdf":        ptr.String("mime:a/pdf"),
	"application/zip":        ptr.String("mime:a/zip"),
	"application/xml":        ptr.String("mime:a/xml"),
	"image/png":              ptr.String("mime:i/png"),
	"image/jpeg":             ptr.String("mime:i/jpeg"),
	"image/svg+xml":          ptr.String("mime:i/svg+xml"),
	"text/html":              ptr.String("mime:t/html"),
	"text/css":               ptr.String("mime:t/css"),
	"text/csv":               ptr.String("mime:t/csv"),
	"text/plain":             ptr.String("mime:t/plain"),
	"application/font-sfnt":  ptr.String("mime:a/font-sfnt"), // ttf + otf
	"application/font-woff":  ptr.String("mime:a/font-woff"),
}

// TypeUnmap maps from omen3 type Ptr to the corresponding MIME type
var TypeUnmap = make(map[ptr.Ptr]string, 10)

func init() {
	for mime, ptr := range TypeMap {
		TypeUnmap[ptr] = mime
	}
}
