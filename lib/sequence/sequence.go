package sequence

import (
	"context"
	"math/big"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/mixup/varmix"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
)

var log = logging.New("sequence")

type Sequence struct {
	lock   sync.Mutex
	name   string
	store  db.Beginner
	remain int
	next   uint64
	batch  int
	prev   int
	mix    *varmix.Mixer
	min    uint64
}

func New(store db.Beginner, name string, mixkey string) *Sequence {
	var mix *varmix.Mixer
	if mixkey != "" {
		mix = varmix.New(mixkey)
	}
	return &Sequence{
		name:  name,
		store: store,
		batch: 1,
		prev:  1,
		mix:   mix,
		min:   1,
	}
}

func (s *Sequence) Min(min uint64) *Sequence {
	s.min = min
	return s
}

func (s *Sequence) Next(ctx context.Context) uint64 {
	s.lock.Lock()
	defer s.lock.Unlock()

	for s.remain <= 0 {
		delta := int64(s.batch)

		sum, err := s.SumMin(ctx, delta, &s.min)

		if err != nil {
			log.Warningf(ctx, "failed to get seq %q batch: %s", s.name, err)
			time.Sleep(100 * time.Millisecond)
		} else if uint64(sum) == s.min {
			log.Debugf(ctx, "all we did was set the min value")
		} else {

			if s.batch < 1000 {
				s.prev, s.batch = s.batch, s.batch+s.prev
				log.Debugf(ctx, "upgraded (next) batch size to %d", s.batch)
			}
			s.remain = int(delta)
			log.Debugf(ctx, "sum %d delta %d", sum, delta)
			if sum-delta < 0 {
				panic("negative start")
			}
			s.next = uint64(sum - delta)
			if s.next < s.min {
				// discard ids lower than min
				s.next++
				s.remain--
			}
			log.Debugf(ctx, "allocated %d starting at %d", s.remain, s.next)
		}
	}
	id := s.next
	s.next++
	s.remain--
	log.Debugf(ctx, "Next() returning M(%d) (remain=%d)", id, s.remain)

	if s.mix != nil {
		return s.mix.VarUint(id)
	}
	return id
}

func (s *Sequence) Sum(ctx context.Context, incr int64) (int64, error) {
	return s.SumMin(ctx, incr, nil)
}

func (s *Sequence) SumMin(ctx context.Context, incr int64, min *uint64) (int64, error) {
	t := uint64(time.Now().UnixNano())
	t = t & ^uint64(7)
	tx := s.store.Begin(ctx, t)
	sum, err := AtomicAdd(ctx, tx, s.name, incr, min)
	if err != nil {
		return 0, err
	}
	err = tx.Commit(ctx)
	if err != nil {
		return 0, err
	}
	return sum, nil
}

func AtomicAdd(ctx context.Context, tx db.Txn, name string, incr int64, min *uint64) (int64, error) {

	state, err := tx.Obs(ctx, name)
	var current *big.Int

	if err != nil {
		if !ptr.IsNotPresent(err) {
			return 0, err
		}
		current = big.NewInt(0)
	} else if state.Value.IsNull() {
		current = big.NewInt(0)
	} else {
		current = state.Value.IntValue()
	}

	next := big.NewInt(incr)
	next.Add(next, current)

	if min != nil {
		//TODO this will fail if min >= 2^63
		minv := big.NewInt(int64(*min))
		if next.Cmp(minv) < 0 {
			log.Debugf(ctx, "%d + %d < %d", current, incr, *min)
			next = minv
		}
	}

	tx.Set(ctx, name, ptr.Int(next))
	return next.Int64(), nil
}
