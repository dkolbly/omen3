package biglist

import (
	"context"

	"bitbucket.org/dkolbly/omen3/ptr"
)

func (n *Node) Get(ctx context.Context, tx Loader, index uint64) (ret ptr.Ptr, ok bool) {

	scanleaf := func(leaf []link, origin uint64) {
		max := origin + uint64(len(leaf))
		if index < max {
			ret = leaf[index-origin].ptr
			ok = true
		} else {
			panic("hmm, why are we here?")
		}
	}

	var scantree func(in *Indirect, origin uint64)

	scantree = func(in *Indirect, origin uint64) {
		if in.depth == 1 {
			// we are at a leaf node
			scanleaf(in.children, origin)
			return
		}

		// consider each of the children...
		for _, l := range in.children {
			max := origin + l.sum
			if index < max {
				// we found a segment that it's in... recurse
				sub := loadTree(ctx, tx, l.ptr)
				scantree(sub, origin)
				return
			}
			// consume
			origin += l.sum
		}
	}

	nd := len(n.depths)
	var origin uint64

	for i := nd; i > 0; i-- {
		// check at depth i
		for _, l := range n.depths[i-1] {
			max := origin + l.sum
			if index < max {
				// recurse
				sub := loadTree(ctx, tx, l.ptr)
				scantree(sub, origin)
				return
			}
			// consume
			origin += l.sum
		}
	}

	// didn't find in any subtrees -- must be in direct, if anywhere
	max := origin + uint64(len(n.direct))
	if index < max {
		ret = n.direct[index-origin]
		ok = true
	}
	return
}
