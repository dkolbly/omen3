package biglist

import (
	"context"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/omen3/ptr"
)

var log = logging.New("biglist")

func (n *Node) Slice(ctx context.Context, tx Loader, from, to uint64) <-chan ptr.Ptr {
	skip := n.skip
	if from < skip {
		panic("trying to start slice before data")
	}
	ch := make(chan ptr.Ptr)
	go n.slicepump(ctx, ch, tx, from-skip, to-skip)
	return ch
}

func (n *Node) slicepump(ctx context.Context, ch chan<- ptr.Ptr, tx Loader, from, to uint64) {
	defer close(ch)
	if to <= from {
		// done already
		return
	}

	send := func(item ptr.Ptr) bool {
		select {
		case <-ctx.Done():
			return false
		case ch <- item:
			return true
		}
	}

	// clip() returns the span of the original request (i.e., (from,to))
	// but clipped to the span under consideration (origin,max)
	clip := func(origin, max uint64) (uint64, uint64) {
		a := from
		b := to
		if a < origin {
			a = origin
		}
		if b > max {
			b = max
		}
		/*log.Infof(ctx, "clip request(%d,%d) x span(%d,%d) => (%d,%d)",
		from, to, origin, max, a, b)*/
		return a, b
	}

	scanleaf := func(leaf []link, origin uint64) bool {
		max := origin + uint64(len(leaf))
		if from >= max {
			return true
		} else if to <= origin {
			return true
		}
		a, b := clip(origin, max)
		// (a,b) is the (from,to) but clipped to the region
		// covered by this leaf
		if b <= a {
			panic("unexpected empty span")
		}
		// here is the number of items we can serve out of this leaf
		here := int(b - a)
		// k is the offset in this leaf of the first item to serve
		k := int(a - origin)
		for i := 0; i < here; i++ {
			if !send(leaf[i+k].ptr) {
				return false
			}
		}
		return true
	}

	var scantree func(in *Indirect, origin uint64) bool

	scantree = func(in *Indirect, origin uint64) bool {
		//log.Infof(ctx, "scantree(depth=%d) origin=%d", in.depth, origin)
		if in.depth == 1 {
			// we are at a leaf node
			return scanleaf(in.children, origin)
		}

		// consider each of the children...
		for _, l := range in.children {
			max := origin + l.sum
			if from < max && to > origin {
				// recurse
				sub := loadTree(ctx, tx, l.ptr)
				if !scantree(sub, origin) {
					return false
				}
			}
			// consume
			origin += l.sum
		}
		return true
	}

	// find the start location

	nd := len(n.depths)
	var origin uint64

	for i := nd; i > 0; i-- {
		// check at depth i
		for _, l := range n.depths[i-1] {
			max := origin + l.sum
			if from < max && to > origin {
				// recurse
				sub := loadTree(ctx, tx, l.ptr)
				if !scantree(sub, origin) {
					return
				}
			}
			// consume
			origin += l.sum
		}
	}

	// finally, check the directs

	max := origin + uint64(len(n.direct))

	if from < max && to > origin {

		/*for i, p := range n.direct {
			log.Infof(ctx, " direct[%d] = %s", i, p)
		}*/

		a, b := clip(origin, max)
		// (a,b) is the (from,to) but clipped to the region
		// covered by this leaf
		if b <= a {
			panic("unexpected empty span")
		}
		// here is the number of items we can serve directly from here
		here := int(b - a)
		// k is the offset in the direct list of the first item to serve
		k := int(a - origin)
		//log.Infof(ctx, "%d here at offset +%d", here, k)
		for i := 0; i < here; i++ {
			if !send(n.direct[i+k]) {
				return
			}
		}
	}
}

/*
var xx = model.MustDecodeBytesPtr([]byte("__________________________________________u"))
var yy = model.MustDecodeBytesPtr([]byte("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAu"))
var zz = model.MustDecodeBytesPtr([]byte("m00000000000000000000000000000000000000000u"))
*/
