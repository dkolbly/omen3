package biglist

import (
	"bytes"
	"context"
	"encoding/binary"

	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
)

type Loader interface {
	Load(context.Context, ptr.Ptr) (*db.Atom, error)
}

var MaxDirect = 50 // max of 255 for encoding reasons
var Fanout = 20    // max of 255 for encoding reasons

type link struct {
	sum uint64
	ptr ptr.Ptr
}

type Indirect struct {
	depth    uint8
	children []link
}

type Node struct {
	skip   uint64
	depths [][]link  // indirect links ([0] = single-indirect)
	direct []ptr.Ptr // direct objects
}

func copydepths(src [][]link) [][]link {
	dst := make([][]link, len(src))
	copy(dst, src)
	return dst
}

func copylinks(src []link) []link {
	dst := make([]link, len(src))
	copy(dst, src)
	return dst
}

func New() *Node {
	return new(Node)
}

func MustLoad(ctx context.Context, tx db.Txn, ptr ptr.Ptr) *Node {
	atom, err := tx.Load(ctx, ptr)
	if err != nil {
		panic(err)
	}
	return FromAtom(atom)
}

func FromAtom(a *db.Atom) *Node {
	if a.Type != listTypePtr {
		panic("invalid list type: " + a.Type.String())
	}
	return deserzList(a.Payload)
}

func appendlink(src []link, item link) []link {
	dst := make([]link, len(src)+1)
	copy(dst, src)
	dst[len(src)] = item
	return dst
}

func appendcopy(src []ptr.Ptr, item ptr.Ptr) []ptr.Ptr {
	dst := make([]ptr.Ptr, len(src)+1)
	copy(dst, src)
	dst[len(src)] = item
	return dst
}

func (n *Node) storeAtom(ctx context.Context, tx db.Txn) (*db.Atom, error) {
	_, data := n.serz()
	return tx.Store(ctx, ListType, data)
}

func (in *Indirect) storeAtom(ctx context.Context, tx db.Txn) (*db.Atom, error) {
	_, data := in.serz()
	return tx.Store(ctx, TreeType, data)
}

func (in *Indirect) mustStore(ctx context.Context, tx db.Txn) ptr.Ptr {
	atom, err := in.storeAtom(ctx, tx)
	if err != nil {
		panic(err)
	}
	return atom.Addr
}

func (n *Node) MustStore(ctx context.Context, tx db.Txn) ptr.Ptr {
	atom, err := n.storeAtom(ctx, tx)
	if err != nil {
		panic(err)
	}
	return atom.Addr
}

// make room for a new direct pointer
func (n *Node) makeDirectRoom(ctx context.Context, tx db.Txn) *Node {
	if len(n.direct) < MaxDirect {
		return n
	}

	depths := make([][]link, len(n.depths))
	copy(depths, n.depths)

	// collapse everything we need to limit fanout, because
	// we're going to be adding an indirect
	for k := range depths {
		// consider indirect depth (k+1)
		if len(depths[k]) < Fanout {
			// adding one is fine; done
			break
		}

		// depth (k+1) has reached fanout; roll it up
		// into a new depth (k+2) indirect node

		super := &Indirect{
			depth:    uint8(k + 2),
			children: make([]link, len(depths[k])),
		}
		// attach the children
		var total uint64
		for i, l := range depths[k] {
			total += l.sum
			super.children[i] = l
		}
		// and record the new indirect
		l := link{
			sum: total,
			ptr: super.mustStore(ctx, tx),
		}
		if k+1 >= len(depths) {
			// this is the first time we've reached this depth
			depths = append(depths, []link{l})
		} else {
			depths[k+1] = appendlink(depths[k+1], l)
		}
		depths[k] = nil // these are now flushed
	}

	// now add the directs from this node onto the list of depth-1
	// indirects, which we know there is room for
	si := &Indirect{
		depth:    1,
		children: make([]link, len(n.direct)),
	}
	for i, p := range n.direct {
		si.children[i].sum = 1
		si.children[i].ptr = p
	}
	sil := link{
		sum: uint64(len(n.direct)),
		ptr: si.mustStore(ctx, tx),
	}
	if 0 >= len(depths) {
		// this is the first time we've added any depth
		depths = append(depths, []link{sil})
	} else {
		depths[0] = appendlink(depths[0], sil)
	}

	return &Node{
		depths: depths,
		direct: nil,
	}
}

func (n *Node) Append(ctx context.Context, tx db.Txn, x ptr.Ptr) *Node {
	n = n.makeDirectRoom(ctx, tx)
	return &Node{
		depths: n.depths,
		direct: appendcopy(n.direct, x),
	}
}

var listTypePtr = ptr.String("omen:biglist:[]Ptr")
var treeTypePtr = ptr.String("omen:biglist:[]Ptr/tree")

type nodetype struct {
	addr ptr.Ptr
	tree bool
}

func (nt *nodetype) Addr() ptr.Ptr {
	return nt.addr
}

// this is enough to get us into the hydration cache... we only know
// how to hydrate one type, which is ourselves (later, we might know
// how to hydrate any literal of the form "omen:biglist:*"
func (nt *nodetype) DataTypeHydrate(ctx context.Context, p ptr.Ptr, _ *db.Atom, _ db.Loader) (db.DataType, bool, error) {
	if nt.addr == p {
		return nt, true, nil
	}
	return nil, false, nil
}

var ListType = &nodetype{listTypePtr, false}
var TreeType = &nodetype{treeTypePtr, true}

func init() {
	db.RegisterTypeHydrater(ListType)
	db.RegisterTypeHydrater(TreeType)
}

func (nt *nodetype) Decode(_ context.Context, data []byte) (interface{}, []ptr.Ptr) {
	if nt.tree {
		x := deserzTree(data)
		return x.decode(), x.scan()
	} else {
		x := deserzList(data)
		return x.decode(), x.scan()
	}
}

func (nt *nodetype) Scan(data []byte) ([]ptr.Ptr, error) {
	if nt.tree {
		return deserzTree(data).scan(), nil
	} else {
		return deserzList(data).scan(), nil
	}
	panic("todo")
}

func (n *Node) decode() interface{} {
	indir := make([]interface{}, len(n.depths))

	for i, depth := range n.depths {
		llist := make([]interface{}, len(depth))
		indir[i] = llist
		for j, link := range depth {
			llist[j] = map[string]interface{}{
				"sum": link.sum,
				"ptr": link.ptr,
			}
		}
	}

	return map[string]interface{}{
		"format":   "list",
		"skip":     n.skip,
		"indirect": indir,
		"direct":   n.direct,
	}
}

func (in *Indirect) decode() interface{} {
	chlist := make([]interface{}, len(in.children))
	for i, link := range in.children {
		chlist[i] = map[string]interface{}{
			"sum": link.sum,
			"ptr": link.ptr,
		}
	}
	return map[string]interface{}{
		"format":   "indirect",
		"depth":    int(in.depth),
		"children": chlist,
	}
}

func (n *Node) scan() []ptr.Ptr {
	lst := make([]ptr.Ptr, 0, len(n.depths)+len(n.direct))
	for _, p := range n.direct {
		if p.IsLoadable() {
			lst = append(lst, p)
		}
	}
	for _, depth := range n.depths {
		for _, lnk := range depth {
			if !lnk.ptr.IsLoadable() {
				panic("oops")
			}
			lst = append(lst, lnk.ptr)
		}
	}
	return lst
}

func (in *Indirect) scan() []ptr.Ptr {
	lst := make([]ptr.Ptr, 0, len(in.children))
	for _, l := range in.children {
		if l.ptr.IsLoadable() {
			lst = append(lst, l.ptr)
		}
	}
	return lst
}

/*
	tmp := new(Node)
	var err error

	if nt.indir {
		err = tmp.deserzIndirect(data)
	} else {
		err = tmp.deserzDirect(data)
	}
	if err != nil {
		return nil, err
	}

	items := make([]model.Ptr, len(tmp.multi)+len(tmp.single)+len(tmp.direct))

	i := 0
	copy(items[i:], tmp.multi)
	i += len(tmp.multi)
	copy(items[i:], tmp.single)
	i += len(tmp.single)
	copy(items[i:], tmp.direct)

	return items, nil
*/
/*}


func (nt *nodetype) Ensure(tx db.Txn) {
	a, err := tx.Store(nil, []byte(nt.name))
	if err != nil {
		panic(err)
	}
	if a.Addr != nt.addr {
		panic("address mismatch")
	}
}

func (nt *nodetype) Check(ptr.Ptr) error {
	panic("todo")
}
*/

// Size returns the amount of storage taken up by the list, which
// is the same as Len except that a skipped prefix does not count against
// the size
func (n *Node) Size() uint64 {
	count := uint64(len(n.direct))
	for _, d := range n.depths {
		for _, l := range d {
			count += l.sum
		}
	}
	return count
}

// Len returns the virtual length of the list, which includes any skipped
// prefix.  In other words, it is the count of the number of things that
// have been appended.
func (n *Node) Len() uint64 {
	return n.Size() + n.skip
}

func linkpopn(ctx context.Context, tx db.Txn, l link, count uint64) link {
	if count == 0 {
		// shouldn't really get here, but a good base case to handle
		return l
	}

	in := loadTree(ctx, tx, l.ptr)

	if in.depth == 1 {
		// special fast case because we know each of the children
		// has sum=1
		strip := count
		if strip >= uint64(len(in.children)) {
			strip = uint64(len(in.children))
		}
		in = &Indirect{
			depth:    1,
			children: in.children[strip:],
		}
		return link{
			sum: uint64(len(in.children)),
			ptr: in.mustStore(ctx, tx),
		}
	}

	// slightly trickier because (1) we have to walk through
	// checking the sum's, and (2) we might have to recurse
	var children []link
	var total uint64
	remain := count

	for _, sub := range in.children {
		// same 3 (aka 2) cases as in popn...
		if remain >= sub.sum {
			// the entire child is being skipped
			remain -= sub.sum
		} else {
			// note that possibly remain==0 here
			sub = linkpopn(ctx, tx, sub, remain)
			total += sub.sum
			children = append(children, sub)
			// we are done
			remain = 0
		}
	}

	in = &Indirect{
		depth:    in.depth,
		children: children,
	}
	return link{
		sum: total,
		ptr: in.mustStore(ctx, tx),
	}
}

func (n *Node) popn(ctx context.Context, tx db.Txn, count uint64) *Node {
	if count > n.Size() {
		panic("trying to pop more than list size")
	}

	remain := count

	strippedDepths := make([][]link, len(n.depths))
	maxDepth := 0

	for i := range n.depths {
		depth := len(n.depths) - i // depth=1 => single indirect
		var linksAtDepth []link
		for _, atdepth := range n.depths[depth-1] {
			// there are only three possibilities -- either
			//
			// (1) link.sum > remain, in which case the
			//     link has more to contribute than we are
			//     popping, in which case we will be left
			//     with a (smaller) link and we will be
			//     done popping, or
			//
			// (2) remain > link.sum, in which case we want
			//     to pop more than the link can provide,
			//     in which case the entire link will be consumed
			//     and therefore removed from the node, or
			//
			// (3) remain = link.sum, which is a combination:
			//     (a) like (1) we will be done popping, but
			//     (b) like (2) the entire link will be consumed
			if remain >= atdepth.sum {
				// the entire link is being consumed
				remain -= atdepth.sum
				// so don't add this to the links at depth
				continue
			}
			// strip a prefix off the link
			if remain > 0 {
				atdepth = linkpopn(ctx, tx, atdepth, remain)
			}
			linksAtDepth = append(linksAtDepth, atdepth)
			remain = 0 // nothing more to do
		}
		if len(linksAtDepth) > 0 {
			if depth > maxDepth {
				maxDepth = depth
			}
		}
		strippedDepths[depth-1] = linksAtDepth
	}

	if remain > uint64(len(n.direct)) {
		panic("somehow have more left than can pop from direct")
	}

	log.Debugf(ctx, "skip was %d, now it will be +%d (len(direct)=%d remain %d)",
		n.skip, count, len(n.direct), remain)
	return &Node{
		skip:   n.skip + count,
		depths: strippedDepths[:maxDepth],
		direct: n.direct[remain:],
	}
}

// Skip return a list with count items removed from the front of it
func (n *Node) Skip(ctx context.Context, tx db.Txn, count uint64) *Node {
	if count == 0 {
		return n
	}
	return n.popn(ctx, tx, count)
}

// Start returns the offset of the first non-skipped item
func (n *Node) Start() uint64 {
	return n.skip
}

// serialize a node into an atom (i.e., a type and a payload)
func (n *Node) serz() (db.DataType, []byte) {
	buf := &bytes.Buffer{}

	// serialize the indirect pointers
	buf.WriteByte(uint8(len(n.depths)))
	for k := range n.depths {
		var tmp [255 * (ptr.PtrSize + 10)]byte

		// serialize them in deepest-first order, because we want
		// to preserve left-to-right semantics (i.e., the leftmost
		// thing in the serial form is the earliest object in the list)
		depth := len(n.depths) - 1 - k
		// here, depth 0 is the last one to be processed and represents
		// single-indirect tree nodes
		atdepth := n.depths[depth]
		j := 0

		tmp[j] = uint8(len(atdepth))
		j++

		for _, link := range atdepth {
			j += binary.PutUvarint(tmp[j:], link.sum)
			copy(tmp[j:], link.ptr.Bits[:])
			j += ptr.PtrSize
		}
		buf.Write(tmp[:j])
	}

	// serialize the direct pointers
	buf.WriteByte(uint8(len(n.direct)))
	for _, p := range n.direct {
		buf.Write(p.Bits[:])
	}
	//fmt.Printf("-----\n%x\n----\n", buf.Bytes())
	return ListType, buf.Bytes()
}

func deserzList(payload []byte) *Node {
	i := 0
	scanptr := func() (p ptr.Ptr) {
		copy(p.Bits[:], payload[i:i+ptr.PtrSize])
		i += ptr.PtrSize
		return
	}

	n := new(Node)

	// read the tree pointers
	count := payload[i]
	i++
	n.depths = make([][]link, count)

	for k := range n.depths {
		// they were serialized in deepest-first order, because we want
		// to preserve left-to-right semantics
		depth := len(n.depths) - 1 - k

		// here, depth 0 is the last one to be processed and represents
		// single-indirect tree nodes
		num := payload[i]
		i++

		atdepth := make([]link, num)
		n.depths[depth] = atdepth
		for l := range atdepth {
			sum, w := binary.Uvarint(payload[i:])
			i += w
			atdepth[l].sum = sum
			atdepth[l].ptr = scanptr()
		}
	}

	// read the direct pointers
	count = payload[i]
	i++

	n.direct = make([]ptr.Ptr, count)
	for i := range n.direct {
		n.direct[i] = scanptr()
	}
	return n
}

func (in *Indirect) serz() (db.DataType, []byte) {
	if in.depth < 1 {
		panic("bad depth")
	}
	buf := &bytes.Buffer{}
	buf.WriteByte(in.depth)

	var tmp [16]byte

	for _, l := range in.children {
		if in.depth > 1 {
			w := binary.PutUvarint(tmp[:], l.sum)
			buf.Write(tmp[:w])
		} else {
			if l.sum != 1 {
				panic("wtf? leaf link sum!=1")
			}
		}
		buf.Write(l.ptr.Bits[:])
	}
	return TreeType, buf.Bytes()
}

func loadTree(ctx context.Context, tx Loader, p ptr.Ptr) *Indirect {
	atom, err := tx.Load(ctx, p)
	if err != nil {
		panic(err)
	}
	if atom.Type != TreeType.addr {
		panic("invalid tree type: " + atom.Type.String())
	}
	return deserzTree(atom.Payload)
}

func deserzTree(payload []byte) *Indirect {
	i := 0

	depth := payload[i]
	if depth < 1 {
		panic("bad depth")
	}
	i++

	approxWidth := (len(payload) - 1) / ptr.PtrSize
	children := make([]link, 0, approxWidth)

	for i < len(payload) {
		var l link

		if depth > 1 {
			sum, w := binary.Uvarint(payload[i:])
			i += w
			l.sum = sum
		} else {
			l.sum = 1
		}
		copy(l.ptr.Bits[:], payload[i:i+ptr.PtrSize])
		i += ptr.PtrSize
		children = append(children, l)
	}
	return &Indirect{
		depth:    depth,
		children: children,
	}
}
