package biglist

import (
	"context"
	"fmt"
	"math/rand"
	"testing"

	//"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/lib/omentest"
	"bitbucket.org/dkolbly/omen3/ptr"
)

func TestShortSlicing(t *testing.T) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)

	seq := []ptr.Ptr{x, y, z}

	lst := new(Node)
	for _, p := range seq {
		lst = lst.Append(ctx, tx, p)
	}

	check := func(a, b uint64) {
		var count uint64
		for item := range lst.Slice(ctx, tx, a, b) {
			if item != seq[a+count] {
				t.Errorf("At [%d] expected %s but got %s",
					count,
					seq[count],
					item)
			}
			count++
		}
		if count != b-a {
			t.Errorf("Expected %d items to be returned, got %d",
				len(seq), count)
		}
	}

	n := lst.Len()
	check(0, n)
	check(0, 0)
	check(n, n)
	check(n-1, n)
	check(0, n-1)
	check(0, 1)
	check(1, 2)
	check(2, 3)
}

func testslice(count int) []ptr.Ptr {
	seq := make([]ptr.Ptr, count)

	for i := range seq {
		buf := fmt.Sprintf("#%d", i)
		seq[i] = ptr.ContentAddress(ptr.Untyped, []byte(buf))
	}

	return seq
}

func TestLongSlicing(t *testing.T) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)

	seq := testslice(7531)

	lst := new(Node)
	for i, p := range seq {
		lst = lst.Append(ctx, tx, p)
		// change our mind about the structure on the fly

		switch i {
		case 231:
			MaxDirect = 23
			Fanout = 11
		case 3237:
			MaxDirect = 25
			Fanout = 13
		case 7500:
			MaxDirect = 14
			Fanout = 2
		}
	}

	// round trip through serdes
	lst = MustLoad(ctx, tx, lst.MustStore(ctx, tx))

	// check expected properties
	if lst.Len() != uint64(len(seq)) {
		t.Fatalf("Did not produce a list of the right size")
	}

	check := func(a, b uint64) {
		var count uint64
		for item := range lst.Slice(ctx, tx, a, b) {
			if item != seq[a+count] {
				t.Errorf("At [%d] expected %s but got %s",
					count,
					seq[count],
					item)
			}
			count++
		}
		if count != b-a {
			t.Errorf("Expected %d items to be returned, got %d",
				len(seq), count)
		}
	}

	n := lst.Len()
	check(0, n)
	check(0, 100)
	check(2*n/4, 3*n/4)
	check(n-1, n)
	check(n-2, n)
	check(n-50, n)
	check(n-1000, n-1)
	check(0, n-1)
	check(0, 1)
	check(1, 2)
	check(2, 3)

	for i, p := range seq {
		at, ok := lst.Get(ctx, tx, uint64(i))
		if !ok {
			t.Errorf("At index [%d] expected ok, but got not ok", i)
		} else if at != p {
			t.Errorf("At index [%d] expected %s, got %s", i, p, at)
		}
	}
	_, ok := lst.Get(ctx, tx, lst.Len()+1)
	if ok {
		t.Errorf("At index [%d] expected to not exist, but got ok", lst.Len()+1)
	}

	max := len(seq)

	// about half of these get skipped because a+b > max
	r := rand.New(rand.NewSource(66666))
	for i := 0; i < 100; i++ {
		a := int(r.Int31n(int32(max)))
		b := a + int(r.Int31n(int32(max)))
		if b <= max {
			check(uint64(a), uint64(b))
		}
	}

	printout(ctx, tx, lst)
}
