package biglist

import (
	"context"
	"fmt"
	"testing"

	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/lib/omentest"
	"bitbucket.org/dkolbly/omen3/ptr"
)

func mustPtr(s string) ptr.Ptr {
	p, ok := ptr.DecodeStringPtr(s)
	if !ok {
		panic("failed")
	}
	return p
}

var x = mustPtr("__________________________________________u")
var y = mustPtr("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAu")
var z = mustPtr("m00000000000000000000000000000000000000000u")
var x2 = mustPtr("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxu")
var y2 = mustPtr("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyu")
var z2 = mustPtr("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzu")

func TestShortList(t *testing.T) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)
	lst := new(Node)
	lst = lst.Append(ctx, tx, x)
	lst = lst.Append(ctx, tx, y)
	lst = lst.Append(ctx, tx, z)

	// round trip through serdes
	lst = MustLoad(ctx, tx, lst.MustStore(ctx, tx))

	// check properties
	if lst.Len() != 3 {
		t.Errorf("expected list to have length 3, got %d", lst.Len())
	}

	v, ok := lst.Get(ctx, tx, 0)
	if !ok {
		t.Errorf("expected index 0 to be present, wasn't")
	}
	if v != x {
		t.Errorf("expected index 0 to be x, but was %s", v)
	}

	v, ok = lst.Get(ctx, tx, 1)
	if !ok {
		t.Errorf("expected index 1 to be present, wasn't")
	}
	if v != y {
		t.Errorf("expected index 1 to be y, but was %s", v)
	}

	v, ok = lst.Get(ctx, tx, 2)
	if !ok {
		t.Errorf("expected index 2 to be present, wasn't")
	}
	if v != z {
		t.Errorf("expected index 2 to be z, but was %s", v)
	}
}

func TestSingleIndirectList(t *testing.T) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)
	lst := new(Node)
	for i := 0; i < MaxDirect+1; i++ {
		lst = lst.Append(ctx, tx, x)
	}

	// round trip through serdes
	lst = MustLoad(ctx, tx, lst.MustStore(ctx, tx))

	// check properties
	if lst.Len() != uint64(MaxDirect+1) {
		t.Errorf("expected list to have length %d, got %d",
			MaxDirect+1, lst.Len())
	}
	if len(lst.direct) != 1 {
		t.Errorf("expected 1 direct, got %d", len(lst.direct))
	}
	if len(lst.depths) != 1 {
		t.Errorf("expected 1 depth, got %d", len(lst.depths))
	}
}

func TestMaxSingleIndirect(t *testing.T) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)
	lst := new(Node)
	count := MaxDirect + Fanout*MaxDirect + 1
	for i := 0; i < count; i++ {
		lst = lst.Append(ctx, tx, x)
	}
	if lst.Len() != uint64(count) {
		t.Errorf("expected list to have length %d, got %d",
			count,
			lst.Len())
	}
	if len(lst.direct) != 1 {
		t.Errorf("expected 1 direct, got %d", len(lst.direct))
	}
	if len(lst.depths) != 2 {
		t.Errorf("expected 2 depth2, got %d", len(lst.depths))
	}
}

func TestPopDirect(t *testing.T) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)
	lst := new(Node)
	for i := 0; i < 18; i++ {
		lst = lst.Append(ctx, tx, x)
	}
	lst = lst.Skip(ctx, tx, 3)

	if lst.Len() != 18 {
		t.Errorf("expected list to have length %d, got %d",
			18, lst.Len())
	}
	if lst.Size() != 15 {
		t.Errorf("expected list to have size %d, got %d",
			15, lst.Size())
	}
	if lst.Start() != 3 {
		t.Errorf("expected list start to be %d, got %d",
			3, lst.Start())
	}
}

func TestPopIndirect(t *testing.T) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)
	lst := new(Node)
	seq := testslice(7657)
	for _, p := range seq {
		lst = lst.Append(ctx, tx, p)
	}

	for _, skip := range []int{17, 81, 2902, 2000, 6, 1000, 51, 1555, 40, 5} {

		lst = lst.Skip(ctx, tx, uint64(skip))
		seq = seq[skip:]

		if lst.Len() != 7657 {
			t.Errorf("expected list to have length %d, got %d",
				7657, lst.Len())
		}
		if lst.Size() != uint64(len(seq)) {
			t.Errorf("expected list to have size %d, got %d",
				len(seq), lst.Size())
		}
		i := 0
		for item := range lst.Slice(ctx, tx, lst.Start(), lst.Len()) {
			if item != seq[i] {
				t.Errorf("At [%d+%d] expected %s but got %s",
					i,
					lst.Start(),
					seq[i],
					item)
			}
			i++
		}
		fmt.Printf("================= AFTER SKIPPING %d\n", skip)
		printout(ctx, tx, lst)
	}
}

/*
func TestMultiIndirect(t *testing.T) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)
	lst := new(Node)
	count := 1 + MaxDirect + MaxSingleIndirect*MaxDirect
	for i:=0; i<count; i++ {
		lst = lst.Append(tx, x)
	}
	if lst.Len() != uint64(count) {
		t.Errorf("expected list to have length %d, got %d",
			count,
			lst.Len())
	}
	if len(lst.direct) != 1 {
		t.Errorf("expected 1 directs, got %d", len(lst.direct))
	}
}

func TestMultiOverflow(t *testing.T) {
	o := omentest.New()
	ctx := context.Background()
	tx := o.Begin(ctx)
	lst := new(Node)
	//count := 1234567
	count := 13237
	for i:=0; i<count; i++ {
		lst = lst.Append(tx, x)
	}
	if lst.Len() != uint64(count) {
		t.Errorf("expected list to have length %d, got %d",
			count,
			lst.Len())
	}
	if len(lst.direct) != 7 {
		t.Errorf("expected 7 directs, got %d", len(lst.direct))
	}
	if len(lst.single) != 8 {
		t.Errorf("expected 8 single-indirects, got %d", len(lst.single))
	}
	if len(lst.multi) != 7 {
		t.Errorf("expected 7 multi-indirects, got %d", len(lst.multi))
	}

	// walk the tree
	var walk func(a *omenapi.Atom, pre string, depth int)


	walk = func(a *omenapi.Atom, pre string, depth int){
		n := deserz(a)
		fmt.Printf("%3d : %s %s : len=%d : %d [%d] + %d [%d] + [%d]\n",
			depth,
			indent(depth),
			pre,
			n.Len(),
			n.multisum,
			len(n.multi),
			n.singlesum,
			len(n.single),
			len(n.direct),
		)
		for i, x := range n.multi {
			walk(tx.Load(x), fmt.Sprintf("multi(%d)", i), depth+1)
		}
		for i, x := range n.single {
			walk(tx.Load(x), fmt.Sprintf("single(%d)", i), depth+1)
		}
	}

	a, _ := lst.storeAtom(tx)
	walk(a, "root", 0)
}

func indent(k int) string {
	return "                                                "[:k*2]
}

func TestRightSpan(t *testing.T) {
	n := findRightSpan([]uint8{})
	if n != 0 {
		t.Fatalf("expected findRightSpan({}) == 0, got %d", n)
	}
	n = findRightSpan([]uint8{1})
	if n != 1 {
		t.Fatalf("expected findRightSpan({1}) == 1, got %d", n)
	}
	n = findRightSpan([]uint8{2, 2})
	if n != 2 {
		t.Fatalf("expected findRightSpan({2, 2}) == 2, got %d", n)
	}
	n = findRightSpan([]uint8{3, 3, 3, 1})
	if n != 1 {
		t.Fatalf("expected findRightSpan({3,3,3,1}) == 1, got %d", n)
	}
	n = findRightSpan([]uint8{3, 2, 2, 1})
	if n != 1 {
		t.Fatalf("expected findRightSpan({3,2,2,1}) == 1, got %d", n)
	}
}
*/

func printout(ctx context.Context, tx db.Txn, lst *Node) {

	var printseq func(nest int, depth uint8, links []link)
	var walk func(nest int, in *Indirect)

	printseq = func(nest int, depth uint8, links []link) {
		fmt.Printf("%3d : %s : depth=%d : [ ",
			nest,
			indent(nest),
			depth,
		)
		for _, l := range links {
			fmt.Printf("%d ", l.sum)
		}
		fmt.Printf("]\n")
		if depth > 1 {
			for _, l := range links {
				walk(nest+1, loadTree(ctx, tx, l.ptr))
			}
		}
	}

	// walk the tree

	walk = func(nest int, in *Indirect) {
		printseq(nest, in.depth, in.children)
	}

	fmt.Printf("*** total len = %d\n", lst.Len())
	if lst.skip > 0 {
		fmt.Printf("*** skip = %d  ; size = %d\n", lst.skip, lst.Size())
	}
	for i := range lst.depths {
		d := len(lst.depths) - i + 1
		atdepth := lst.depths[d-2]
		fmt.Printf("[%d] -- biglist depth %d\n", i, d)
		printseq(1, uint8(d), atdepth)
	}
	fmt.Printf("plus %d direct\n", len(lst.direct))
}

func indent(k int) string {
	return "                                                "[:k*2]
}
