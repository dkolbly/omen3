package txmgr

import (
	"context"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/lib/sequence"
	"bitbucket.org/dkolbly/omen3/ptr"
)

var log = logging.New("txmgr")

type Beginner struct {
	states  db.StateSupplier
	atoms   db.AtomSupplier
	lock    sync.Mutex
	flushed map[db.DataType]struct{}
	seq     *sequence.Sequence
}

// New provides a simple transaction interface on top of basic storage
// and state infrastructure
func New(s db.StateSupplier, a db.AtomSupplier, seqname, seqkey string) *Beginner {
	b := &Beginner{
		states:  s,
		atoms:   a,
		flushed: make(map[db.DataType]struct{}),
	}
	if seqname != "" {
		b.seq = sequence.New(b, seqname, seqkey).Min(1000)
	}
	return b
}

type simpleTxn struct {
	backing       *Beginner
	loaded        int
	stored        int
	alloced       int
	prereqs       map[string]*db.State
	edits         map[string]int
	record        db.TxnRecord
	commitResults []*db.State
	start         time.Time
}

func (s *simpleTxn) StateSupplierReveal() db.StateSupplier {
	return s.backing.states
}

func (s *simpleTxn) Final() *db.Disposition {
	if s.record.ID == 0 {
		return nil
	}
	states := make(map[string]*db.State, len(s.commitResults))
	for _, state := range s.commitResults {
		states[state.Name] = state
	}
	return &db.Disposition{
		Record:       s.record,
		AtomsRead:    s.loaded,
		AtomsWritten: s.stored,
		IDsAllocated: s.alloced,
		Committed:    states,
	}
}

func (b *Beginner) Begin(ctx context.Context, audit uint64) db.Txn {
	return &simpleTxn{
		backing: b,
		record: db.TxnRecord{
			Audit: audit,
		},
		prereqs: make(map[string]*db.State),
		edits:   make(map[string]int),
		start:   time.Now(),
	}
}

func (t *simpleTxn) Commit(ctx context.Context) error {
	txid, tm, states, err := t.backing.Apply(ctx, &t.record)
	if err != nil {
		return err
	}
	t.record.ID = txid
	t.record.Timestamp = tm
	t.commitResults = states
	return nil
}

func (t *simpleTxn) Set(ctx context.Context, name string, value ptr.Ptr) {
	if i, ok := t.edits[name]; ok {
		t.record.Edits[i].Value = value
	} else {
		t.edits[name] = len(t.edits)
		t.record.Edits = append(t.record.Edits, db.Edit{
			Name:  name,
			Value: value,
		})
	}
}

func (t *simpleTxn) applyEdit(name string, old *db.State) *db.State {
	if i, ok := t.edits[name]; ok {
		var seq uint64
		if old != nil {
			seq = old.Sequence
		}
		return &db.State{
			Name:     name,
			Value:    t.record.Edits[i].Value,
			Modified: t.start,
			Sequence: seq + 1,
			Audit:    t.record.Audit,
		}
	}
	return old
}

func (t *simpleTxn) Obs(ctx context.Context, name string) (*db.State, error) {
	if s, ok := t.prereqs[name]; ok {
		// TODO what about the READ-MISSING DELETE READ case?
		s = t.applyEdit(name, s)
		if s == nil {
			return nil, ptr.ErrNotPresent(ptr.Obs(name))
		}
		return s, nil
	}
	log.Debugf(ctx, "backing is  %T", t.backing)
	s, err := t.backing.Obs(ctx, name)
	if err != nil {
		if ptr.IsNotPresent(err) {
			// remember this error for later
			t.prereqs[name] = nil
		}
		return nil, err
	}
	t.prereqs[name] = s
	// note that it could have been modified already
	// TODO! think about the prereq semantics of a
	//    WRITE
	//    READ
	// sequence
	return t.applyEdit(name, s), nil
}

func (t *simpleTxn) Load(ctx context.Context, p ptr.Ptr) (*db.Atom, error) {
	t.loaded++
	return t.backing.Load(ctx, p)
}

func (t *simpleTxn) Store(ctx context.Context, dt db.DataType, payload []byte) (*db.Atom, error) {
	t.stored++
	return t.backing.Store(ctx, dt, payload)
}

func (t *simpleTxn) Alloc(ctx context.Context) uint64 {
	t.alloced++
	return t.backing.seq.Next(ctx)
}

func (s *Beginner) Load(ctx context.Context, addr ptr.Ptr) (*db.Atom, error) {
	return s.atoms.Load(ctx, addr)
}

func (s *Beginner) Obs(ctx context.Context, name string) (*db.State, error) {
	s0 := &db.State{Name: name}
	ret, err := s.states.WaitForChange(ctx, []*db.State{s0})
	if err != nil {
		return nil, err
	}
	return ret[0], nil
}

func (s *Beginner) Store(ctx context.Context, dt db.DataType, buf []byte) (*db.Atom, error) {
	s.flushTypes(ctx, dt)
	return s.atoms.Store(ctx, dt, buf)
}

func (s *Beginner) Apply(ctx context.Context, txr *db.TxnRecord) (uint64, time.Time, []*db.State, error) {
	return s.states.Apply(ctx, txr)
}

func (s *Beginner) Alloc(ctx context.Context) uint64 {
	panic("todo")
}

func (s *Beginner) flushTypes(ctx context.Context, dt db.DataType) {
	s.lock.Lock()
	defer s.lock.Unlock()

	if _, ok := s.flushed[dt]; ok {
		return
	}
	s.flushed[dt] = struct{}{}

	atomser, ok := dt.(db.Atomser)
	if !ok {
		return
	}

	for _, a := range atomser.Atoms() {
		var t db.DataType
		if !a.Type.IsNull() {
			t = db.ScanlessType(a.Type)
		}
		a2, err := s.atoms.Store(ctx, t, a.Payload)
		if err == nil {
			if a2.Addr != a.Addr {
				panic("addr mismatch")
			}
		}
	}
}

func (t *simpleTxn) AlreadyObserved(name string) *db.State {
	return t.prereqs[name]
}

func (t *simpleTxn) FlushObservations(lst []*db.State) {
	for _, s := range lst {
		if pre, ok := t.prereqs[s.Name]; ok {
			// this indicates an implementation error in the subtxn; if they flush
			// something to us that we have previously observed (perhaps because of some
			// change in the outer transaction), then it had better be the same value
			if pre.Value != s.Value {
				panic("cannot flush a changed observation")
			}
		} else {
			t.prereqs[s.Name] = s
		}
	}
}
