package pki

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"io/ioutil"
	"math/big"
	"time"
)

// CreateRootCertificate creates and returns a self-signed certificate
// and its private key
func CreateRootCertificate() *Entity {
	// create the CA's keypair
	priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)

	if err != nil {
		panic(err)
	}

	buf, err := x509.MarshalECPrivateKey(priv)
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("ca-private-key.der", buf, 0600)
	if err != nil {
		panic(err)
	}

	name := pkix.Name{
		Country:            []string{"US"},
		Province:           []string{"Texas"},
		Locality:           []string{"Austin"},
		Organization:       []string{"Hexplore Omen Group"},
		OrganizationalUnit: []string{"Certificate Authority"},
		CommonName:         "Hexplore Omen3 Root CA",
	}

	ca := &x509.Certificate{
		//AuthorityKeyId
		BasicConstraintsValid: true,
		DNSNames: []string{
			"omen.hexplore.us",
		},
		// ExcludedDNSDomains
		// ExtKeyUsage:
		IsCA:     true,
		KeyUsage: x509.KeyUsageCertSign | x509.KeyUsageCRLSign, /* | x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature*/
		//ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		MaxPathLen: 3,
		NotAfter:   time.Now().Add(time.Hour * 24 * 365 * 2),
		NotBefore:  time.Now(),
		// PermittedDNSDomains
		// PermittedDNSDomainsCritical
		SerialNumber:       big.NewInt(1),
		SignatureAlgorithm: x509.ECDSAWithSHA256,
		Subject:            name,
		//SubjectKeyId
		//UnknownExtKeyUsage
	}

	der, err := x509.CreateCertificate(
		rand.Reader,
		ca,
		ca,
		priv.Public(),
		priv)

	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("ca-certificate.der", der, 0600)
	if err != nil {
		panic(err)
	}

	cacert, err := x509.ParseCertificate(der)
	if err != nil {
		panic(err)
	}

	ctx := context.TODO()
	log.Debugf(ctx, "--> checking CA")
	e := &Entity{
		certs: []*x509.Certificate{cacert},
		priv:  priv,
	}
	verify(ctx, e)
	return e
}

/*
	// stamp out a minibroker certificate
	mb := paveMinibroker(priv, cacert)

	log.Debugf(ctx, "--> checking Minibroker")
	verify(ctx, mb, nil, cacert)
}
*/

/*
// Signed is a signed certificate
type Signed struct {
	subject pkix.Name
	der []byte
	cert *x509.Certificate
}
*/
