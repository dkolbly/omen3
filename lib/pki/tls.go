package pki

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"time"
)

func (e *Entity) TLSClientConfig() *tls.Config {
	ctx := context.TODO()

	certPool := x509.NewCertPool()
	ca := e.certs[len(e.certs)-1]
	certPool.AddCert(ca)
	log.Debugf(ctx, "cert pool: %s", ca.Subject)
	client := tls.Certificate{
		PrivateKey: e.priv,
	}
	for _, c := range e.certs[:len(e.certs)-1] {
		client.Certificate = append(client.Certificate, c.Raw)
		log.Debugf(ctx, "supply chain: %s", c.Subject)
		if c.NotAfter.Before(time.Now()) {
			log.Errorf(ctx, "expires %s",
				c.NotAfter.Format("2006-01-02 15:04:05"))
		} else {
			log.Debugf(ctx, "expires %s",
				c.NotAfter.Format("2006-01-02 15:04:05"))
		}
	}

	return &tls.Config{
		Certificates: []tls.Certificate{client},
		RootCAs:      certPool,
	}
}

func (e *Entity) TLSServerConfig() *tls.Config {
	ctx := context.TODO()

	certPool := x509.NewCertPool()
	ca := e.certs[len(e.certs)-1]
	certPool.AddCert(ca)
	log.Debugf(ctx, "cert pool: %s", ca.Subject)
	client := tls.Certificate{
		PrivateKey: e.priv,
	}
	for _, c := range e.certs[:len(e.certs)-1] {
		client.Certificate = append(client.Certificate, c.Raw)
		log.Debugf(ctx, "supply chain: %s", c.Subject)
		if c.NotAfter.Before(time.Now()) {
			log.Errorf(ctx, "expires %s",
				c.NotAfter.Format("2006-01-02 15:04:05"))
		} else {
			log.Debugf(ctx, "expires %s",
				c.NotAfter.Format("2006-01-02 15:04:05"))
		}
	}

	return &tls.Config{
		ClientAuth:   tls.RequireAndVerifyClientCert,
		Certificates: []tls.Certificate{client},
		ClientCAs:    certPool,
	}
}
