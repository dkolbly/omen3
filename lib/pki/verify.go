package pki

import (
	"context"
	"crypto/x509"
)

func verify(ctx context.Context, e *Entity) {
	ipool := x509.NewCertPool()
	capool := x509.NewCertPool()

	n := len(e.certs)
	for _, c := range e.certs[1 : n-1] {
		ipool.AddCert(c)
	}
	capool.AddCert(e.certs[n-1])

	vopts := x509.VerifyOptions{
		Intermediates: ipool,
		Roots:         capool,
	}

	chains, err := e.certs[0].Verify(vopts)
	if err != nil {
		log.Errorf(ctx, "Failed verification: %s", err)
		panic(err)
	}
	log.Debugf(ctx, "Verification produced %d chains", len(chains))

}
