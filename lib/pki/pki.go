package pki

import (
	"bytes"
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"bitbucket.org/dkolbly/logging"
)

const trace = false

var log = logging.New("pki")
var errRequestFailed = errors.New("request failed")
var errNoCredentials = errors.New("could not get credentials to access dispensary")
var errNotAuthorized = errors.New("not authorized to access dispensary")

var tenantName = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 4774, 95, 3}
var agentIdentifier = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 4774, 95, 4}
var commonName = asn1.ObjectIdentifier{2, 5, 4, 3}

type Subject struct {
	TenantName      string
	AgentIdentifier string
	CommonName      string
}

func ApprovedExtraNames(src pkix.Name) (a []pkix.AttributeTypeAndValue) {
	for _, n := range src.Names {
		if n.Type.Equal(tenantName) || n.Type.Equal(agentIdentifier) {
			a = append(a, n)
		}
	}
	return
}

// GetSubject extracts a Subject from a given name, and returns
// and true if all fields were present
func GetSubject(name pkix.Name) (s Subject, ok bool) {
	var flags uint

	for _, n := range name.Names {
		if n.Type.Equal(tenantName) {
			if str, ok := n.Value.(string); ok {
				s.TenantName = str
				flags |= 1
			}
		} else if n.Type.Equal(agentIdentifier) {
			if str, ok := n.Value.(string); ok {
				s.AgentIdentifier = str
				flags |= 2
			}
		} else if n.Type.Equal(commonName) {
			if str, ok := n.Value.(string); ok {
				s.CommonName = str
				flags |= 4
			}
		}
	}
	ok = flags == 7
	return
}

// Parse checks a collection of verified certificate chains (as might
// be found in the VerifiedChains field of a tls.ConnectionState) to
// find the omen subject, which includes a TenantName and
// AgentIdentifier as well as the usual CommonName
func Parse(ctx context.Context, certs [][]*x509.Certificate) (Subject, bool) {
	for _, pcc := range certs {
		//var s Subject
		//var flags uint

		for _, pc := range pcc {
			s, ok := GetSubject(pc.Subject)
			if ok {
				return s, true
			}
			/*
				if trace {
					log.Debugf(ctx, "  [%d][%d] OU=%q CN=%q",
						i, j,
						pc.Subject.OrganizationalUnit,
						pc.Subject.CommonName,
					)
				}

				for k, n := range pc.Subject.Names {
					if trace {
						log.Debugf(ctx, "   [%d] %#v", k, n)
					}
					if n.Type.Equal(tenantName) {
						if str, ok := n.Value.(string); ok {
							s.TenantName = str
							flags |= 1
						}
					} else if n.Type.Equal(agentIdentifier) {
						if str, ok := n.Value.(string); ok {
							s.AgentIdentifier = str
							flags |= 2
						}
					} else if n.Type.Equal(commonName) {
						if str, ok := n.Value.(string); ok {
							s.CommonName = str
						}
					}
				}
				if flags == 3 {
					return s, true
				}
			*/
		}
	}
	return Subject{}, false
}

type entitySetup struct {
	ent          *Entity
	extraNames   []pkix.AttributeTypeAndValue
	csr          *x509.CertificateRequest
	server       string
	accessToken  string
	tenantOption bool
}

type Entity struct {
	certs []*x509.Certificate // starts with ours and ends with root authority
	priv  *ecdsa.PrivateKey
}

func (e *Entity) EncodeCA() []byte {
	return e.certs[len(e.certs)-1].Raw
}

func (e *Entity) EncodeChain() []byte {
	b := &bytes.Buffer{}
	putuint := func(x int) {
		var tmp [4]byte
		n := binary.PutUvarint(tmp[:], uint64(x))
		b.Write(tmp[:n])
	}

	n := len(e.certs) - 1 // the ca is in the chain, we don't want to include it
	putuint(1)            // version
	putuint(n)

	for _, c := range e.certs[:n] {
		putuint(len(c.Raw))
		b.Write(c.Raw)
	}

	return b.Bytes()
}

func (e *Entity) Subject(ctx context.Context) (Subject, bool) {
	return Parse(ctx, [][]*x509.Certificate{e.certs})
}

func (e *Entity) CertificateDER() []byte {
	return e.certs[0].Raw
}

func (e *Entity) Authority() []*x509.Certificate {
	return e.certs[1:]
}

func (e *Entity) PrivateKeyDER() []byte {
	buf, err := x509.MarshalECPrivateKey(e.priv)
	if err != nil {
		panic(err) // what would cause this?
	}
	return buf
}

type Request struct {
	Server          string
	Authorization   string
	AgentIdentifier string
	Tenant          string
}

func (r *Request) Do() error {
	panic("tood")
}

type EntityOption func(context.Context, *entitySetup) error

func Server(s string) EntityOption {
	return func(ctx context.Context, e *entitySetup) error {
		e.server = s
		return nil
	}
}

func Name(name pkix.Name) EntityOption {
	return func(ctx context.Context, e *entitySetup) error {
		e.csr.Subject = name
		return nil
	}
}

// Tenant configures the tenant for a new entity; normally, you cannot
// set this explicitly -- it is a property of the client credentials
// supplied by Creds
func Tenant(name string) EntityOption {
	return func(ctx context.Context, e *entitySetup) error {
		e.tenantOption = true
		e.extraNames = append(e.extraNames,
			pkix.AttributeTypeAndValue{
				Type:  tenantName,
				Value: name,
			},
		)
		return nil
	}
}

func Creds(id, secret string) EntityOption {
	return func(ctx context.Context, e *entitySetup) error {
		token, err := getAccessToken(ctx, id, secret)
		if err != nil {
			return err
		}
		//log.Debugf(ctx, "access token is %s", token)
		e.accessToken = token

		return nil
	}
}

func Agent(name string) EntityOption {
	return func(ctx context.Context, e *entitySetup) error {
		e.extraNames = append(e.extraNames,
			pkix.AttributeTypeAndValue{
				Type:  agentIdentifier,
				Value: name,
			},
		)
		return nil
	}
}

// NewEntity gets a certificate for a new endpoint entity, which in
// the context of the omen PKI could be a client or a server or both.
// We authenticate with the broker using an authorization token.
func NewEntity(ctx context.Context, opts ...EntityOption) (*Entity, error) {

	priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, err
	}

	e := &entitySetup{
		ent: &Entity{
			priv: priv,
		},
		csr: &x509.CertificateRequest{
			SignatureAlgorithm: x509.ECDSAWithSHA256,
		},
	}
	for _, opt := range opts {
		err := opt(ctx, e)
		if err != nil {
			return nil, err
		}
	}

	// finalize

	if e.server == "" {
		e.server = "https://auth.omen.hexplore.us/"
	}

	u, err := url.Parse(e.server)
	if err != nil {
		return nil, err
	}

	sign, _ := url.Parse("/sign")

	e.csr.Subject.ExtraNames = append(e.csr.Subject.ExtraNames, e.extraNames...)

	if !e.tenantOption && e.accessToken != "" {
		ten := accessTokenTenant(e.accessToken)
		if ten == "" {
			log.Warningf(ctx, "no tenant information in access token")
		} else {
			e.csr.Subject.ExtraNames = append(e.csr.Subject.ExtraNames,
				pkix.AttributeTypeAndValue{
					Type:  tenantName,
					Value: ten,
				},
			)
		}
	}

	//name pkix.Name, dns []string) (*Request, error) {
	random := rand.Reader

	/*csr := &x509.CertificateRequest{
		Subject: name,
	}*/

	der, err := x509.CreateCertificateRequest(random, e.csr, priv)
	if err != nil {
		return nil, err
	}

	// exchange it
	req, err := http.NewRequest(
		http.MethodPost,
		u.ResolveReference(sign).String(),
		bytes.NewReader(der))
	if err != nil {
		panic(err)
	}
	req.Header.Set("Content-Type", "application/x-omen-csr")
	if e.accessToken != "" {
		req.Header.Set("Authorization", "Bearer "+e.accessToken)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return nil, errRequestFailed
	}

	//fmt.Printf("answer: %s\n", resp.Status)

	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	var r Response
	json.Unmarshal(buf, &r)

	//ioutil.WriteFile("output.der", buf, 0600)

	// this list does not include the root
	for _, c := range r.Certificates {
		signed, err := x509.ParseCertificate(c)
		if err != nil {
			panic(err)
		}
		e.ent.certs = append(e.ent.certs, signed)
		//log.Debugf(ctx, "cert [%d] is DER: %x", j, signed.Raw)
	}

	// TODO verify that what we got back is trustworthy
	ca := loadCAFromEnv()

	// stick the CA on the end
	e.ent.certs = append(e.ent.certs, ca)

	verify(ctx, e.ent)
	return e.ent, nil
}

type Response struct {
	Certificates [][]byte `json:"certificates"`
}

type tokenRequest struct {
	GrantType    string `json:"grant_type"`
	ClientID     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	Audience     string `json:"audience"`
}

type tokenResponse struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int    `json:"expires_in"`
	Scope       string `json:"scope"`
}

func getAccessToken(ctx context.Context, id, secret string) (string, error) {
	tr := tokenRequest{
		GrantType:    "client_credentials",
		ClientID:     id,
		ClientSecret: secret,
		Audience:     omenAudience,
	}
	buf, _ := json.Marshal(&tr)
	//log.Debugf(ctx, "request: %s", buf)
	req, err := http.NewRequest(
		http.MethodPost,
		auth0Endpoint,
		bytes.NewReader(buf))
	req.Header.Set("Content-Type", "application/json")

	if err != nil {
		return "", err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == http.StatusUnauthorized {
			return "", errNotAuthorized
		}
		return "", errNoCredentials
	}

	buf, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	//log.Debugf(ctx, "answer %s\n%s", resp.Status, buf)

	var tresp tokenResponse
	err = json.Unmarshal(buf, &tresp)
	if err != nil {
		return "", err
	}
	return tresp.AccessToken, nil
}

func accessTokenTenant(token string) string {
	parts := strings.Split(token, ".")
	buf, err := base64.RawStdEncoding.DecodeString(parts[1])
	if err != nil {
		return ""
	}
	var c claims
	json.Unmarshal(buf, &c)
	return c.Tenant
}

type claims struct {
	Tenant string `json:"http://omen.hexplore.us/tenant"`
}

const auth0Endpoint = "https://kolbly.auth0.com/oauth/token"
const omenAudience = "omen:api:2"
