package pki

import (
	"bytes"
	"crypto/x509"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"os"
	"sync"
)

type ErrNoEntityInEnv string
type ErrEntityMissingPrivateKey string
type ErrEntityMissingCertificate string

var ErrMissingCertificateAuthorityCert = errors.New("No OMEN_CA_CERT in environment")

type ErrInvalidCertificateChain string

func (err ErrNoEntityInEnv) Error() string {
	return fmt.Sprintf("No x509 entity in environment %s_PRIVATE_KEY and %s_CERT",
		string(err),
		string(err))
}

func (err ErrEntityMissingPrivateKey) Error() string {
	return fmt.Sprintf("x509 entity missing %s_PRIVATE_KEY",
		string(err))
}

func (err ErrEntityMissingCertificate) Error() string {
	return fmt.Sprintf("x509 entity missing %s_CERT_CHAIN",
		string(err))
}

func (err ErrInvalidCertificateChain) Error() string {
	return fmt.Sprintf("value for %s_CERT_CHAIN is invalid",
		string(err))
}

// LoadEntityFromEnv reads a private key and certificate from the
// environment in base-64 encoded DER format.  The environment
// variables are <prefix>_PRIVATE_KEY and <prefix>_CERT
func LoadEntityFromEnv(prefix string) (*Entity, error) {
	ca := loadCAFromEnv()

	privder64 := os.Getenv(prefix + "_PRIVATE_KEY")
	chainder64 := os.Getenv(prefix + "_CERT_CHAIN")

	if privder64 == "" && chainder64 == "" {
		return nil, ErrNoEntityInEnv(prefix)
	}
	if privder64 == "" {
		return nil, ErrEntityMissingPrivateKey(prefix)
	}
	if chainder64 == "" {
		return nil, ErrEntityMissingCertificate(prefix)
	}

	privder, err := base64.StdEncoding.DecodeString(privder64)
	if err != nil {
		return nil, err
	}

	chainder, err := base64.StdEncoding.DecodeString(chainder64)
	if err != nil {
		return nil, err
	}

	b := bytes.NewBuffer(chainder)

	v, err := binary.ReadUvarint(b)
	if v != 1 {
		return nil, ErrInvalidCertificateChain(prefix)
	}

	n, err := binary.ReadUvarint(b)

	// there are n of them (does not include the ca)
	chain := make([]*x509.Certificate, n+1)

	for i := 0; i < int(n); i++ {
		m, err := binary.ReadUvarint(b)
		certder := make([]byte, m)
		b.Read(certder)
		cert, err := x509.ParseCertificate(certder)
		if err != nil {
			return nil, err
		}
		chain[i] = cert
	}

	chain[n] = ca // stick the ca on the end

	priv, err := x509.ParseECPrivateKey(privder)
	if err != nil {
		return nil, err
	}

	return &Entity{
		certs: chain,
		priv:  priv,
	}, nil
}

var ca1 sync.Once
var ca *x509.Certificate

func loadCAFromEnv() *x509.Certificate {
	ca1.Do(func() {
		enc := os.Getenv("OMEN_CA_CERT")
		if enc == "" {
			enc = builtinCA // fallback
		}
		//return nil, ErrMissingCertificateAuthorityCert

		certder, err := base64.StdEncoding.DecodeString(enc)
		if err != nil {
			panic(err)
		}

		ca, err = x509.ParseCertificate(certder)
		if err != nil {
			panic(err)
		}
	})
	return ca
}

const xbuiltinCA = `MIICTTCCAfKgAwIBAgIBATAKBggqhkjOPQQDAjCBjDELMAkGA1UEBhMCVVMxDjAMBgNVBAgTBVRleGFzMQ8wDQYDVQQHEwZBdXN0aW4xHDAaBgNVBAoTE0hleHBsb3JlIE9tZW4gR3JvdXAxHjAcBgNVBAsTFUNlcnRpZmljYXRlIEF1dGhvcml0eTEeMBwGA1UEAxMVSGV4cGxvcmUgT21lbiBSb290IENBMB4XDTE4MDQyMjIyNDE0MloXDTIwMDQyMTIyNDE0MlowgYwxCzAJBgNVBAYTAlVTMQ4wDAYDVQQIEwVUZXhhczEPMA0GA1UEBxMGQXVzdGluMRwwGgYDVQQKExNIZXhwbG9yZSBPbWVuIEdyb3VwMR4wHAYDVQQLExVDZXJ0aWZpY2F0ZSBBdXRob3JpdHkxHjAcBgNVBAMTFUhleHBsb3JlIE9tZW4gUm9vdCBDQTBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABNxqbarLkmloaBreRWQHUmy0nu+NcgcKZprFzAayESVCBgI7uOaz3+kataquDJa+5by72ybcWgSFCumIdHSAl8KjQzBBMA4GA1UdDwEB/wQEAwIBBjASBgNVHRMBAf8ECDAGAQH/AgEDMBsGA1UdEQQUMBKCEG9tZW4uaGV4cGxvcmUudXMwCgYIKoZIzj0EAwIDSQAwRgIhAIxbsssTlrXP60Z8Gf8V7TZ1Vvu4i61Pa9zjVefKjFeXAiEAywhUg0ndz27xdK8xk6AOIbxOahXibK6XP6cnmekpBxg=`

const builtinCA = `MIICTTCCAfKgAwIBAgIBATAKBggqhkjOPQQDAjCBjDELMAkGA1UEBhMCVVMxDjAMBgNVBAgTBVRleGFzMQ8wDQYDVQQHEwZBdXN0aW4xHDAaBgNVBAoTE0hleHBsb3JlIE9tZW4gR3JvdXAxHjAcBgNVBAsTFUNlcnRpZmljYXRlIEF1dGhvcml0eTEeMBwGA1UEAxMVSGV4cGxvcmUgT21lbiBSb290IENBMB4XDTE4MDQyNDE1NDAxMFoXDTIwMDQyMzE1NDAxMFowgYwxCzAJBgNVBAYTAlVTMQ4wDAYDVQQIEwVUZXhhczEPMA0GA1UEBxMGQXVzdGluMRwwGgYDVQQKExNIZXhwbG9yZSBPbWVuIEdyb3VwMR4wHAYDVQQLExVDZXJ0aWZpY2F0ZSBBdXRob3JpdHkxHjAcBgNVBAMTFUhleHBsb3JlIE9tZW4gUm9vdCBDQTBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABBdxOMSXgRbucqeLDPwVINCKF4ZH+tV+clyfjMZrLyymDVW+kapmJN8dggd1YKPJeykVoZcgNb6keMwVfqxR7iKjQzBBMA4GA1UdDwEB/wQEAwIBBjASBgNVHRMBAf8ECDAGAQH/AgEDMBsGA1UdEQQUMBKCEG9tZW4uaGV4cGxvcmUudXMwCgYIKoZIzj0EAwIDSQAwRgIhAMjWFHELlhzP06dxVsi/t1h/nMTIHRRGVytJ15wXsCUMAiEA0JMN/HyDtaH1l5Lc1pfAyQ2Zpa8BXpWNAdhBkkigjLE=`
