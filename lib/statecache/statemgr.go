package statecache

// TODO incoming context getting canceled (the use of 'done' in req*)
// *** NOTE *** this is what's causing omen-proxy to crash after a
// while; incoming WaitForChange's get stacked up in goroutines,
// holding connections open etc., even though they've been long since
// canceled

import (
	"context"
	"strings"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/omen3/lib/db"
)

var log = logging.New("statecache")

func New(next db.StateSupplier) *StateCache {
	chn := make(chan *reqn, 10)
	giveups := make(chan *reqn, 10)

	s := &StateCache{
		reqn:   chn,
		next:   next,
		giveup: giveups,
	}

	go s.run(chn, giveups)
	return s
}

type reqn struct {
	given    []*db.State
	pending  int
	callback chan<- []*db.State
}

type StateCache struct {
	reqn   chan<- *reqn
	giveup chan<- *reqn
	next   db.StateSupplier
}

func (s *StateCache) Get(ctx context.Context, name string) (*db.State, error) {
	s0 := &db.State{
		Name: name,
	}
	states, err := s.WaitForChange(ctx, []*db.State{s0})
	if err != nil {
		return nil, err
	}
	return states[0], nil
}

type observing struct {
	attention time.Time // last time there was attention on this one
	last      *db.State
	waiters   map[*reqn]struct{}
}

func (s *StateCache) run(chn <-chan *reqn, giveups <-chan *reqn) {
	// TODO flip this around so there's a single key
	cache := make(map[string]*observing)

	ctx := context.Background()

	var cancelwatch func()
	updates := make(chan []*db.State)

	rewatch := func() {
		if cancelwatch != nil {
			cancelwatch()
		}

		local := make([]*db.State, len(cache))

		var lst []string
		i := 0
		for k, v := range cache {
			local[i] = &db.State{
				Name: k,
			}
			if v.last != nil {
				local[i].Sequence = v.last.Sequence
			}
			lst = append(lst, k)
			i++
		}
		log.Debugf(ctx, "starting watch on: %s",
			strings.Join(lst, ", "))
		ctx, can := context.WithCancel(context.Background())
		cancelwatch = can
		go s.watch(ctx, updates, local)
	}

	computeSatisfaction := func(askn *reqn) (ret []*db.State) {
		for _, s := range askn.given {
			o := cache[s.Name]
			if o.last == nil {
				// pending counter should keep us from being here
				panic("should not get here")
			}
			if o.last.Sequence > s.Sequence {
				ret = append(ret, o.last)
			}
		}
		return
	}

	ticker := time.NewTicker(10 * time.Second)

	for {
		log.Debugf(ctx, "status: observing %d", len(cache))
		for k, v := range cache {
			if v.last == nil {
				log.Debugf(ctx, "status: %s has %d waiters NO LAST",
					k, len(v.waiters))
			} else {
				log.Debugf(ctx, "status: %s has %d waiters last #%d",
					k, len(v.waiters), v.last.Sequence)
			}
		}
		select {
		case t := <-ticker.C:
			var aging [11]int
			var drop []string
			var active int

			for k, v := range cache {
				if len(v.waiters) == 0 {
					age := int(t.Sub(v.attention) / (10 * time.Second))
					if age >= 10 {
						age = 10
						drop = append(drop, k)
					}
					aging[age]++
				} else {
					active++
				}
			}
			log.Debugf(ctx, "ticking active: %d idle: %d ; dropping %d",
				active, aging[:], len(drop))
			for _, k := range drop {
				delete(cache, k)
			}

		case askn := <-chn:
			t := time.Now()
			log.Debugf(ctx, "asked to listen at %p", askn)
			// two cases: (1) there is a match, in which
			// case return it right away, (2) there is
			// not, in which case remember the ask for
			// later
			var match []*db.State
			missing := false
			added := false

			for _, s := range askn.given {
				n := s.Name
				r, ok := cache[n]
				if ok {
					r.attention = t
				} else {
					r = &observing{
						waiters:   make(map[*reqn]struct{}),
						attention: t,
					}
					cache[n] = r
					added = true
				}
				if r.last == nil {
					// there is no known state; we're obliged to wait
					// for the first fill
					missing = true
					askn.pending++ // still waiting for this to fill
				} else if !missing && r.last.Sequence > s.Sequence {
					match = append(match, r.last)
				}
			}
			if !missing && match != nil {
				log.Debugf(ctx, "%d matches already", len(match))
				askn.callback <- match
				close(askn.callback)

			} else {
				log.Debugf(ctx, "no matches (missing=%t)", missing)
				// no matches or lack info about some,
				// so add it to the waiters
				for _, s := range askn.given {
					n := s.Name
					cache[n].waiters[askn] = struct{}{}
				}
				// update the watch if we added something
				// to the set of things we're watching
				if added {
					rewatch()
				}
			}

		case up := <-updates:
			satisfy := make(map[*reqn][]*db.State)

			// figure out which waiters, if any, we're going to satisfy
			for _, s := range up {
				o := cache[s.Name]
				firstfill := o.last == nil
				o.last = s
				for askn := range o.waiters {
					if firstfill {
						askn.pending-- // one less pending, now
						log.Debugf(ctx, "first fill for %q, %d left", s.Name, askn.pending)
					}
				}
			}

			for _, s := range up {
				for askn := range cache[s.Name].waiters {
					if askn.pending == 0 {
						// see if it's satisfied now
						l := computeSatisfaction(askn)
						if len(l) > 0 {
							satisfy[askn] = l
						}
					}
				}
			}

			// for each request that is being satisfied,
			// remove it from the waiting list of all the
			// observables that it was waiting on
			for askn, value := range satisfy {
				for _, s := range askn.given {
					n := s.Name
					delete(cache[n].waiters, askn)
				}
				askn.callback <- value
				close(askn.callback)
			}

		case giveup := <-giveups:
			log.Debugf(ctx, "giving up on %p", giveup)
			// remove this request from the waiting list of
			// all the observables it was waiting on
			for _, s := range giveup.given {
				n := s.Name
				delete(cache[n].waiters, giveup)
			}
			// the fact that we received this request on
			// the giveup channel means the caller is no
			// longer listening for a response.  No need
			// to close it (which may race with an actual
			// response going back and hence being closed
			// -- i.e., we may have already sent a
			// response to this waiter though they hadn't
			// received it by the time they sent the
			// giveup).  Point being, there's (a) no reason
			// to close the channel, since they aren't listening
			// anyway, and (b) it's dangerous to do so because
			// we might have *already* closed it.
			//close(giveup.callback)
		}
	}
}

func dropw(lst []*reqn, drop *reqn) []*reqn {
	if len(lst) == 1 {
		if lst[0] != drop {
			panic("not present")
		}
		return nil
	}

	out := make([]*reqn, 0, len(lst)-1)
	for _, r := range lst {
		if r != drop {
			out = append(out, r)
		}
	}
	return out
}

func (s *StateCache) watch(ctx context.Context, dst chan<- []*db.State, current []*db.State) {
	log.Debugf(ctx, "watch %d", len(current))

	toc := make(map[string]int)
	local := make([]*db.State, len(current))

	for i, s := range current {
		toc[s.Name] = i
		tmp := new(db.State)
		*tmp = *s
		local[i] = tmp
	}

	for {
		states, err := s.next.WaitForChange(ctx, local)
		if err != nil {
			log.Errorf(ctx, "error during WaitForChange: %s", err)
			return
		}
		dst <- states
		for _, s := range states {
			log.Debugf(ctx, "watch returned %s at #%d", s.Name, s.Sequence)
			if k, ok := toc[s.Name]; ok {
				// bump the sequence
				local[k].Sequence = s.Sequence
			}
		}
	}
}

// we are ourselves a StateSupplier
func (s *StateCache) WaitForChange(ctx context.Context, current []*db.State) ([]*db.State, error) {
	// make sure there's always room for the control loop to send
	// us a reply. otherwise we could wind up deadlocked because
	// the control loop is trying to send a reply but we are
	// trying to send it a cancelation because we fell through the
	// ctx.Done() case

	ch := make(chan []*db.State, 1) // make sure there's room

	r := &reqn{
		given:    current,
		callback: ch,
	}
	log.Debugf(ctx, "send reqn %p", r)
	s.reqn <- r
	select {
	case answer := <-ch:
		return answer, nil
	case <-ctx.Done():
		log.Debugf(ctx, "context is done; we're giving up")
		s.giveup <- r
		return nil, context.Canceled
	}
}

func (s *StateCache) Apply(ctx context.Context, txn *db.TxnRecord) (uint64, time.Time, []*db.State, error) {
	return s.next.Apply(ctx, txn)
}
