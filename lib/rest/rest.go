package rest

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/omen3/lib/db"
	"bitbucket.org/dkolbly/omen3/ptr"
)

var log = logging.New("restclient")

type RestClient struct {
	base *url.URL
	h    *http.Client
}

func Dial(base string) *RestClient {
	u, err := url.Parse(base)
	if err != nil {
		panic(err)
	}

	return &RestClient{
		base: u,
		h: &http.Client{
			CheckRedirect: noRedirect,
		},
	}
}

type jsonObs struct {
	Name     string     `json:"name"`
	Value    ptr.Ptr    `json:"value,omitempty"`
	ModTime  *time.Time `json:"modified_time,omitempty"`
	Sequence uint64     `json:"sequence,omitempty"`
	Audit    uint64     `json:"audit,omitempty"`
}

type jsonTxnAck struct {
	ID        uint64     `json:"id"`
	Timestamp time.Time  `json:"timestamp"`
	States    []*jsonObs `json:"states"`
}

type jsonTxn struct {
	ID        uint64      `json:"id,omitempty"`
	Audit     uint64      `json:"audit,omitempty"`
	Timestamp *time.Time  `json:"timestamp,omitempty"`
	Prereqs   []db.Prereq `json:"prereqs,omitempty"`
	Edits     []db.Edit   `json:"edits,omitempty"`
}

func jstate(j *jsonObs) *db.State {
	state := &db.State{
		Name:     j.Name,
		Value:    j.Value,
		Audit:    j.Audit,
		Sequence: j.Sequence,
	}
	if j.ModTime != nil {
		state.Modified = *j.ModTime
	}
	return state
}

func statej(s *db.State) *jsonObs {
	j := &jsonObs{
		Name:     s.Name,
		Value:    s.Value,
		Sequence: s.Sequence,
		Audit:    s.Audit,
	}
	if !s.Modified.IsZero() {
		j.ModTime = &s.Modified
	}
	return j
}

func noRedirect(r *http.Request, via []*http.Request) error {
	return http.ErrUseLastResponse
}

func (c *RestClient) rel(parts ...string) string {
	all := make([]string, len(parts)+1)
	all[0] = c.base.Path
	copy(all[1:], parts)

	u := &url.URL{
		Scheme: c.base.Scheme,
		Host:   c.base.Host,
		Path:   path.Join(all...),
	}
	return u.String()
}

func (c *RestClient) Obs(ctx context.Context, name string) (*db.State, error) {

	x := c.rel("api/obs") + "/" + name
	r, err := http.NewRequest(http.MethodGet, x, nil)
	if err != nil {
		return nil, err
	}
	resp, err := c.h.Do(r)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == http.StatusNotFound {
			return nil, ptr.ErrNotPresent(ptr.Obs(name))
		}
		return nil, errors.New("server responded non-OK")
	}

	var jo jsonObs
	err = json.Unmarshal(buf, &jo)
	if err != nil {
		return nil, err
	}

	return jstate(&jo), nil
}

func (c *RestClient) Load(ctx context.Context, at ptr.Ptr) (*db.Atom, error) {

	switch at.Metatype() {
	case ptr.Untyped, ptr.HasTypePointer:
	default:
		panic("can only load stored CAM data")
	}

	r, err := http.NewRequest(
		http.MethodGet,
		c.rel("api/atom", at.String()),
		nil)
	if err != nil {
		return nil, err
	}
	resp, err := c.h.Do(r)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("load failed")
	}
	defer resp.Body.Close()
	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	a := &db.Atom{
		Payload: buf,
		Addr:    at,
	}

	t := resp.Header.Get("X-Omen-Type")
	var actual ptr.Ptr
	if t != "" && !strings.EqualFold(t, "none") {
		tp, ok := ptr.DecodeStringPtr(t)
		if !ok {
			return nil, errors.New("invalid type returned")
		}
		a.Type = tp
		actual = ptr.TypedContentAddress(tp, buf)
	} else {
		actual = ptr.ContentAddress(ptr.Untyped, buf)
	}
	if actual != at {
	}

	return a, nil
}

func (c *RestClient) Alloc(ctx context.Context) uint64 {
	r, err := http.NewRequest(http.MethodPost, c.rel("api/id"), nil)

	if err != nil {
		panic(err)
		return 0
	}

	resp, err := c.h.Do(r)
	if err != nil {
		panic(err)
		return 0
	}

	defer resp.Body.Close()

	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
		return 0
	}

	if resp.StatusCode != http.StatusCreated {
		panic("not CREATED")
		return 0
	}

	var k uint64
	err = json.Unmarshal(buf, &k)
	if err != nil {
		return 0
	}
	return k
}

func (c *RestClient) Store(ctx context.Context, t0 db.DataType, data []byte) (*db.Atom, error) {

	r, err := http.NewRequest(http.MethodPost, c.rel("api/atom"),
		bytes.NewReader(data))
	if err != nil {
		return nil, err
	}

	if t0 != nil {
		r.Header.Set("X-Omen-Type", t0.Addr().String())
	}

	resp, err := c.h.Do(r)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	if resp.StatusCode != http.StatusMovedPermanently {
		return nil, fmt.Errorf("store failed (%d)", resp.StatusCode)
	}
	loc := resp.Header.Get("Location")
	parts := strings.SplitN(loc, "/", 4)
	if len(parts) != 4 {
		return nil, errors.New("store failed; bad redirect")
	}

	p, ok := ptr.DecodeStringPtr(parts[3])
	if !ok {
		return nil, errors.New("store failed; invalid redirect")
	}

	return &db.Atom{
		Payload: data,
		Addr:    p,
	}, nil
}

func (c *RestClient) Apply(ctx context.Context, txn *db.TxnRecord) (uint64, time.Time, []*db.State, error) {
	j := &jsonTxn{
		ID:      txn.ID,
		Audit:   txn.Audit,
		Prereqs: txn.Prereqs,
		Edits:   txn.Edits,
	}
	if !txn.Timestamp.IsZero() {
		j.Timestamp = &txn.Timestamp
	}

	buf, err := json.MarshalIndent(j, "", "  ")
	if err != nil {
		panic(err)
	}

	r, err := http.NewRequest(
		http.MethodPost,
		c.rel("api/txn"),
		bytes.NewReader(buf))

	if err != nil {
		return 0, time.Time{}, nil, err
	}
	resp, err := c.h.Do(r)
	if err != nil {
		return 0, time.Time{}, nil, err
	}

	defer resp.Body.Close()

	buf, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, time.Time{}, nil, err
	}

	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == http.StatusPreconditionFailed {
			return 0, time.Time{}, nil, db.ErrConcurrencyFailure
		}
		return 0, time.Time{}, nil, txnError(resp.Status)
	}

	var ja jsonTxnAck

	json.Unmarshal(buf, &ja)

	results := make([]*db.State, len(ja.States))
	for i, src := range ja.States {
		results[i] = jstate(src)
	}
	return ja.ID, ja.Timestamp, results, nil
}

type queryResult struct {
	Query   string   `json:"query"`
	Cursor  string   `json:"cursor,omitempty"`
	Results []string `json:"results"`
}

func (c *RestClient) Glob(ctx context.Context, pattern string) ([]string, error) {
	q := url.Values{
		"q": []string{pattern},
	}
	x := c.rel("api/obs") + "?" + q.Encode()

	r, err := http.NewRequest(http.MethodGet, x, nil)
	if err != nil {
		return nil, err
	}
	resp, err := c.h.Do(r)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("server responded non-OK")
	}

	var qr queryResult
	err = json.Unmarshal(buf, &qr)
	if err != nil {
		return nil, err
	}

	return qr.Results, nil
}

type waitReq struct {
	Name  string `json:"name"`
	After uint64 `json:"after,omitempty"`
}

func (c *RestClient) WaitForChange(ctx context.Context, current []*db.State) ([]*db.State, error) {
	req := make([]waitReq, len(current))
	for i, s := range current {
		req[i].Name = s.Name
		req[i].After = s.Sequence
	}
	data, _ := json.Marshal(req)
	log.Debugf(ctx, "SENDING %s", data)

	r, err := http.NewRequest(
		http.MethodPost, c.rel("api/wait"),
		bytes.NewReader(data))
	r.Header.Set("Content-Type", "application/json")

	resp, err := c.h.Do(r.WithContext(ctx))
	if err != nil {

		// special case -- return context.Canceled if our caller broke out

		select {
		case <-ctx.Done():
			return nil, context.Canceled
		default:
		}
		log.Errorf(ctx, "ERROR: %s (type %T)", err, err)
		return nil, err
	}

	defer resp.Body.Close()

	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == http.StatusRequestTimeout {
			return nil, context.Canceled
		}
		return nil, txnError(resp.Status)
	}

	var jo []jsonObs
	err = json.Unmarshal(buf, &jo)
	if err != nil {
		panic(err)
	}

	s := make([]*db.State, len(jo))
	for i, j := range jo {
		s[i] = jstate(&j)
	}
	return s, nil
}

func (c *RestClient) Wait(ctx context.Context, name string, after uint64) (*db.State, error) {
	q := url.Values{}
	if after > 0 {
		q.Set("after", strconv.FormatUint(after, 10))
	}
	x := c.rel("api/wait", name) + "?" + q.Encode()
	r, err := http.NewRequest(http.MethodGet, x, nil)
	if err != nil {
		return nil, err
	}
	resp, err := c.h.Do(r.WithContext(ctx))
	if err != nil {
		if err, ok := err.(net.Error); ok && err.Timeout() {
			return nil, context.Canceled
		}
		return nil, err
	}

	defer resp.Body.Close()
	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == http.StatusNotFound {
			return nil, ptr.ErrNotPresent(ptr.Obs(name))
		}
		return nil, errors.New("server responded non-OK")
	}

	var jo jsonObs
	err = json.Unmarshal(buf, &jo)
	if err != nil {
		return nil, err
	}

	return jstate(&jo), nil
}

type txnError string

func (err txnError) Error() string {
	return fmt.Sprintf("transaction failed: %s", string(err))
}
