package omen

import (
	"sync"
	"time"
)

type idsupplier struct {
	lock         sync.Mutex
	buffer       []uint64
	index        int
	agent        string
	lastAskCount int
	lastAskTime  time.Time
}

func newIdSupplier(agent string) {
	panic("todo")
}

func (ids *idsupplier) Next() uint64 {
	ids.lock.Lock()
	defer ids.lock.Unlock()

	if ids.index < len(ids.buffer) {
		ids.index++
		return ids.buffer[ids.index-1]
	}

	/*
		// need more... estimate allocation rate
		t := time.Now()
		ask := ids.lastAskCount

		if ask == 0 {
			// haven't asked before; just grab one
			ask = 1
		} else {
			dt := t.Sub(ids.lastAskTime)

			scale := 1.0
			if dt > upperThreshold {
				// it's been a while since we asked... back it down
				ask = lessAsk(ask)
			} else if dt < lowerThreshold {
				// it's not been very long... crank it up
				ask = moreAsk(ask)
			}
		}
	*/

	// go get some
	panic("TODO")
}

const upperThreshold = 2 * time.Second
const lowerThreshold = 200 * time.Millisecond

func moreAsk(n int) int {
	if n < 10 {
		return n + 1
	} else if n > 1000 {
		return 1000
	}
	return n + (n / 4) // +25%
}

func lessAsk(n int) int {
	if n == 1 {
		return 1
	} else if n < 10 {
		return n - 1
	}
	return n - (n / 4) // -25%
}
