package omen

import (
	//"context"

	"bitbucket.org/dkolbly/logging"
	//"bitbucket.org/dkolbly/omen3/lib/pki"
	//"google.golang.org/grpc"
	//"google.golang.org/grpc/credentials"
)

var log = logging.New("omen")

type Client struct {
	agent string
	//alloc *pbomen.IDAllocator
	ids *idsupplier
	//conn *omen.Client
}

/*
func (c *Client) Alloc(ctx context.Context) uint64 {
	return c.alloc.next(ctx)
}

// Begin implements omenapi.API
func (c *Client) Begin(ctx context.Context, audit uint64) omenapi.Tx {
	return c.conn.Begin(ctx, audit)
}

func Dial(ctx context.Context) (*Client, error) {
	// connect to the omen infrastracture
	conn, err := omen.Dial(ctx, omen.Tenant("vista"))
	if err != nil {
		return nil, err
	}

	creds, agent := setupClientPKI(ctx)
	if err != nil {
		return nil, err
	}
	log.Debugf(ctx, "agent <%s> creds are %p", agent, creds)

	c, err := grpc.Dial("localhost:50060",
		grpc.WithTransportCredentials(creds),
	)
	if err != nil {
		return nil, err
	}

	return &Client{
		agent: agent,
		alloc: newSequencer(c, agent),
		//alloc: pbomen.NewIDAllocator(conn, "vista", "xyzzy", 100, agent),
		conn: conn,
	}, nil
}

//func (c *Client)

// return the transport credentials for grpc plus the agent name
func setupClientPKI(ctx context.Context) (credentials.TransportCredentials, string) {
	e, err := pki.LoadEntityFromEnv("CLIENT")
	if err != nil {
		panic(err)
	}
	s, ok := e.Subject(ctx)
	if !ok {
		panic("client cert has no tenantName/agentIdentifier")
	}
	return credentials.NewTLS(e.TLSClientConfig()), s.AgentIdentifier
}
*/

/*
// c.f. https://bbengfort.github.io/programmer/2017/03/03/secure-grpc.html
func setupClientPKI(ctx context.Context) (credentials.TransportCredentials, string) {

	// set up our trust pool
	certPool := x509.NewCertPool()
	ca, err := ioutil.ReadFile(caCertDir + "cert.pem")
	if err != nil {
		panic(err)
	}
	if !certPool.AppendCertsFromPEM(ca) {
		panic("could not include cert")
	}

	log.Debugf(ctx, "%d certs in pool", len(certPool.Subjects()))
	// load our own (server) certificate
	cert, err := tls.LoadX509KeyPair(
		certDir+"cert.pem",
		certDir+"private.pem",
	)
	if err != nil {
		log.Errorf(ctx, "could not load certificate: %s", err)
		os.Exit(1)
	}


	leaf, err := x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		// wouldn't expect to be here... if it failed, it should
		// have failed already above
		panic(err)
	}
	subj, ok := pki.Parse(ctx, [][]*x509.Certificate{{leaf}})
	if !ok {
		log.Errorf(ctx, "certificate does not comply with omen client protocol")
		os.Exit(1)
	}
	log.Infof(ctx, "yo: %#v", subj)

	return credentials.NewTLS(&tls.Config{
		ServerName:   "localhost", // this has to match the CN
					   // of the server's cert
		Certificates: []tls.Certificate{cert},
		RootCAs:      certPool,
	}), subj.AgentIdentifier
}
*/
