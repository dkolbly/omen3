package store

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"sort"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/dkolbly/omen3/lib/store/pb"
	"bitbucket.org/dkolbly/omen3/ptr"
	"github.com/golang/protobuf/proto"
)

var log = logging.New("store")

type LocalObsId uint64

type Store struct {
	dir       string
	logch     chan logWrite
	writing   *os.File
	namelock  sync.RWMutex
	names     map[string]LocalObsId
	pendlock  sync.RWMutex
	pending   map[uint64]*pender
	statelock sync.RWMutex
	states    map[LocalObsId]*state
}

type pender struct {
	txn      *pb.Txn
	touching []touch
}

type state struct {
	lock  sync.RWMutex
	lid   LocalObsId
	audit uint64  // the audit id that last updated us
	mtime int64   // the time we were last udpated (nsec)
	ctime int64   // the time we were created (nsec)
	rev   uint64  // the revision seq # (for a series defined by a single ctime), 1=first revision
	value ptr.Ptr // the current value
}

func (s *state) rlock() {
	//log.Debugf(context.Background(), "[%d] R lock", s.lid)
	s.lock.RLock()
}

func (s *state) runlock() {
	//log.Debugf(context.Background(), "[%d] R unlock", s.lid)
	s.lock.RUnlock()
}

func (s *state) wlock() {
	//log.Debugf(context.Background(), "[%d] W lock", s.lid)
	s.lock.Lock()
}

func (s *state) wunlock() {
	//log.Debugf(context.Background(), "[%d] W unlock", s.lid)
	s.lock.Unlock()
}

func (s *state) eqv(bits []byte) bool {
	return bytes.Compare(s.value.Bits[:], bits) == 0
}

func Open(ctx context.Context, dir string) (*Store, error) {
	// start a new log

	logdir := path.Join(dir, "log.d")
	fi, err := os.Stat(logdir)

	if err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(logdir, 0700)
		}
		if err != nil {
			return nil, err
		}
	} else if !fi.IsDir() {
		return nil, fmt.Errorf("broken store: %s is not a directory", logdir)
	}

	lst, err := ioutil.ReadDir(logdir)
	if err != nil {
		return nil, err
	}

	n := maxseq(lst)
	ch := make(chan logWrite, 10)

	s := &Store{
		dir:     dir,
		names:   make(map[string]LocalObsId, 5000),
		pending: make(map[uint64]*pender),
		states:  make(map[LocalObsId]*state),
		logch:   ch,
	}

	// brute force: replay all the logs
	for j := 1; j <= n; j++ {
		log.Debugf(ctx, "Checking [%d]", j)
		err = s.readLog(ctx, j, replaying{s})
		if err != nil {
			log.Errorf(ctx, "failed: %s", err)
		}
	}

	fd, err := os.Create(path.Join(logdir, fmt.Sprintf("%06d", n+1)))
	if err != nil {
		return nil, err
	}

	s.writing = fd

	go s.logWrite(ch)

	s.toLogSync(entryStart, &pb.LogStart{
		Index:  int32(n + 1),
		TimeNs: time.Now().UnixNano(),
	})
	return s, nil
}

type entryTag uint8

const (
	entryEnd = entryTag(iota << 4)
	entryPrepare
	entryStart
	entryCommit
	entryName
	entryAbort
)

var errMessageTooBig = errors.New("message too big")
var errShortWrite = errors.New("short write")
var errShortRead = errors.New("short read")
var errBadHeader = errors.New("bad header")
var errBadTag = errors.New("bad tag")

type logWrite struct {
	tag  entryTag
	buf  []byte
	sync chan struct{} // writer will close this if non-nil
}

func (s *Store) toLogSync(tag entryTag, msg proto.Message) {
	buf, err := proto.Marshal(msg)
	if err != nil {
		panic(err)
	}
	if len(buf) >= (1 << 19) {
		panic("message too big")
	}

	ack := make(chan struct{})
	s.logch <- logWrite{tag, buf, ack}
	<-ack
}

func (s *Store) toLog(tag entryTag, msg proto.Message) {
	buf, err := proto.Marshal(msg)
	if err != nil {
		panic(err)
	}
	if len(buf) >= (1 << 19) {
		panic("message too big")
	}

	s.logch <- logWrite{tag, buf, nil}
}

func (s *Store) logWrite(ch <-chan logWrite) {
	ctx := context.Background()
	for w := range ch {
		s.appendLog1(ctx, w)
		if w.sync != nil {
			close(w.sync)
		}
	}
}

func (s *Store) appendLog1(ctx context.Context, w logWrite) error {
	tag := w.tag

	var hdr [4]byte
	hdr[0] = 0xEA
	blen := len(w.buf)
	hdr[1] = uint8(tag) + 0xF&uint8(blen>>16) // 20 bits of length data, up to 1MB
	hdr[2] = uint8(blen >> 8)
	hdr[3] = uint8(blen)

	n, err := s.writing.Write(hdr[:])
	if err != nil {
		return err
	}
	if n != 4 {
		return errShortWrite
	}
	n, err = s.writing.Write(w.buf)
	if err != nil {
		return err
	}
	if n != blen {
		return errShortWrite
	}

	return nil
}

type logReader interface {
	end(context.Context, *pb.LogEnd) error
	prepare(context.Context, *pb.Txn) error
	commit(context.Context, *pb.Txn) error
	abort(context.Context, *pb.Txn) error
	start(context.Context, *pb.LogStart) error
	name(context.Context, *pb.Name) error
}

func (s *Store) readLog(ctx context.Context, seq int, lr logReader) error {

	f := path.Join(s.dir, "log.d", fmt.Sprintf("%06d", seq))
	fd, err := os.Open(f)
	if err != nil {
		return err
	}
	defer fd.Close()

	src := bufio.NewReader(fd)
	tmp := make([]byte, 1<<19)

	for {
		var hdr [4]byte
		n, err := src.Read(hdr[:])
		if err == io.EOF {
			break
		}
		if n != 4 {
			return errShortRead
		}
		if err != nil {
			return err
		}
		if hdr[0] != 0xEA {
			return errBadHeader
		}
		tag := entryTag(hdr[1] & 0xF0)

		buflen := int(hdr[1]&0xF)<<16 +
			int(hdr[2])<<8 +
			int(hdr[3])

		buf := tmp[:buflen]
		n, err = src.Read(buf)
		if n != buflen {
			return errShortRead
		}
		if err != nil {
			return err
		}

		switch tag {
		case entryEnd:
			var msg pb.LogEnd
			err = proto.Unmarshal(buf, &msg)
			if err != nil {
				return err
			}
			err = lr.end(ctx, &msg)
		case entryStart:
			var msg pb.LogStart
			err = proto.Unmarshal(buf, &msg)
			if err != nil {
				return err
			}
			err = lr.start(ctx, &msg)
		case entryName:
			var msg pb.Name
			err = proto.Unmarshal(buf, &msg)
			if err != nil {
				return err
			}
			err = lr.name(ctx, &msg)

		case entryPrepare, entryCommit, entryAbort:
			var msg pb.Txn
			err = proto.Unmarshal(buf, &msg)
			if err != nil {
				return err
			}
			switch tag {
			case entryPrepare:
				err = lr.prepare(ctx, &msg)
			case entryCommit:
				err = lr.commit(ctx, &msg)
			case entryAbort:
				err = lr.abort(ctx, &msg)
			}

		default:
			panic(fmt.Sprintf("unhandled tag %d", tag))
			return errBadTag
		}
		if err != nil {
			return nil
		}
	}
	return nil
}

func maxseq(lst []os.FileInfo) int {
	max := 0
	for _, entry := range lst {
		n, err := strconv.ParseInt(entry.Name(), 10, 32)
		if err == nil {
			if int(n) > max {
				max = int(n)
			}
		}
	}
	return max
}

type replaying struct {
	*Store
}

func (r replaying) end(ctx context.Context, msg *pb.LogEnd) error {
	t := time.Unix(0, msg.TimeNs).In(time.Local)
	log.Debugf(ctx, "end at %s after %d records", t.Format(time.RFC3339Nano), msg.Count)
	return nil
}

func (r replaying) prepare(ctx context.Context, txn *pb.Txn) error {
	r.Store.doPrepare(ctx, txn)
	log.Debugf(ctx, "preparing #%d", txn.Id)
	return nil
}

func (r replaying) commit(ctx context.Context, txn *pb.Txn) error {
	log.Debugf(ctx, "committing #%d", txn.Id)
	r.Store.doCommit(ctx, txn.Id)
	return nil
}

func (r replaying) abort(ctx context.Context, txn *pb.Txn) error {
	log.Debugf(ctx, "aborting #%d", txn.Id)
	r.Store.doAbort(ctx, txn.Id)
	return nil
}

func (r replaying) name(ctx context.Context, n *pb.Name) error {
	log.Debugf(ctx, "name %q --> %d", n.Name, n.Id)
	r.Store.names[n.Name] = LocalObsId(n.Id)
	return nil
}

func (r replaying) start(ctx context.Context, msg *pb.LogStart) error {
	t := time.Unix(0, msg.TimeNs).In(time.Local)
	log.Debugf(ctx, "start at %s", t.Format(time.RFC3339Nano))
	return nil
}

func (s *Store) Prepare(ctx context.Context, txn *pb.Txn) (bool, error) {
	ok := s.doPrepare(ctx, txn)
	if !ok {
		return false, nil
	}

	s.toLog(entryPrepare, txn)
	// TODO flush to disk
	return true, nil
}

func (s *Store) doPrepare(ctx context.Context, txn *pb.Txn) bool {
	p := s.makePending(ctx, txn)
	err := p.acquire()
	if err != nil {
		return false
	}
	s.pendlock.Lock()
	s.pending[txn.Id] = p
	s.pendlock.Unlock()
	return true
}

type touchSlice []touch

func (lst touchSlice) Len() int           { return len(lst) }
func (lst touchSlice) Less(i, j int) bool { return lst[i].subject.lid < lst[j].subject.lid }
func (lst touchSlice) Swap(i, j int)      { lst[i], lst[j] = lst[j], lst[i] }

type touch struct {
	subject *state
	read    bool
	eff     *pb.Effect
}

func (s *Store) makePending(ctx context.Context, txn *pb.Txn) *pender {
	var touching []touch
	for _, pre := range txn.Prereqs {
		k := LocalObsId(pre.Subject)
		t := touch{
			subject: s.obs(k),
			read:    true,
			eff:     pre,
		}
		touching = append(touching, t)
		//p.reading = append(p.reading, s.obs(k))
	}

	for _, eff := range txn.Effects {
		k := LocalObsId(eff.Subject)
		t := touch{
			subject: s.obs(k),
			eff:     eff,
		}
		touching = append(touching, t)
		//p.updating = append(p.updating, s.obs(k))
	}

	sort.Sort(touchSlice(touching))

	return &pender{
		txn:      txn,
		touching: touching,
	}
}

func (t touch) lock() {
	if t.read {
		t.subject.rlock()
	} else {
		t.subject.wlock()
	}
}

func (t touch) unlock() {
	if t.read {
		t.subject.runlock()
	} else {
		t.subject.wunlock()
	}
}

func (t touch) satisfied() error {
	if t.subject.satisfies(t.eff) {
		return nil
	}
	return errNotSatisfied(t)
}

func (t touch) commit(audit uint64, ts int64) {
	if t.read {
		t.subject.runlock()
		return
	}

	eff := t.eff
	s := t.subject

	s.audit = audit
	if eff.ToValue == nil {
		// the obs is being deleted
		s.rev = 0
		s.ctime = 0
		s.mtime = 0
		s.value = ptr.Ptr{}
	} else if eff.FromValue == nil {
		// the obs is being created
		s.rev = 1
		s.ctime = ts
		s.mtime = ts
		copy(s.value.Bits[:], eff.ToValue)
	} else {
		s.rev++
		s.mtime = ts
		copy(s.value.Bits[:], eff.ToValue)
	}
	s.wunlock()
}

type errNotSatisfied touch

func (err errNotSatisfied) Error() string {
	return "not satisfied"
}

func (p *pender) acquire() error {
	for i, t := range p.touching {
		t.lock()
		err := t.satisfied()
		if err != nil {
			for _, u := range p.touching[:i+1] {
				u.unlock()
			}
			return err
		}
	}
	return nil
}

func (p *pender) commit() {
	audit := p.txn.Audit
	ts := int64(p.txn.TimeNs)

	for _, t := range p.touching {
		t.commit(audit, ts)
	}
}

func (p *pender) release() {
	for _, t := range p.touching {
		t.unlock()
	}
}

func (s *state) satisfies(eff *pb.Effect) bool {
	if eff.FromValue == nil {
		return s.rev == 0
	}

	if s.rev == 0 {
		return false
	}

	return s.eqv(eff.FromValue)
}

func (s *Store) Commit(ctx context.Context, tid uint64) {
	s.doCommit(ctx, tid)
	s.toLogSync(entryCommit, &pb.Txn{Id: tid})
}

func (s *Store) Abort(ctx context.Context, tid uint64) {
	s.doAbort(ctx, tid)
	s.toLog(entryAbort, &pb.Txn{Id: tid})
}

func (s *Store) doCommit(ctx context.Context, tid uint64) {
	s.pendlock.Lock()
	p := s.pending[tid]
	delete(s.pending, tid)
	s.pendlock.Unlock()

	p.commit() // release the locks and apply the changes
}

func (s *Store) doAbort(ctx context.Context, tid uint64) {
	s.pendlock.Lock()
	p := s.pending[tid]
	delete(s.pending, tid)
	s.pendlock.Unlock()

	p.release() // release the locks
}

func (s *Store) obs(k LocalObsId) *state {
	s.statelock.RLock()
	o := s.states[k]
	s.statelock.RUnlock()
	if o != nil {
		return o
	}

	s.statelock.Lock()
	o = s.states[k] // someone else might have slipped in... double-check
	if o == nil {
		o = &state{
			lid: k,
		}
		s.states[k] = o
	}
	s.statelock.Unlock()
	return o
}

func (s *Store) Bind(ctx context.Context, name string) LocalObsId {
	s.namelock.RLock()
	id, ok := s.names[name]
	s.namelock.RUnlock()

	if ok {
		return id
	}

	s.namelock.Lock()
	// double-check; someone else may have assigned it after we released
	// the read lock
	id, ok = s.names[name]
	if !ok {
		id = 1 + LocalObsId(len(s.names))
		s.toLog(entryName, &pb.Name{
			Id:   uint64(id),
			Name: name,
		})
		s.names[name] = id
	}
	s.namelock.Unlock()
	return id
}

func (s *Store) Current(ctx context.Context, k LocalObsId) (ptr.Ptr, bool) {
	s.statelock.RLock()
	defer s.statelock.RUnlock()

	if x, ok := s.states[k]; ok && x.rev > 0 {
		return x.value, true
	}
	return ptr.Ptr{}, false
}
