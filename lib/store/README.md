


index.d/%09d.index
log.d/%09d.log

The log is comprised of entries

def(tid,...) -- trasaction definition
ready(tid) -- transaction is ready for commit
commit(tid) -- transaction is committed
abort(tid) -- transaction is aborted



we predefine 64K buckets; initial allocation is across 16 regions

   4 bits -- region
   4 bits -- spare
   8 bits -- sharding

A prefix entry looks like

  PREFIX -->