module bitbucket.org/dkolbly/omen3

require (
	bitbucket.org/dkolbly/logging v0.8.1
	bitbucket.org/dkolbly/mixup v1.1.0
	bitbucket.org/dkolbly/rpc v0.0.1
	github.com/golang/protobuf v1.3.1
	golang.org/x/net v0.0.0-20190313220215-9f648a60d977
	google.golang.org/grpc v1.19.0
)
