#! /bin/bash

set -x
set -e

if test -z "$GOPATH"
then GOPATH=$(readlink -f ../../../..)
fi

protoc -I $GOPATH/src --go_out=plugins=grpc:$GOPATH/src \
       --descriptor_set_out=pkgtest/pbtypes_api.pb \
       bitbucket.org/dkolbly/omen3/api/api.proto \
       bitbucket.org/dkolbly/omen3/api/persist.proto \
       bitbucket.org/dkolbly/omen3/api/extension.proto

protoc -I $GOPATH/src --go_out=plugins=grpc:$GOPATH/src \
       --descriptor_set_out=pkgtest/pbtypes.pb \
       bitbucket.org/dkolbly/omen3/pkgtest/datamodel.proto

go-bindata -pkg pkgtest -o pkgtest/asset.go pkgtest/pbtypes.pb pkgtest/pbtypes_api.pb
go fmt pkgtest/asset.go pkgtest/datamodel.pb.go

protoc -I $GOPATH/src --go_out=plugins=grpc:$GOPATH/src \
       bitbucket.org/dkolbly/omen3/grpcapi/api_service.proto

protoc -I $GOPATH/src --go_out=plugins=grpc:$GOPATH/src \
       bitbucket.org/dkolbly/omen3/lib/store/pb/storage.proto

protoc -I $GOPATH/src --go_out=plugins=grpc:$GOPATH/src \
       bitbucket.org/dkolbly/omen3/lib/store/service/service.proto


protoc -I $GOPATH/src --go_out=plugins=grpc:$GOPATH/src \
       --descriptor_set_out=lib/protodecode/test/pbtypes.pb \
       bitbucket.org/dkolbly/omen3/lib/protodecode/test/encrypt.proto

protoc -I $GOPATH/src --go_out=plugins=grpc:$GOPATH/src \
       bitbucket.org/dkolbly/omen3/lib/cluster/pb/cluster.proto
