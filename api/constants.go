package api

import (
	"bitbucket.org/dkolbly/omen3/ptr"
	"github.com/golang/protobuf/proto"
)

var MetaTypeFileSet = ptr.String("type^:omen:pb/file_set")
var MetaTypeNamed = ptr.String("type^:omen:pb/named_type")

func UnmarshalNamedType(buf []byte) (*NamedType, error) {
	var dst NamedType
	err := proto.Unmarshal(buf, &dst)
	if err != nil {
		return nil, err
	}
	return &dst, nil
}

func FromPtr(p ptr.Ptr) *Ptr {
	return &Ptr{Addr: p.Bits[:]}
}

func (p *Ptr) ToPtr() (dst ptr.Ptr) {
	copy(dst.Bits[:], p.Addr)
	return
}
